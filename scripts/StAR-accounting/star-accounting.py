#!/usr/bin/python2
################################################################################
## Original script is now integrated in the dmliteshell which should be used  ##
## instead of calling this legacy but fully backward compatible CLI interface ##
################################################################################
#
# star-accounting
#
# A script that produces an accounting record following the EMI StAR specs
# in the version 1.2
#
# Syntax:
#
# star-accounting [-h] [--help]
# .. to get the help screen
#
# Dependencies:
#  MySQL-python or python2-PyMySQL/python3-PyMySQL, python-lxml, python-uuid
#
#  v1.0.0 initial release
#  v1.0.2 removed site debug printouts that were screwing up the output
#  v1.0.3 avoid summing the size fields for directories. Fixes EGI doublecounting
#  v1.0.4 marks the report as regarding the past period, not the next one; parse
#         the DPM group names into VO and role
#  v1.3.0 Petr Vokac (petr.vokac@cern.ch), February 7, 2019
#         * replace SQL join with simple queries to improve performance
#         * compatibility with python 3
#  v1.4.0 Petr Vokac (petr.vokac@cern.ch), February 7, 2019
#         * integrated in dmliteshell
#
import sys
from dmliteshell import star

star._log.warn("Calling directly %s is deprecated, use `dmlite-shell -e 'star'`" % sys.argv[0])
sys.exit(star.main(sys.argv))
