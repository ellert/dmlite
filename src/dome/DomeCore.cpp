/*
 * Copyright 2015 CERN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/** @file   DomeCore.cpp
 * @brief  The main class where operation requests are applied to the status
 * @author Fabrizio Furano
 * @date   Dec 2015
 */


#include "DomeCore.h"
#include "DomeLog.h"
#include "DomeReq.h"
#include <davix.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sys/vfs.h>
#include <unistd.h>
//#include <fastcgi.h>
#include <openssl/crypto.h>

#include <sys/time.h>
#include <sys/resource.h>

DomeCore::DomeCore(): informerdavixctx() {
  domelogmask = Logger::get()->getMask(domelogname);
  //Info(Logger::Lvl1, fname, "Ctor " << dmlite_MAJOR <<"." << dmlite_MINOR << "." << dmlite_PATCH);
  initdone = false;
  terminationrequested = false;

}

DomeCore::~DomeCore() {


  Log(Logger::Lvl1, domelogmask, domelogname, "Stopping ticker.");


  if(davixPool) {
    delete davixPool;
    davixPool = NULL;
  }

  if(davixFactory) {
    delete davixFactory;
    davixFactory = NULL;
  }

  if (ticker) {
    Log(Logger::Lvl1, domelogmask, domelogname, "Joining ticker.");
    ticker->interrupt();
    ticker->join();
    delete ticker;
    ticker = 0;
    Log(Logger::Lvl1, domelogmask, domelogname, "Joined ticker.");

  }

  if (informerTicker) {
    Log(Logger::Lvl1, domelogmask, domelogname, "Joining informerTicker.");
    informerTicker->interrupt();
    informerTicker->join();
    delete informerTicker;
    informerTicker = 0;
    Log(Logger::Lvl1, domelogmask, domelogname, "Joined informerTicker.");
    
  }
}





int DomeCore::processreq(DomeReq &dreq) {
    
    status.stats.countRequest();
    
    // Last barrier against uncatched DmExceptions
    // If any comes, we give a generic error citing it
    try {
      try {
        try {
          
  
          Log(Logger::Lvl4, domelogmask, domelogname, "clientdn: '" << dreq.clientdn << "' clienthost: '" << dreq.clienthost <<
          "' remoteclient: '" << dreq.creds.clientName << "' remoteclienthost: '" << dreq.creds.remoteAddress);
          
          Log(Logger::Lvl4, domelogmask, domelogname, "req:" << dreq.verb << " cmd:" << dreq.domecmd << " query:" << dreq.object << " bodyitems: " << dreq.bodyfields.size() );
          
          
          // -------------------------
          // Generic authorization
          // Please note that authentication must be configured in the web server, not in DOME
          // -------------------------
          
          // First, check the dn is whitelisted as 'root' user
          bool authorize = status.isDNwhitelisted((char *)dreq.clientdn.c_str());

          
          if (!authorize) {
            // The whitelist in the config file did not authorize
            // Anyway this call may come from a server that was implicitly known, e.g.
            // head node trusts all the disk nodes that are registered in the filesystem table
            // disk node trusts head node as defined in the config file
            
            {
              boost::unique_lock<boost::recursive_mutex> l(status);
              authorize = status.isDNaKnownServer(dreq.clientdn);
            }
            if (authorize)
              Log(Logger::Lvl2, domelogmask, domelogname, "DN '" << dreq.clientdn << "' is authorized as a known server of this cluster.");
          }
          
          // -------------------------
          // Command dispatching
          // -------------------------
          
          if (authorize) {
            
            // Client was authorized. We log the request
            Log(Logger::Lvl1, domelogmask, domelogname, "clientdn: '" << dreq.clientdn << "' clienthost: '" << dreq.clienthost <<
            "' remoteclient: '" << dreq.creds.clientName << "' remoteclienthost: '" << dreq.creds.remoteAddress << "' oidc_issuer: '" << dreq.creds.oidc_issuer << "' oidc_scope: '" << dreq.creds.oidc_scope << "' oidc_audience: '" << dreq.creds.oidc_audience << "'");
            
            
            
            if (dreq.bodyfields.get<std::string>("lfn", "").size() > 0)
              Log(Logger::Lvl1, domelogmask, domelogname, "req:" << dreq.verb << " cmd:" << dreq.domecmd <<
              " query:" << dreq.object << " bodyitems: " << dreq.bodyfields.size() << " lfn: '" << dreq.bodyfields.get<std::string>("lfn", "") << "'");
            else
              Log(Logger::Lvl1, domelogmask, domelogname, "req:" << dreq.verb << " cmd:" << dreq.domecmd << " query:" << dreq.object << " bodyitems: " << dreq.bodyfields.size());
            

            
            // First discriminate on the HTTP request: GET/POST, etc..
            if(dreq.verb == "GET") {
              
              // Now dispatch based on the actual command name
              if ( dreq.domecmd == "dome_access" ) {
                dome_access(dreq);
              } else if ( dreq.domecmd == "dome_statpfn" ) {
                dome_statpfn(dreq);
              } else if ( dreq.domecmd == "dome_getstatinfo" ) {
                dome_getstatinfo(dreq);
              } else if ( dreq.domecmd == "dome_getreplicainfo" ) {
                dome_getreplicainfo(dreq);
              } else if ( dreq.domecmd == "dome_accessreplica" ) {
                dome_accessreplica(dreq);
              } else if ( dreq.domecmd == "dome_getspaceinfo" ) {
                dome_getspaceinfo(dreq);
              } else if(dreq.domecmd == "dome_chksum") {
                dome_chksum(dreq);
              } else if(dreq.domecmd == "dome_getdirspaces") {
                dome_getdirspaces(dreq);
              }else if(dreq.domecmd == "dome_getquotatoken") {
                dome_getquotatoken(dreq);
              }else if(dreq.domecmd == "dome_get") {
                dome_get(dreq);
              } else if ( dreq.domecmd == "dome_statpool" ) {
                dome_statpool(dreq);
              } else if ( dreq.domecmd == "dome_getdir" ) {
                dome_getdir(dreq);
              } else if ( dreq.domecmd == "dome_getuser" ) {
                dome_getuser(dreq);
              } else if ( dreq.domecmd == "dome_getusersvec" ) {
                dome_getusersvec(dreq);
              } else if ( dreq.domecmd == "dome_getidmap" ) {
                dome_getidmap(dreq);
              } else if (dreq.domecmd == "dome_info") {
                dome_info(dreq, -1, authorize);
              } else if (dreq.domecmd == "dome_getcomment") {
                dome_getcomment(dreq);
              } else if (dreq.domecmd == "dome_getgroup") {
                dome_getgroup(dreq);
              } else if (dreq.domecmd == "dome_getgroupsvec") {
                dome_getgroupsvec(dreq);
              } else if (dreq.domecmd == "dome_getreplicavec") {
                dome_getreplicavec(dreq);
              } else if (dreq.domecmd == "dome_readlink") {
                dome_readlink(dreq);
              } else if (dreq.domecmd == "dome_chooseserver") {
                dome_chooseserver(dreq);
              } else if (dreq.domecmd == "dome_config") {
                dome_config(dreq);
              } else {
                dreq.SendSimpleResp(418, SSTR("Command '" << dreq.object << "' unknown for a GET request. I like your style."));
                return -1;
              }
              
            } else if(dreq.verb == "HEAD"){ // meaningless placeholder
              dreq.SendSimpleResp(200, SSTR("This is contradicting, isn't it ?"));
              return -1;
              
            } else if(dreq.verb == "POST"){
              if ( dreq.domecmd == "dome_put" ) {
                bool success;
                dome_put(dreq, success);
              }
              else if ( dreq.domecmd == "dome_putdone" ) {
                
                if(status.role == status.roleHead)
                  dome_putdone_head(dreq);
                else
                  dome_putdone_disk(dreq);
                
              }
              else if (dreq.domecmd == "dome_unlink") {
                dome_unlink(dreq);
              }
              else if ( dreq.domecmd == "dome_setquotatoken" ) {
                dome_setquotatoken(dreq);
              }
              else if ( dreq.domecmd == "dome_addreplica" ) {
                dome_addreplica(dreq);
              }
              else if ( dreq.domecmd == "dome_delreplica" ) {
                dome_delreplica(dreq);
              }
              else if ( dreq.domecmd == "dome_pfnrm" ) {
                dome_pfnrm(dreq);
              }
              else if ( dreq.domecmd == "dome_addfstopool" ) {
                dome_addfstopool(dreq);
              }
              else if ( dreq.domecmd == "dome_modifyfs" ) {
                dome_modifyfs(dreq);
              }
              else if ( dreq.domecmd == "dome_rmfs" ) {
                dome_rmfs(dreq);
              }
              else if ( dreq.domecmd == "dome_delquotatoken" ) {
                dome_delquotatoken(dreq);
              }
              else if(dreq.domecmd == "dome_chksumstatus") {
                dome_chksumstatus(dreq);
              }
              else if(dreq.domecmd == "dome_dochksum") {
                dome_dochksum(dreq);
              }
              else if(dreq.domecmd == "dome_rmpool") {
                dome_rmpool(dreq);
              }
              else if(dreq.domecmd == "dome_addpool") {
                dome_addpool(dreq);
              }
              else if(dreq.domecmd == "dome_modifypool") {
                dome_modifypool(dreq);
              }
              else if(dreq.domecmd == "dome_pull") {
                dome_pull(dreq);
              }
              else if(dreq.domecmd == "dome_pullstatus") {
                dome_pullstatus(dreq);
              }
              else if(dreq.domecmd == "dome_updatexattr") {
                dome_updatexattr(dreq);
              }
              else if(dreq.domecmd == "dome_makespace") {
                dome_makespace(dreq);
              }
              else if(dreq.domecmd == "dome_modquotatoken") {
                dome_modquotatoken(dreq);
              }
              else if(dreq.domecmd == "dome_create") {
                dome_create(dreq);
              }
              else if(dreq.domecmd == "dome_makedir") {
                dome_makedir(dreq);
              }
              else if(dreq.domecmd == "dome_deleteuser") {
                dome_deleteuser(dreq);
              }
              else if(dreq.domecmd == "dome_newuser") {
                dome_newuser(dreq);
              }
              else if(dreq.domecmd == "dome_updateuser") {
                dome_updateuser(dreq);
              }
              else if(dreq.domecmd == "dome_deletegroup") {
                dome_deletegroup(dreq);
              }
              else if(dreq.domecmd == "dome_newgroup") {
                dome_newgroup(dreq);
              }
              else if(dreq.domecmd == "dome_updategroup") {
                dome_updategroup(dreq);
              }
              else if(dreq.domecmd == "dome_setcomment") {
                dome_setcomment(dreq);
              }
              else if (dreq.domecmd == "dome_removedir") {
                dome_removedir(dreq);
              }
              else if (dreq.domecmd == "dome_symlink") {
                dome_symlink(dreq);
              }
              else if (dreq.domecmd == "dome_rename") {
                dome_rename(dreq);
              }
              else if (dreq.domecmd == "dome_setacl") {
                dome_setacl(dreq);
              }
              else if (dreq.domecmd == "dome_setmode") {
                dome_setmode(dreq);
              }
              else if (dreq.domecmd == "dome_setowner") {
                dome_setowner(dreq);
              }
              else if (dreq.domecmd == "dome_setutime") {
                dome_setutime(dreq);
              }
              else if (dreq.domecmd == "dome_setsize") {
                dome_setsize(dreq);
              }
              else if (dreq.domecmd == "dome_setchecksum") {
                dome_setchecksum(dreq);
              }
              else if (dreq.domecmd == "dome_updatereplica") {
                dome_updatereplica(dreq);
              }
              else if (dreq.domecmd == "dome_config") {
                dome_config(dreq);
              }
              else {
                dreq.SendSimpleResp(418, SSTR("Command '" << dreq.domecmd << "' unknown for a POST request.  Nice joke, eh ?"));
                return -1;
              }
            }
            
          } // if authorized
          else {
            // only possible to run info when unauthorized
            if(dreq.domecmd == "dome_info") {
              dome_info(dreq, -1, authorize);
            }
            else {
              Err(domelogname, "DN '" << dreq.clientdn << "' has NOT been authorized.");
              dreq.SendSimpleResp(403, SSTR(dreq.clientdn << " is unauthorized. Sorry :-)"));
              return -1;
            }
          }
          
        } catch (dmlite::DmException &e) {
          Err(domelogname, "Wrong parameters. err: " << e.code() << " what: '" << e.what() << "'");
          dreq.SendSimpleResp(422, SSTR("Wrong parameters. err: " << e.code() << " what: '" << e.what() << "'"));
          return -1;
        }
      }
      catch(boost::property_tree::ptree_error &e) {
        Err(domelogname, "Error while parsing json body: " << e.what());
        dreq.SendSimpleResp(422, SSTR("Error while parsing json body: " << e.what()));
        return -1;
      }
    }
    catch(...) {
      Err(domelogname, "Generic exception.");
      dreq.SendSimpleResp(422, "Generic exception.");
      return -1;
    }
    
  
  return 0;
}








static Davix::RequestParams getDavixParams() {
  Davix::RequestParams params;

  // set timeouts, etc
  long conn_timeout = CFG->GetLong("glb.restclient.conn_timeout", 15);
  struct timespec spec_timeout;
  spec_timeout.tv_sec = conn_timeout;
  spec_timeout.tv_nsec = 0;
  params.setConnectionTimeout(&spec_timeout);
  Log(Logger::Lvl1, domelogmask, domelogname, "Davix: Connection timeout is set to : " << conn_timeout);

  long ops_timeout = CFG->GetLong("glb.restclient.ops_timeout", 15);
  spec_timeout.tv_sec = ops_timeout;
  spec_timeout.tv_nsec = 0;
  params.setOperationTimeout(&spec_timeout);
  Log(Logger::Lvl1, domelogmask, domelogname, "Davix: Operation timeout is set to : " << ops_timeout);

  // get ssl check
  bool ssl_check = CFG->GetBool("glb.restclient.ssl_check", true);
  params.setSSLCAcheck(ssl_check);
  Log(Logger::Lvl1, domelogmask, domelogname, "SSL CA check for davix is set to  " + std::string((ssl_check) ? "TRUE" : "FALSE"));

  // ca check
  std::string ca_path = CFG->GetString("glb.restclient.ca_path", (char *)"/etc/grid-security/certificates");
  if( !ca_path.empty()) {
    Log(Logger::Lvl1, domelogmask, domelogname, "CA Path :  " << ca_path);
    params.addCertificateAuthorityPath(ca_path);
  }

  // credentials
  Davix::X509Credential cred;
  Davix::DavixError* tmp_err = NULL;

  
  cred.loadFromFilePEM(CFG->GetString("glb.restclient.cli_private_key", (char *)""), CFG->GetString("glb.restclient.cli_certificate", (char *)""), "", &tmp_err);
  if( tmp_err ){
      std::ostringstream os;
      os << "Cannot load cert-privkey " << CFG->GetString("glb.restclient.cli_certificate", (char *)"") << "-" <<
            CFG->GetString("glb.restclient.cli_private_key", (char *)"") << ", Error: "<< tmp_err->getErrMsg();

      Davix::DavixError::clearError(&tmp_err);
      throw dmlite::DmException(EPERM, os.str());
  }
  params.setClientCertX509(cred);
  
  
  params.setKeepAlive(CFG->GetBool("glb.restclient.keepalive", true));
  
  
  return params;
}

int DomeCore::init(const char *cfgfile) {
        const char *fname = "DomeCore::init";
  {
    boost::lock_guard<boost::recursive_mutex> l(mtx);
    if (initdone) return -1;

    Logger::get()->setLevel(Logger::Lvl4);

    // Process the config file
    Log(Logger::Lvl1, domelogmask, domelogname, "------------------------------------------");
    Log(Logger::Lvl1, domelogmask, domelogname, "------------ Starting. Config: " << cfgfile);

    struct rlimit rlim;
    if (getrlimit(RLIMIT_NOFILE, &rlim)) {
      Err(fname, "Initialization error. Cannot invoke getrlimit()");
      return -1;
    }
    Log(Logger::Lvl1, domelogmask, domelogname, "------------ File descriptors: " << rlim.rlim_cur <<
      " max: " << rlim.rlim_max);
    
    if (rlim.rlim_cur < 48*1024) {
      Err(fname, "Initialization error. Not enough file descriptors. Please apply the system tuning hints.");
      return -1;
    }
    
    if (!cfgfile || !strlen(cfgfile)) {
      Err(fname, "No config file given." << cfgfile);
      return -1;
    }

    if (CFG->ProcessFile((char *)cfgfile)) {
      Err(fname, "Error processing config file." << cfgfile << std::endl);
      return 1;
    }

    // Initialize the logger
    long debuglevel = CFG->GetLong("glb.debug", 1);
    Logger::get()->setLevel((Logger::Level)debuglevel);

    // Initialize the status
    if (status.Init()) {
      Err(fname, "Can't initialize the status. Fatal.");
      return -1;
    }
    
    // Initialize the metadata cache
    if (!DOMECACHE) {
      Err(fname, "Can't initialize DOME cache. Fatal.");
      return -1;
    }
    DOMECACHE->configure();
    
    std::string r = CFG->GetString("glb.role", (char *)"head");
    if (r == "head") status.role = status.roleHead;
    else if (r == "disk") status.role = status.roleDisk;
    else {
      Err(fname, "Invalid role: '" << r << "'");
      return -1;
    }

    if (status.role == status.roleHead)
      status.headnodename = status.myhostname;
    else {
      // Now get the host name of the head node
      Davix::Uri uri(CFG->GetString("disk.headnode.domeurl", (char *)""));
      status.headnodename = uri.getHost();
    }

    Log(Logger::Lvl1, domelogmask, domelogname, "My head node hostname is: '" << status.headnodename << "'");

    // The limits for the prio queues, get them from the cfg
    std::vector<size_t> limits;
    limits.push_back( CFG->GetLong("head.checksum.maxtotal", 1000) );
    limits.push_back( CFG->GetLong("head.checksum.maxpernode", 40) );

    // Create the chksum queue
    status.checksumq = new GenPrioQueue(CFG->GetLong("head.checksum.qtmout", 15), limits);

    // Create the queue for the callouts
    limits.clear();
    limits.push_back( CFG->GetLong("head.filepulls.maxtotal", 1000) );
    limits.push_back( CFG->GetLong("head.filepulls.maxpernode", 40) );
    status.filepullq = new GenPrioQueue(CFG->GetLong("head.filepulls.qtmout", 15), limits);

    // Allocate the mysql factory and configure it
    if(status.role == status.roleHead) {
      DomeMySql::configure( CFG->GetString("head.db.host",     (char *)"localhost"),
                            CFG->GetString("head.db.user",     (char *)"guest"),
                            CFG->GetString("head.db.password", (char *)"none"),
                            CFG->GetLong  ("head.db.port",     0),
                            CFG->GetLong  ("head.db.poolsz",   128),
                            CFG->GetString("head.db.cnsdbname", (char *)"cns_db"),
                            CFG->GetString("head.db.dpmdbname", (char *)"dpm_db")
                          );

      // Try getting a db connection and use it. If it does not work
      // an exception will just kill us, which is what we want
      DomeMySql sql;
      // Check for the presence of the root entry in the db. If error... exit
      dmlite::DmStatus ret = sql.checkRootEntry();
      if (!ret.ok()) {
        // If we can't create the dir then this is a serious error, unless it already exists
        Err(domelogname, "Cannot create root entry. err: " << ret.code() << "-" << ret.what());
        return -1;
      }

      // Only load quotatokens on the head node
      status.loadQuotatokens();
    }


    // Configure the davix pool
    davixFactory = new dmlite::DavixCtxFactory();

    davixFactory->setRequestParams(getDavixParams());
    davixPool = new dmlite::DavixCtxPool(davixFactory, CFG->GetLong("glb.restclient.poolsize", 256));
    status.setDavixPool(davixPool);

    // Load filesystems
    status.loadFilesystems();

    
    // Initialize the davix params for the informer
    
    struct timespec spec_timeout;
    spec_timeout.tv_sec = 20;
    spec_timeout.tv_nsec = 0;
    informerdavixparams.setConnectionTimeout(&spec_timeout);
    
    spec_timeout.tv_sec = 10;
    spec_timeout.tv_nsec = 0;
    informerdavixparams.setOperationTimeout(&spec_timeout);
    
    informerdavixparams.setSSLCAcheck(false);
    
    // ca check
    std::string ca_path = CFG->GetString("glb.restclient.ca_path", (char *)"/etc/grid-security/certificates");
    if( !ca_path.empty()) {
      informerdavixparams.addCertificateAuthorityPath(ca_path);
    }
    
    // credentials
    Davix::X509Credential cred;
    Davix::DavixError* tmp_err = NULL;
    
    
    cred.loadFromFilePEM(CFG->GetString("glb.restclient.cli_private_key", (char *)""), CFG->GetString("glb.restclient.cli_certificate", (char *)""), "", &tmp_err);
    if( tmp_err ){
      std::ostringstream os;
      os << "Cannot load cert-privkey " << CFG->GetString("glb.restclient.cli_certificate", (char *)"") << "-" <<
      CFG->GetString("glb.restclient.cli_private_key", (char *)"") << ", Error: "<< tmp_err->getErrMsg();
      
      Davix::DavixError::clearError(&tmp_err);
      throw dmlite::DmException(EPERM, os.str());
    }
    informerdavixparams.setClientCertX509(cred);
    
    informerdavixparams.setKeepAlive(false);
    

    // Start the tickers
    Log(Logger::Lvl1, domelogmask, domelogname, "Starting tickers.");
    ticker = new boost::thread(boost::bind(&DomeCore::tick, this, 0));
    queueTicker = new boost::thread(boost::bind(&DomeCore::queueTick, this, 0));
    
    if(status.role == status.roleHead) {
      informerTicker = new boost::thread(boost::bind(&DomeCore::informerTick, this, 0));
      pendingPutsTicker = new boost::thread(boost::bind(&DomeCore::pendingPutsTick, this, 0));
    }
    
    return 0;
  }
}



void DomeCore::tick(int parm) {

  while (! this->terminationrequested ) {
    time_t timenow = time(0);

    Log(Logger::Lvl4, domelogmask, domelogname, "Tick");

    try {
      status.tick(timenow);
      dmTaskExec::tick();
    
      DOMECACHE->tick();
      
      //
      // Print simple stats
      //

      
      time_t tdiff = time(0) - status.stats.lastcheck;
      time_t peaktdiff = time(0) - status.stats.lastpeakcheck;
      {
        boost::unique_lock<boost::mutex> l(status.stats);
        if (peaktdiff > 0)
          status.stats.peakreqrate = fmax(status.stats.peakreqrate, (float)status.stats.peakcntrequests / peaktdiff);
        else status.stats.peakreqrate = (float)status.stats.peakcntrequests;
        
        status.stats.peakcntrequests = 0;
        status.stats.lastpeakcheck = time(0);
        
        if (tdiff > CFG->GetLong("glb.printstats.interval", 60)) {
          status.stats.stats_reqrate = (float)status.stats.cntrequests / tdiff;
          status.stats.stats_intercluster = (float)status.stats.intercluster / tdiff;
          status.stats.peak_reqrate = status.stats.peakreqrate;

          status.stats.lastcheck = time(0);
          status.stats.cntrequests = 0;
          status.stats.peakcntrequests = 0;
          status.stats.intercluster = 0;
          status.stats.peakreqrate = 0;
          
          {
            boost::unique_lock<boost::mutex> l(DomeMySql::dbstats);
              
            status.stats.db_reqrate = (float)DomeMySql::dbstats.dbqueries / tdiff;
            status.stats.db_transrate = (float)DomeMySql::dbstats.dbtrans / tdiff;
            status.stats.db_timerate = DomeMySql::dbstats.dbtrans > 0 ? (float)(DomeMySql::dbstats.dbtime / DomeMySql::dbstats.dbtrans / 1e6) / tdiff : 0.;
            DomeMySql::dbstats.dbqueries = 0;
            DomeMySql::dbstats.dbtrans = 0;
            DomeMySql::dbstats.dbtime = 0;
          }
        
          int ttot, trunning, tfinished;
          this->getTaskCounters(ttot, trunning, tfinished);
          
          Log(Logger::Lvl1, domelogmask, domelogname, "Request rate: " << status.stats.stats_reqrate
                  << "Hz (Peak: " << status.stats.peak_reqrate << "Hz) -- DB queries: " << status.stats.db_reqrate
                  << "Hz -- DB transactions: " << status.stats.db_transrate << "Hz -- DB avg transaction time: "
                  << status.stats.db_timerate << "ms -- Intercluster messages: " << status.stats.stats_intercluster
                  << "Hz -- Known tasks: " << ttot << " -- Running tasks: " << trunning << " -- Finished tasks: " << tfinished);
        }
        
      }
    }
    catch (...) {
      Err(domelogname, "Exception catched in the ticker. This is strange.");
    
    }

    sleep(CFG->GetLong("glb.tickfreq", 5));
  }

}

void DomeCore::queueTick(int parm) {

  while(! this->terminationrequested) {
    try {
      time_t timenow = time(0);
      status.waitQueues();

      Log(Logger::Lvl4, domelogmask, domelogname, "queueTick");
      status.tickQueues(timenow);
    }
    catch (...) {
      Err(domelogname, "Exception catched in the queue ticker. This is strange.");
        
    }
  }
}




void DomeCore::pendingPutsTick(int parm) {

  time_t lst = time(0);
  while(! this->terminationrequested) {
    try {


      sleep( CFG->GetLong("glb.tickfreq", 5) );
      time_t timenow = time(0);
      if (timenow <= lst+60)
        continue;
      lst = time(0);

      Log(Logger::Lvl4, domelogmask, domelogname, "Checking for expired uploads");

      std::deque<PendingPut> vpp;
      int runningupls = 0;
      {
        // Look for put entries that have expired and accumulate them into an easier place
        boost::unique_lock<boost::mutex> lk(putqueue_mtx);
        runningupls = pendingPuts.size();
        
        for(std::map<std::string, PendingPut>::iterator ii = pendingPuts.begin();
          ii != pendingPuts.end(); ii++) {
            if (timenow > (ii->second.puttime + CFG->GetLong("head.pendingput.timeout", 7200) ) )
            vpp.push_back(ii->second);
           }

      }

      if (runningupls)
        Log(Logger::Lvl1, domelogmask, domelogname, "Running uploads: " << runningupls);
      
      if (vpp.size()) {
        Log(Logger::Lvl1, domelogmask, domelogname, "Found " << vpp.size() << " expired replicas. Deleting them.");
      }
      
      
      // And now let's remove the expired replicas, without serialization
      for(std::deque<PendingPut>::iterator ii = vpp.begin(); ii != vpp.end(); ii++) {

      
        //  Doublecheck that the replica is pending,  we don't want troubles!
        DomeMySql sql;
        dmlite::Replica rep;
        dmlite::DmStatus ret;

        ret = sql.getReplicabyRFN(rep, ii->rfn);
        if (!ret.ok()) {
          {
            // This upload is not pending anymore for sure
            boost::unique_lock<boost::mutex> lk(putqueue_mtx);
            pendingPuts.erase(ii->rfn);
          }
          
          if ((ret.code() == ENOENT) || (ret.code() == DMLITE_NO_SUCH_REPLICA)) {
            Err(domelogname, "Expired replica not found '" << ii->rfn << "'");
            continue;
           }
           
          
            
          Err(domelogname, "Expired replica not accessible '" << ii->rfn << "' err: "<< ret.what());
          
          continue;
        }
        
        if (rep.status != dmlite::Replica::kBeingPopulated) {
          Err(domelogname, "Expired replica not pending '" << ii->rfn << "' err: "<< ret.what());
          
          {
            // This upload is not pending anymore for sure
            boost::unique_lock<boost::mutex> lk(putqueue_mtx);
            pendingPuts.erase(ii->rfn);
          }
          continue;
        }
        
        // If the replica is too recent,  don't kill it,  as it's strange
        if (rep.atime > time(0) - CFG->GetLong("head.pendingput.timeout", 7200)) {
          Err(domelogname, "Expired replica not really expired '" << ii->rfn << "' err: "<< ret.what());

          {
            // This upload is not pending anymore for sure
            boost::unique_lock<boost::mutex> lk(putqueue_mtx);
            pendingPuts.erase(ii->rfn);
          }
          continue;
        }
         
        // Now delete the physical file
        std::string diskurl;

        // If we have the shared secret key then we have to use plain http+sec token
        if (CFG->GetString("glb.restclient.xrdhttpkey", (char *)"").length() > 32)
        diskurl = disksrvurl("http://", ii->server.c_str());  
        else
        diskurl = disksrvurl("https://", ii->server.c_str());  

        Log(Logger::Lvl1, domelogmask, domelogname, "Dispatching deletion of expired replica '" << ii->pfn << "' to disk node: '" << ii->server);

        DomeUtils::DomeTalker talker(*davixPool, diskurl,
          "POST", "dome_pfnrm");

        boost::property_tree::ptree jreq;
        jreq.put("pfn", ii->pfn);
        jreq.put("server", ii->server);

        status.stats.countIntercluster();

        if(!talker.execute(jreq)) {
          Err(domelogname, talker.err());

          {
            boost::unique_lock<boost::mutex> lk(putqueue_mtx);
            pendingPuts.erase(ii->rfn);
          }

          continue;
         }





        Log(Logger::Lvl1, domelogmask, domelogname, "Removing expired replica: '" << ii->rfn);
        // And now remove the replica
        {
          



          DomeMySqlTrans t(&sql);
          // Free some space

          if(sql.delReplica(ii->fileid, ii->rfn) != 0) {
            std::ostringstream os;
            os << "Cannot delete replica '" << ii->rfn;
            Err(domelogname, os.str());

            {
              boost::unique_lock<boost::mutex> lk(putqueue_mtx);
              pendingPuts.erase(ii->rfn);
            }
          continue;
           }





          Log(Logger::Lvl4, domelogmask, domelogname, "Check if we have to remove the logical fileid " << ii->fileid);

          std::vector<dmlite::Replica> repls;

          dmlite::DmStatus ret = sql.getReplicas(repls, ii->fileid);
          if (!ret.ok() && ret.code() != DMLITE_NO_SUCH_REPLICA) {
            
            Err(domelogname, SSTR("Can't get replicas of fileid " << ii->fileid <<
              " err: " << ret.code() << " what:" << ret.what()) );
            {
              boost::unique_lock<boost::mutex> lk(putqueue_mtx);
              pendingPuts.erase(ii->rfn);
            }
            continue;
          
           }

          if (repls.size() == 0) {
            // Delete the logical entry if this was the last replica
            ret = sql.unlink(ii->fileid);

            if (!ret.ok()) {
              std::ostringstream os;
              os << "Cannot unlink fileid: '"<< ii->fileid << "' : " << ret.code() << "-" << ret.what();
              Err(domelogname, os.str());

              {
                boost::unique_lock<boost::mutex> lk(putqueue_mtx);
                pendingPuts.erase(ii->rfn);
              }
              continue;
             }
           }




          {
            boost::unique_lock<boost::mutex> lk(putqueue_mtx);
            pendingPuts.erase(ii->rfn);
          } 

          t.Commit();
        }
       }  // for

     }                                               

    catch (...) {
      Err(domelogname, "Exception catched in the queue ticker. This is strange.");

     }
   } //  while
 }



int DomeCore::sendInformerstring(std::ostringstream &urlquery) {
  
  
  
  // Now send it
  Davix::DavixError* tmp_err = NULL;
  Log(Logger::Lvl1, domelogmask, domelogname, "Starting request: '" << urlquery.str() << "'");
  Davix::GetRequest req(informerdavixctx, urlquery.str(), &tmp_err);
  
  if( tmp_err != NULL){
    Err(domelogname, "informer: can't initiate query for" << urlquery.str() << ", Error: "<< tmp_err->getErrMsg());
    Davix::DavixError::clearError(&tmp_err);
    return 1;
  }
  
  // Set decent timeout values for the operation
  req.setParameters(informerdavixparams);
  
  req.executeRequest(&tmp_err);
  
  std::ostringstream ss;
  ss << "Finished contacting '" << urlquery.str() << "'. Status code: " << req.getRequestCode();
  if(tmp_err) {
    ss << " DavixError: '" << tmp_err->getErrMsg() << "'";
    Err(domelogname, ss.str());
    Davix::DavixError::clearError(&tmp_err);
    return 2;
  }
  
  return 0;
}


int DomeCore::getInformerstring(std::ostringstream &str) {
  
  time_t timenow = time(0);
  
  str << "?dome=" << DMLITE_MAJOR << "." << DMLITE_MINOR << "." << DMLITE_PATCH; 
  str << "&host=" << status.myhostname;
  str << "&t=" << timenow;
  
  long long totspace, freespace;
  int poolst;
  std::string wildcard = "*";
  status.getPoolSpaces(wildcard, totspace, freespace, poolst);
  str << "&tot=" << totspace << "&free=" << freespace;
  
  
  if (CFG->GetBool("head.informer.additionalinfo", false))
  {
    boost::unique_lock<boost::mutex> l(status.stats);
    str << "&rate=" << status.stats.stats_reqrate <<
    "&peak=" << status.stats.peak_reqrate <<
    "&dbq=" << status.stats.db_reqrate <<
    "&dbtr=" << status.stats.db_transrate <<
    "&msg=" << status.stats.stats_intercluster;
  }
  
  return 0;
}

void DomeCore::informerTick(int parm) {
  
  while(! this->terminationrequested) {
    try {
      
      sleep(CFG->GetLong("head.informer.delay", 600));
      
      
      
      Log(Logger::Lvl4, domelogmask, domelogname, "informerTick");
      
      {
        std::string nfourl = CFG->GetString("head.informer.mainurl", (char *)"https://dpmhead-rc.cern.ch/dpminfo");
        if (nfourl.length() > 10) {
          std::ostringstream qry;
          qry << nfourl;
          
          getInformerstring(qry);
          
          // And here we send it
          sendInformerstring(qry);
          
        }
      }
      
      // If the feature is enabled here we send basic infosystem-like
      // information to an external system
      int idx = 0;
      while (1) {
        char infourl[1024];
        infourl[0] = '\0';
        CFG->ArrayGetString("head.informer.additionalurls", infourl, idx);
        if (!infourl[0])
          break;
        
        std::string nfourl = infourl;
        if (nfourl.length() > 10) {
          std::ostringstream qry;
          qry << nfourl;
          
          getInformerstring(qry);
          
          // And here we send it
          sendInformerstring(qry);
          
        }
        
        
      }
      
    }
    catch (...) {
      Err(domelogname, "Exception catched in the informer ticker. This is strange.");
      
    }
  }
  
  
  
}

/// Send a notification to the head node about the completion of this task
void DomeCore::onTaskCompleted(dmlite::dmTask &task) {
  
  Log(Logger::Lvl4, domelogmask, domelogname, "Entering. key: " << task.key << "' rc: " << task.resultcode << " cmd: '" << task.cmd << "' stdout: '" << task.stdout << "'");
  
  if (task.resultcode)
    Err(domelogname, "Entering. key: " << task.key << "' rc: " << task.resultcode << " cmd: '" << task.cmd << "' stdout: '" << task.stdout << "'");
  
  int key = task.key;
  bool dosend = false;
  
  PendingChecksum pending;
  PendingPull pendingpull;
  
  std::map<int, PendingChecksum>::iterator it;
  {
    boost::lock_guard<boost::recursive_mutex> l(mtx);
    it = diskPendingChecksums.find(key);
    
    
    
    if(it != diskPendingChecksums.end()) {
      Log(Logger::Lvl4, domelogmask, domelogname, "Found pending checksum. key: " << task.key);
      pending = it->second;
      diskPendingChecksums.erase(it);
      dosend = true;
    }
  }
  
  if (dosend) {
    sendChecksumStatus(pending, task, true);
    Log(Logger::Lvl4, domelogmask, domelogname, "Entering. key: " << task.key);
    return;
  }
  
  dosend = false;
  std::map<int, PendingPull>::iterator pit;
  {
    boost::lock_guard<boost::recursive_mutex> l(mtx);
    pit = diskPendingPulls.find(key);
  
  
    if(pit != diskPendingPulls.end()) {
      pendingpull = pit->second;
      Log(Logger::Lvl4, domelogmask, domelogname, "Found pending file pull. key: " << task.key);
      diskPendingPulls.erase(pit);
      dosend = true;
    }
  }
  
  if (dosend) {
    sendFilepullStatus(pendingpull, task, true);
    return;
  }
  
  
  // This may be a stat
  // This would be an internal error!!!!
  //Err(domelogname, "Cannot match task notification. key: " << task.key);
}

/// Send a notification to the head node about a task that is running
void DomeCore::onTaskRunning(dmlite::dmTask &task) {
  Log(Logger::Lvl4, domelogmask, domelogname, "Entering. key: " << task.key);
  int key = task.key;
  PendingChecksum pending;
  PendingPull pendingpull;
  bool dosend = false;
  
  std::map<int, PendingChecksum>::iterator it;
  {
    boost::lock_guard<boost::recursive_mutex> l(mtx);
    it = diskPendingChecksums.find(key);
    
    if(it != diskPendingChecksums.end()) {
      pending = it->second;
      Log(Logger::Lvl4, domelogmask, domelogname, "Found pending checksum. key: " << task.key);
      dosend = true;
    }
  }
  
  if (dosend) {
    sendChecksumStatus(pending, task, false);
    return;
  }
  
  dosend = false;
  std::map<int, PendingPull>::iterator pit;
  {
    boost::lock_guard<boost::recursive_mutex> l(mtx);
    pit = diskPendingPulls.find(key);
    
    if(pit != diskPendingPulls.end()) {
      pendingpull = pit->second;
      dosend = true;
      Log(Logger::Lvl4, domelogmask, domelogname, "Found pending file pull. key: " << task.key);
    }
  }
  
  if (dosend) {
    sendFilepullStatus(pendingpull, task, false);
    return;
  }
  
  // This would be an internal error or just a stat request.
  //Err(domelogname, "Cannot match task notification. key: " << task.key);
}


void DomeCore::fillSecurityContext(dmlite::SecurityContext &ctx, DomeReq &req) {

  // Take the info coming from the request
  req.fillSecurityContext(ctx);

  if (Logger::get()->getLevel() >= 4) {
    DomeGroupInfo g;
    std::ostringstream ss;
    for(size_t i = 0; i < ctx.groups.size(); i++) {
      if (i > 0) ss << ",";
      ss << ctx.groups[i].name;
      if (status.getGroup(ctx.groups[i].name, g)) {
        ss << "(" << g.groupid << "," << g.banned << ")";
      } else {
        ss << "(unknown)";
      }
    }
    Log(Logger::Lvl4, domelogmask, domelogname,
        "clientdn: '" << ctx.credentials.clientName << "' " <<
        "clienthost: '" << ctx.credentials.remoteAddress << "' " <<
        "ctx.user.name: '" << ctx.user.name << "' " <<
        "ctx.groups[" << ctx.groups.size() << "]: " << ss.str()
    );
  }

  // Now map uid and gids into the spooky extensible
  if (status.isDNRoot(ctx.user.name.c_str())) {
    // A client can connect ONLY if it has the right identity, i.e.
    // it's a machine in the cluster
    // If on top of that such a client has a privileged DN
    // then he is root, and can read/write everywhere
    // A privileged DN is a DN that has been explicitely whitelisted
    // OR the local hostname
    ctx.user["uid"] = 0;
    ctx.user["gid"] = 0;
    ctx.user["banned"] = false;
  }
  else {
    DomeUserInfo u;
    if (status.getUser(ctx.user.name, u)) {
      ctx.user["uid"] = u.userid;
      ctx.user["banned"] = (int)u.banned;
    } else {
      // Maybe we have to do something if the user was unknown?
      DomeMySql sql;
      if ((ctx.user.name.length() > 0) && sql.newUser(u, ctx.user.name).ok()) {
        ctx.user["uid"] = u.userid;
        ctx.user["banned"] = (int)u.banned;
      }
      else
        Err(domelogname, "Cannot add unknown user '" << ctx.user.name << "'");
    }

    DomeGroupInfo g;
    for(size_t i = 0; i < ctx.groups.size(); i++) {
      if (status.getGroup(ctx.groups[i].name, g)) {
        ctx.groups[i]["gid"] = g.groupid;
        ctx.groups[i]["banned"] = (int)g.banned;
      } else {
        // Maybe we have to do something if the group was unknown?
        DomeMySql sql;
        if ((ctx.groups[i].name.length() > 0) && sql.newGroup(g, ctx.groups[i].name).ok()) {
          ctx.groups[i]["gid"] = g.groupid;
          ctx.groups[i]["banned"] = (int)g.banned;
        }
        else
          Err(domelogname, "Cannot add unknown group '" << ctx.groups[i].name << "'");
      }


    }
  }

}






int DomeCore::checkPermissionsFullpath(const std::string &fullpath, const dmlite::SecurityContext* context, const dmlite::Acl& acl, const struct stat& stat, __mode_t mode ) {
  
  // We may have a bearer token that should override the standard perms
  return 0;
}
