class voms::ams02 {
  voms::client{'ams02.cern.ch':
      servers => [{ server => 'voms-02.pd.infn.it',
                    port   => '15008',
                    dn     => '/C=IT/O=INFN/OU=Host/L=Padova/CN=voms-02.pd.infn.it',
                    ca_dn  => '/C=IT/O=INFN/CN=INFN Certification Authority'
                  },
                  {
                    server => 'voms.grid.sinica.edu.tw',
                    port   => '15016',
                    dn     => '/C=TW/O=AS/OU=GRID/CN=voms.grid.sinica.edu.tw',
                    ca_dn  => '/C=TW/O=AS/CN=Academia Sinica Grid Computing Certification Authority Mercury'
                  },
      ],
  }
}

