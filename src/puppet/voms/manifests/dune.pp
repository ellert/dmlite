# Class defining the dune VO, as seen by the VOMS service.
#
# Takes care of all the required setup to enable access to the DUNE VO
# (users and services) in a grid enabled machine.
#
# == Examples
# 
# Simply enable this class:
#   class{'voms::dune':}
#
# == Authors
#
# CERN IT/GT/DMS <it-dep-gt-dms@cern.ch>
# CERN IT/PS/PES <it-dep-ps-pes@cern.ch>

class voms::dune {
  voms::client{'dune':
    servers => [
      {
        server => 'voms2.fnal.gov',
        port   => '15042',
        dn     => '/DC=org/DC=incommon/C=US/ST=Illinois/O=Fermi Research Alliance/OU=Fermilab/CN=voms2.fnal.gov',
        ca_dn  => '/C=US/O=Internet2/OU=InCommon/CN=InCommon IGTF Server CA',
      },
      {
        server => 'voms1.fnal.gov',
        port   => '15042',
        dn     => '/DC=org/DC=incommon/C=US/ST=Illinois/O=Fermi Research Alliance/OU=Fermilab/CN=voms1.fnal.gov',
        ca_dn  => '/C=US/O=Internet2/OU=InCommon/CN=InCommon IGTF Server CA',
      },
    ],
  }
}
