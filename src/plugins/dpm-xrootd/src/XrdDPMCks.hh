
/* 
 * Copyright (C) 2015 by CERN/IT/ST/AD. All rights reserved.
 *
 * File:   XrdDPMCks.hh
 * Author: Fabrizio Furano
 * 
 * An XrdCks plugin providing a checksum manager
 *
 * Created on Jan 13, 2012, 10:25 AM
 */

#ifndef XRDDPMCKS_HH
#define XRDDPMCKS_HH

#include <XrdSys/XrdSysLogger.hh>
#include <XrdDPMCommon.hh>
#include "sys/types.h"

#include <XrdCks/XrdCks.hh>
#include <XrdCks/XrdCksData.hh>

/* This class defines the checksum management interface. It may also be used
 *   as the base class for a plugin. This allows you to replace selected methods
 *   which may be needed for handling certain filesystems (see protected ones).
 */

class  XrdCksLoader;
class  XrdSysError;
struct XrdVersionInfo;




class XrdDPMCksManager : public XrdCks
{
public:
  virtual int         Calc( const char *Lfn, XrdCksData &Cks, int doSet=1);
  
  virtual int         Config(const char *Token, char *Line);
  
  virtual int         Del(  const char *Lfn, XrdCksData &Cks);
  
  virtual int         Get(  const char *Lfn, XrdCksData &Cks);
  
  virtual int         Init(const char *ConfigFN, const char *AddCalc=0);
  
  virtual char       *List(const char *Lfn, char *Buff, int Blen, char Sep=' ');
  
  virtual const char *Name(int seqNum=0);
  
  
  virtual int         Size( const char  *Name=0);
  
  virtual int         Set(  const char *Lfn, XrdCksData &Cks, int myTime=0);
  
  virtual int         Ver(  const char *Lfn, XrdCksData &Cks);
  
  XrdDPMCksManager(XrdSysError *erP, const char *configfn, const char *parms);
  virtual            ~XrdDPMCksManager() {};
  
protected:
  

  
private:

  static const char *XrdDPMCksNames[];

};







#endif   /* XRDDPMCKS_HH */

//------------------------------------------------------------------------------
//! Obtain an instance of the checksum manager.
//!
//! XrdCksInit() is an extern "C" function that is called to obtain an instance
//! of a checksum manager object that will be used for all subsequent checksums.
//! This is useful when checksums come from an alternate source (e.g. database).
//! The proxy server uses this mechanism to obtain checksums from the underlying
//! data server. You can also defined plugins for specific checksum calculations
//! (see XrdCksCalc.hh). The function must be defined in the plug-in shared
//! library. All the following extern symbols must be defined at file level!
//!
//! @param eDest-> The XrdSysError object for messages.
//! @param cfn  -> The name of the configuration file
//! @param parm -> Parameters specified on the ckslib directive. If none it is
//!                zero.
//!
//! @return Success: A pointer to the checksum manager object.
//!         Failure: Null pointer which causes initialization to fail.
//------------------------------------------------------------------------------

/*! extern "C" XrdCks *XrdCksInit(XrdSysError *eDest,
 *                                const char  *cFN,
 *                                const char  *Parms
 *                                );
 */
//------------------------------------------------------------------------------
//! Declare the compilation version number.
//!
//! Additionally, you *should* declare the xrootd version you used to compile
//! your plug-in. While not currently required, it is highly recommended to
//! avoid execution issues should the class definition change. Declare it as:
//------------------------------------------------------------------------------

/*! #include "XrdVersion.hh"
 *  XrdVERSIONINFO(XrdCksInit,<name>);
 * 
 *  where <name> is a 1- to 15-character unquoted name identifying your plugin.
 */
