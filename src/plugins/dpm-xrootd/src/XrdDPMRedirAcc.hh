/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: Fabrizio Furano, David Smith <David.Smith@cern.ch>
*/

#ifndef XRDDPMREDIRACC_HH
#define   XRDDPMREDIRACC_HH

#include <XrdAcc/XrdAccAuthorize.hh>
#include <XrdSys/XrdSysLogger.hh>
#include <XrdDPMCommon.hh>

class XrdDPMRedirAcc : public XrdAccAuthorize {
public:

   virtual XrdAccPrivs Access(const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env = 0);

   int Audit(const int accok,
      const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env);

   int Test(const XrdAccPrivs priv,
      const Access_Operation oper);

   XrdDPMRedirAcc(const char *cfn,int ForceSecondary);
   ~XrdDPMRedirAcc();

private:
   bool                   ForceSecondary;
   DpmCommonConfigOptions CommonConfig;
};

extern "C" XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
   const char *cfn,
   const char *parm);

extern "C" XrdAccAuthorize *DpmXrdAccAuthorizeObject(XrdSysLogger *lp,
   const char *cfn,
   const char *parm, int ForceSecondary, DpmRedirConfigOptions *rconf);

#endif
