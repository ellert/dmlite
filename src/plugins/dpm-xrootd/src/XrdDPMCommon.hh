/* 
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMCommon.hh
 * Author: Fabrizio Furano, David Smith
 * 
 * Common functions
 *
 * Created on February 2, 2012, 1:57 PM
 */

#ifndef XRDDPMCOMMON_HH
#define XRDDPMCOMMON_HH

#include <pthread.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <string>
#include <list>
#include <memory>

#include <XrdSec/XrdSecEntity.hh>
#include <XrdOuc/XrdOucString.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdOuc/XrdOucName2Name.hh>
#include <XrdSys/XrdSysError.hh>
#include <XrdSys/XrdSysPthread.hh>

#include <XrdDPMStatInfo.hh>

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/utils/extensible.h>
#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/utils/poolcontainer.h>

#define XRDDPM_IS_PUT 1
#define XRDDPM_ID_URL 2

/**********************************/
/*           classes              */
/**********************************/

class DpmIdentityConfigOptions {
public:
   XrdOucString              principal;
   std::vector<XrdOucString> fqans;
   std::vector<XrdOucString> validvo;
};


class DpmIdentity {
public:
   DpmIdentity(XrdOucEnv *Env, DpmIdentityConfigOptions &config);
   DpmIdentity(XrdOucEnv *Env);
   DpmIdentity();
   void                CopyToStack(dmlite::StackInstance&) const;
   const XrdOucString& Dn() const {return m_name;}
   const XrdOucString& Groups() const {return m_endors_raw;}
   bool                IsSecEntID() const {return UsesSecEntForID;}
   static bool         usesPresetID(XrdOucEnv *Env,const XrdSecEntity *Entity=0);
   static bool         badPresetID(DpmIdentityConfigOptions &config, XrdOucString &err);

private:
   void parse_grps();
   void parse_secent(const XrdSecEntity *secEntity);
   void check_validvo(DpmIdentityConfigOptions &config);

   XrdOucString              m_name;
   std::vector<XrdOucString> m_vorgs;
   std::vector<XrdOucString> vs;
   XrdOucString              m_endors_raw;
   bool                      UsesSecEntForID;
};

class XrdDmStackFactory : public dmlite::PoolElementFactory<dmlite::StackInstance*> {
public:
   XrdDmStackFactory() { }
   ~XrdDmStackFactory() { }
   void                   SetDmConfFile(XrdOucString &fn) { DmConfFile = fn; }
   dmlite::StackInstance* create();
   void                   destroy(dmlite::StackInstance *si) { delete si; }
   bool                   isValid(dmlite::StackInstance *) { return true; }

private:
   std::unique_ptr<dmlite::PluginManager> managerP;
   XrdSysMutex                          ManagerMtx;
   XrdOucString                         DmConfFile;
};

class XrdDmStackStore {
public:
   XrdDmStackStore() : depth(0), pool(&factory, depth) { }
   ~XrdDmStackStore() { }
   static void resetStackDpmParams(dmlite::StackInstance &si) {
      si.eraseAll();
      si.set("protocol",std::string("xroot"));
   }
   dmlite::StackInstance* getStack(DpmIdentity&, bool&);
   void RetireStack(dmlite::StackInstance *si, bool vp) {
      if (vp) pool.release(si);
      else factory.destroy(si);
   }
   void SetDmConfFile(XrdOucString &fn) { factory.SetDmConfFile(fn); }
   void SetDmStackPoolSize(int poolsz) { depth = poolsz; pool.resize(depth); }

private:
   XrdDmStackStore(const XrdDmStackStore&);
   XrdDmStackStore& operator=(const XrdDmStackStore&);

   XrdDmStackFactory factory;
   int               depth;
   dmlite::PoolContainer<dmlite::StackInstance*> pool;
};

class XrdDmStackWrap {
public:
   XrdDmStackWrap(XrdDmStackStore &stks, DpmIdentity &ident) : ss(&stks), si(0) {
      si = ss->getStack(ident,ViaPool);
   }

   XrdDmStackWrap() : ss(0), si(0) { }

   void reset() {
      dmlite::StackInstance *tmpsi = si;
      si = 0;
      if (tmpsi) { ss->RetireStack(tmpsi, ViaPool); }
      ss = 0;
   }

   void reset(XrdDmStackStore &stks, DpmIdentity &ident) {
      dmlite::StackInstance *tmpsi = si;
      si = 0;
      if (tmpsi) { ss->RetireStack(tmpsi, ViaPool); }
      ss = &stks;
      si = ss->getStack(ident, ViaPool);
   }

   dmlite::StackInstance *operator->() {
      if (!si)
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL), "No stack");
      return si;
   }

   dmlite::StackInstance&   operator*() {
      if (!si)
         throw dmlite::DmException(DMLITE_SYSERR(EINVAL), "No stack");
      return *si;
   }

   ~XrdDmStackWrap() {
      try {
         if (si) { ss->RetireStack(si, ViaPool); }
      } catch(...) {
         // ignore
      }
   }

private:
   XrdDmStackWrap(const XrdDmStackWrap&);
   XrdDmStackWrap& operator=(const XrdDmStackWrap&);

   XrdDmStackStore       *ss;
   dmlite::StackInstance *si;
   bool                   ViaPool;
};


/**********************************/
/*          functions             */
/**********************************/

XrdOucString DecodeString(XrdOucString in);

// Encode an array of bytes to base64
char *Tobase64(const unsigned char *input, int length);


// Calculates the url hash, v1 and v2, from the given information
void calc2Hashes(
   char **hashes,
   unsigned int versions,
   const char *xrd_fn,
   const char *sfn,
   const char *dpmdhost,
   const char *pfn,
   const char *dpmtk,
   unsigned int flags,
   const char *dn,
   const char *vomsnfo,
   time_t tim,
   int tim_grace,
   const char *nonce,
   const XrdOucString &locstr,
   const std::vector<XrdOucString> &chunkstr,
   unsigned const char *key,
   size_t keylen);

int compareHash(
        const char *h1,
        const char *h2);

inline const char *SafeCStr(const XrdOucString &in) {
   const char *p = in.c_str();
   if (!p)
      return "";  
   return p;
}

void InitLocalHostNameList(std::vector<XrdOucString> &names);

const char *LoadKeyFromFile(unsigned char **dat, size_t *dsize);

class DpmCommonConfigOptions {
public:
   DpmCommonConfigOptions() : OssTraceLevel(0), OfsTraceLevel(0),
      DmliteConfig("/etc/dmlite.conf"), DmliteStackPoolSize(500) { }
   int OssTraceLevel;
   int OfsTraceLevel;
   XrdOucString DmliteConfig;
   int DmliteStackPoolSize;
   XrdOucString cmslib;
};

// DpmRedirConfigOptions:
// some of these options are taken from the configuraiton file in XrdDPMCommon
// and some by XrdDPMFinder
//
class DpmRedirConfigOptions {
public:
   DpmRedirConfigOptions() : theN2N(0), theN2NVec(0), ss(0) { }
   XrdOucString defaultPrefix;
   std::vector<std::pair<XrdOucString, XrdOucString > > pathPrefixes;
   DpmIdentityConfigOptions IdentConfig;
   std::vector<XrdOucString> AuthLibRestrict;
   XrdOucName2Name *theN2N;
   XrdOucName2NameVec *theN2NVec;
   XrdOucString lroot_param;
   std::vector<XrdOucString> N2NCheckPrefixes;
   XrdDmStackStore *ss;
};

XrdOucString CanonicalisePath(const char *s, int trailing_slash);

XrdOucString TranslatePath(
   DpmRedirConfigOptions &config,
   const char *in);

std::vector<XrdOucString> TranslatePathVec(
                DpmRedirConfigOptions &config, const char *in);

XrdOucString TranslatePath(DpmRedirConfigOptions &config,
                const char *in, XrdDmStackWrap &sw, bool ensure = false);

void LocationToOpaque(const dmlite::Location &loc, XrdOucString &locstr,
   std::vector<XrdOucString> &chunkstr);

void EnvToLocstr(XrdOucEnv *env, XrdOucString &locstr,
   std::vector<XrdOucString> &chunkstr);

void EnvToLocation(dmlite::Location &loc, XrdOucEnv *env, const char *fn);

int DmExInt2Errno(int err);
int DmExErrno(const dmlite::DmException &e);

XrdOucString DmExStrerror(const dmlite::DmException &e, const char *action = 0, const char *path = 0);

XrdSysError_Table *XrdDmliteError_Table();

int DpmCommonConfigProc(XrdSysError &Eroute, const char *configfn,
   DpmCommonConfigOptions &conf, DpmRedirConfigOptions *rconf = 0);

void XrdDmCommonInit(XrdSysLogger *lp);

DpmRedirConfigOptions *GetDpmRedirConfig(XrdOucString &cmslib);

#endif   /* XRDDPMCOMMON_HH */
