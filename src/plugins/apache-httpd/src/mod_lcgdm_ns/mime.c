/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_hash.h>
#include <httpd.h>
#include <http_config.h>

#include "mime.h"

static apr_hash_t *mime_extensions = NULL;

void dav_ns_mime_init(apr_pool_t *pool, const char *map_path)
{
    ap_configfile_t *mime_cfg;
    char line[MAX_STRING_LEN];

    /* Create the hash table */
    mime_extensions = apr_hash_make(pool);

    /* Open the mime types file */
    if (ap_pcfg_openfile(&mime_cfg, pool, map_path) != APR_SUCCESS) {
        return;
    }

    /* Read line by line */
    while (!(ap_cfg_getline(line, MAX_STRING_LEN, mime_cfg))) {
        const char *ll = line, *ct;

        if (line[0] == '#')
            continue;

        ct = ap_getword_conf(pool, &ll);

        while (ll[0]) {
            char *ext = ap_getword_conf(pool, &ll);
            ap_str_tolower(ext);
            apr_hash_set(mime_extensions, ext, APR_HASH_KEY_STRING, ct);
        }
    }

    ap_cfg_closefile(mime_cfg);
}

const char *dav_ns_mime_get(char *buffer, size_t maxlen, const char *filename)
{
    char *p;

    if (!mime_extensions)
        return NULL ;

    if (!(p = strrchr(filename, '.')))
        return NULL ;

    strncpy(buffer, p + 1, maxlen);
    ap_str_tolower(buffer);

    p = apr_hash_get(mime_extensions, buffer, APR_HASH_KEY_STRING);
    if (p) {
        strncpy(buffer, p, maxlen);
        return buffer;
    }
    else {
        return NULL ;
    }
}
