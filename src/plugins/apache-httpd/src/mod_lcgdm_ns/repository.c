/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_file_io.h>
#include <apr_strings.h>
#include <dmlite/c/dmlite.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/pool.h>
#include <httpd.h>
#include <http_log.h>
#include <http_protocol.h>
#include <http_request.h>

#include "delivery.h"
#include "../shared/config.h"
#include "../shared/delegation.h"
#include "mymacaroons.h"
#include "myoidc.h"

/* Used for internal recursivity */
#define DAV_WALKTYPE_POSTFIX 0x8000
#define DAV_CALLTYPE_POSTFIX 1000

/* Advanced declarations */
static dav_error *dav_ns_walk(const dav_walk_params *params, int depth,
        dav_response **response);

/** Structure used for the walking */
struct dav_ns_walker_context
{
    const dav_walk_params *params;

    dav_walk_resource walk_resource;

    dav_buffer sfn;
    dav_buffer uri;
};
typedef struct dav_ns_walker_context dav_ns_walker_context;

/** Structure for streams (dummy, since only empty creates will be handled) */
struct dav_stream
{
};

/**
 * Just iterate the query, and pass the key/values to the dmlite stack
 */
static int dav_ns_pass_query_callback(void *rec, const char *key,
        const char *value)
{
    dmlite_context* ctx = (dmlite_context*)rec;
    dmlite_any* any_value = dmlite_any_new_string(value);
    dmlite_set(ctx, key, any_value);
    dmlite_any_free(any_value);
    return 1;
}

static void dav_ns_pass_query_to_dmlite(apr_pool_t* pool, const apr_table_t* query,
        dmlite_context* ctx)
{
    apr_table_do(dav_ns_pass_query_callback, ctx, query, NULL);
}

/**
 * Append the nameserver to the URL, so the disk can do callbacks
 */
static void dav_ns_append_self(dav_resource_private *info,
        dmlite_location *location)
{
    dmlite_any* self = dmlite_any_new_string(info->sfn);
    dmlite_any_dict_insert(location->chunks[0].url.query, "dav_sfn", self);
    dmlite_any_free(self);
}

/**
 * Append the user who requested the replica
 */
static void dav_ns_append_user(dav_resource_private *info,
        dmlite_location *location)
{
    if (info->user_creds) {
        dmlite_any* user_dn = dmlite_any_new_string(info->user_creds->client_name);
        dmlite_any_dict_insert(location->chunks[0].url.query, "dav_user", user_dn);
        dmlite_any_free(user_dn);
    }
}

/**
 * Gets the location when the server is a DPM
 */
static dav_error *dav_ns_get_dpm_location(apr_pool_t *pool,
        dav_resource_private *info, char force_secure_redirect)
{
    dmlite_location *location = dmlite_get(info->ctx, info->sfn);

    if (location == NULL) {
        return dav_shared_new_error(info->request, info->ctx, 0,
                "Could not get the location for %s", info->sfn);
    }

    if (location->chunks[0].url.domain[0] == '\0') {
        info->virtual_location = location;
        info->is_virtual = 1;
        return NULL;
    }

    /* Append the callback, so the disk can call the nameserver */
    dav_ns_append_self(info, location);

    /* Append the user */
    dav_ns_append_user(info, location);

    /* Destination URL */
    info->redirect = dav_shared_build_url(pool, &location->chunks[0].url,
            &info->d_conf->redirect, force_secure_redirect);
    dmlite_location_free(location);

    return NULL ;
}

/**
 * Gets the location when the server is an LFC
 */
static dav_error *dav_ns_get_lfc_location(apr_pool_t *pool, dav_resource_private *info,
        char force_secure_redirect)
{
    unsigned nReplicas = 0;
    dmlite_replica* replicas = NULL;

    if (dmlite_getreplicas(info->ctx, info->sfn, &nReplicas, &replicas) != 0) {
        return dav_shared_new_error(info->request, info->ctx, 0,
                "Could not get the replicas for %s", info->sfn);
    }
    else if (nReplicas == 0) {
        return dav_shared_new_error(info->request, info->ctx, HTTP_NOT_FOUND,
                "No replicas found for %s", info->sfn);
    }

    /* Destination URL. Should be in order, so pick first */
    dmlite_url* url = dmlite_parse_url(replicas[0].rfn);
    info->redirect = dav_shared_build_url(pool, url,
            &info->d_conf->redirect, force_secure_redirect);

    dmlite_url_free(url);
    dmlite_replicas_free(nReplicas, replicas);

    return NULL ;
}

/**
 * Gets the location when the server is a Plain server
 */
static dav_error *dav_ns_get_plain_location(apr_pool_t *pool,
                                          dav_resource_private *info, char force_secure_redirect)
{
    dmlite_location *location = dmlite_get(info->ctx, info->sfn);

    if (location == NULL) {
        return dav_shared_new_error(info->request, info->ctx, 0,
                                    "Could not get the location for %s", info->sfn);
    }

    if (location->chunks[0].url.domain[0] == '\0') {
        info->virtual_location = location;
        info->is_virtual = 1;
        return NULL;
    }

    /* Destination URL */
    info->redirect = dav_shared_build_url(pool, &location->chunks[0].url,
                                          &info->d_conf->redirect, force_secure_redirect);
    dmlite_location_free(location);

    return NULL ;
}

/**
 * Get the location of the file
 */
dav_error *dav_ns_get_location(apr_pool_t *pool, dav_resource_private *info,
        char force_secure_redirect)
{
    dav_error *nested_error;

    /* Logic here depends on the node type */
    switch (info->s_conf->type) {
        case DAV_NS_NODE_HEAD:
            nested_error = dav_ns_get_dpm_location(pool, info,
                    force_secure_redirect);
            break;

            /* LFC wants the whole list */
        case DAV_NS_NODE_LFC:
            nested_error = dav_ns_get_lfc_location(pool, info,
                    force_secure_redirect);
            break;

        case DAV_NS_NODE_PLAIN:
            nested_error = dav_ns_get_plain_location(pool, info,
                    force_secure_redirect);
            break;

        default:
            return dav_shared_new_error(info->request, NULL,
                    HTTP_INTERNAL_SERVER_ERROR, "Invalid node type configured");
    }

    if (nested_error)
        return nested_error;

    return NULL ;
}

/**
 * Return the parent path of "path"
 * @param pool Where to allocate space
 * @param path The path
 * @return     The parent path
 */
static char *dav_ns_dirname(apr_pool_t *pool, const char *path)
{
    char *parent;
    int i;

    parent = apr_pstrcat(pool, path, NULL );

    /* We skip the last character so if path is a directory, we are fine */
    for (i = strlen(parent) - 2; i >= 0; --i) {
        if (parent[i] == '/') {
            parent[i + 1] = '\0'; /* Keep the slash */
            break;
        }
    }

    return parent;
}

static apr_status_t dav_ns_free_dict(void *ptr)
{
    dmlite_credentials *creds = (dmlite_credentials*)ptr;
    dmlite_any_dict_free(creds->extra);
    return APR_SUCCESS;
}

/**
 * Initialize the credentials from mod_ssl/mod_gridsite environment
 */
static dav_error *dav_ns_init_user_from_env(dav_resource_private *info)
{
    info->user_creds = dav_shared_get_user_credentials(info->request->pool, info->request,
        info->d_conf->anon_user, info->d_conf->anon_group,
        info->d_conf->trusted_dns);

    if (!info->user_creds) {
        return dav_shared_new_error(info->request, NULL, HTTP_FORBIDDEN,
            "Can not authenticate the user");
    }

    apr_pool_pre_cleanup_register(info->request->pool, info->user_creds, dav_ns_free_dict);

    return NULL;
}

/**
 * Get the context from the connection notes, or creates and initializes.
 * @return NULL on success.
 */
static dav_error *dav_ns_initialize_dmlite_context(dav_resource_private *info)
{
    info->ctx = (dmlite_context*) apr_table_get(
            info->request->connection->notes, info->d_conf->context_key);

    if (info->ctx == NULL ) {
        dmlite_manager* manager;
        if (info->d_conf->manager)
            manager = info->d_conf->manager;
        else if (info->s_conf->manager)
            manager = info->s_conf->manager;
        else
            return dav_shared_new_error(info->request, NULL, HTTP_INTERNAL_SERVER_ERROR,
                    "No dmlite manager found. Probably the server is misconfigured.");

        info->ctx = dmlite_context_new(manager);
        if (info->ctx == NULL) {
            return dav_shared_new_error(info->request, NULL,
                    HTTP_INTERNAL_SERVER_ERROR,
                    "Could not instantiate a context: %s",
                    dmlite_manager_error(manager));
        }
        dmlite_any* protocol;
        if (is_ssl_used(info->request))
            protocol = dmlite_any_new_string("https");
        else
            protocol = dmlite_any_new_string("http");
        dmlite_set(info->ctx, "protocol", protocol);
        dmlite_any_free(protocol);

        apr_pool_pre_cleanup_register(info->request->connection->pool,
                info->ctx, dav_shared_context_free);

        apr_table_setn(info->request->connection->notes, info->d_conf->context_key,
                (char*) info->ctx);
    }

    if (!(info->d_conf->flags & DAV_NS_NOAUTHN)) {
        dav_error *error = NULL;
        int r;

        // Let's see if there is onfo coming from OIDC
        r = dav_ns_init_info_from_oidc(&error, info);
        if (r && (r != 1)) {
          // The OIDC info was there and was incomplete or not
          // acceptable
          return error;
        }
        
        // No OIDC? Let's see if there is a macaroon
        if (r) {
          r = dav_ns_init_info_from_macaroon(&error, info);
          if (r && (r != 1)) {
            // There was an invalid macaroon
            return error;
          }
        }

        // Otherwise, standard authn from the Apache/gridsite envvars
        if (r || (!info->user_creds)) {
          error = dav_ns_init_user_from_env(info);
            if (error) {
              return error;
            }
        }
        
        /* Pass query and headers */
        dav_shared_pass_query_args(info->request->pool, info->user_creds, info->request);
        dav_shared_pass_headers(info->request->pool, info->user_creds, info->request);
        
        if (dmlite_setcredentials(info->ctx, info->user_creds) != 0) {
            return dav_shared_new_error(info->request, info->ctx,
                HTTP_FORBIDDEN, "Could not set credentials");
        }
    }

    return NULL ;
}

/**
 * This function is used internally to create new resources.
 * @param r          The request to associate.
 * @param sfn        The SFN to link to the resource.
 * @param uri        The URI (without the query!)
 * @param user       The associated user. If NULL, will be queried and initialized.
 * @param resource   Where to put it.
 * @return           NULL on success.
 */
static dav_error *dav_ns_internal_get_resource(request_rec *r, const char *sfn,
        dmlite_context *ctx,
        dav_resource **resource)
{
    dav_resource_private *info;
    int exists;
    apr_table_t *query;
    unsigned nargs;
    dav_error *nested_error;

    /* Initialize info */
    info = apr_pcalloc(r->pool, sizeof(dav_resource_private));
    info->stat.extra = dmlite_any_dict_new();
    apr_pool_pre_cleanup_register(r->pool, info->stat.extra,
            dav_shared_dict_free);

    info->request = r;
    info->sfn = sfn;

    info->s_conf = ap_get_module_config(r->server->module_config, &lcgdm_ns_module) ;
    info->d_conf = ap_get_module_config(r->per_dir_config, &lcgdm_ns_module) ;

    /* Instantiate the catalog, if not in the connection notes */
    if (ctx) {
        info->ctx = ctx;
    }
    else {
        nested_error = dav_ns_initialize_dmlite_context(info);
        if (nested_error)
            return nested_error;
    }

    /* Extract parameters from the query */
    query = dav_shared_parse_query(r->pool, r->parsed_uri.query, &nargs);

    /* Check the existence of copyRedirected */
    if (apr_table_get(query, "copyRedirected") != NULL ) {
      info->copy_already_redirected = 1;
      apr_table_unset(query, "copyRedirected");
    }
    
#ifdef WITH_METALINK
    /* Explicit */
    info->metalink = (apr_table_get(query, "metalink") != NULL );
    /* Through headers maybe? (i.e. aria2c) */
    if (!info->metalink && dav_shared_request_accepts(r, "application/metalink+xml")) {
        info->metalink = 1;
    }
#else
    info->metalink = 0;
#endif

    /* Check if the user wants the new UI */
    const char* cookie_str = apr_table_get(r->headers_in, "Cookie");
    if (cookie_str) {
        apr_table_t* cookies = dav_lcgdm_parse_cookies(r->pool, cookie_str);
        const char* ui_cookie = apr_table_get(cookies, "lcgdm_dav.ui");
        info->new_ui = (ui_cookie && strncmp(ui_cookie, "new", 3) == 0);
    }

    /* Check if the user wants secure redirection */
    const char *secure_redirection = apr_table_get(r->headers_in, "Secure-Redirection");
    info->force_secure = (secure_redirection && strncmp(secure_redirection, "0", 1) != 0);

    /* Pass everything to dmlite */
    dav_ns_pass_query_to_dmlite(r->pool, query, info->ctx);

    /* Stat */
    switch (dmlite_statx(info->ctx, info->sfn, &(info->stat))) {
        /* We are good */
        case 0:
            exists = 1;
            break;

        /* Does not exists */
        case ENOENT:
            exists = 0;
            break;

        /* Access denied */
        case EACCES:
            /* If this is not a HEAD or a GET (both are M_GET), just fail,
             * but keep going otherwise */
            if (r->method_number != M_GET)
                return dav_shared_new_error(r, info->ctx, 0,
                        "Access forbidden for %s on %s", info->sfn, r->method);
            /* no break */

        default:
            return dav_shared_new_error(r, info->ctx, 0, "Can not stat %s",
                    info->sfn);
    }

    /* Create and return resource */
    *resource = apr_pcalloc(r->pool, sizeof(dav_resource));
    (*resource)->type = DAV_RESOURCE_TYPE_REGULAR;
    (*resource)->exists = exists;
    (*resource)->collection = ((info->stat.stat.st_mode & S_IFDIR) != 0);
    (*resource)->uri = sfn;
    (*resource)->info = info;
    (*resource)->hooks = &dav_ns_hooks_repository;
    (*resource)->pool = r->pool;

    if (exists)
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "Resource for %s (%s) found (dir=%d)", (*resource)->uri,
                (*resource)->info->sfn, (*resource)->collection);
    else
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "NULL resource for %s (%s) created", (*resource)->uri,
                (*resource)->info->sfn);

    return NULL ;
}

/**
 * Generate the resource used for later operations
 * First security checkings can be done here (like user mapping)
 * @param r                The request coming from Apache
 * @param root_dir         The root dir of the request (as configured in
 *                         Apache with Location)
 * @param label            Used with version control. Not relevant.
 * @param use_checked_in   Used with version control. Not relevant.
 * @param resource         Where to put the generated resource
 * @return                 NULL on success. A dav_error instance if not.
 */
static dav_error *dav_ns_get_resource(request_rec *r, const char *root_dir,
        const char *label, int use_checked_in, dav_resource **resource)
{
    (void) label;
    (void) use_checked_in;

    char *sfn;
    dav_error *err;
    int len;

    /* Compose path */
    len = strlen(root_dir);
    if (root_dir[len - 1] == '/') {
        char *tmp = apr_pstrdup(r->pool, root_dir);
        --len;
        tmp[len] = '\0';
        root_dir = tmp;
    }

    sfn = r->parsed_uri.path ? r->parsed_uri.path : "";

    /* Create resource */
    err = dav_ns_internal_get_resource(r, sfn, NULL, resource);
    if (err)
        return err;

    /* Directories MUST finish with '/', so redirect if not */
    len = strlen(sfn);
    if ((*resource)->collection && (len == 0 || sfn[len - 1] != '/')) {
        (*resource)->info->redirect = apr_pstrcat(r->pool,
                ap_escape_uri(r->pool, sfn), "/", r->args ? "?" : "",
                r->args ? r->args : "", NULL );
        (*resource)->uri = apr_pstrcat(r->pool, (*resource)->uri, "/", NULL );
        (*resource)->info->sfn = apr_pstrcat(r->pool, (*resource)->info->sfn,
                "/", NULL );
    }

    /* If resource does not exists, or it is a file, it can be copied to/from a remote
     * endpoint
     */
    if (!(*resource)->exists ||
        (*resource)->type == DAV_RESOURCE_TYPE_REGULAR) {
        dav_lcgdm_notify_support_external_copy(r);
    }

    /* We are done here */
    return NULL ;
}

/**
 * Get the parent of a resource (if any)
 * @param resource        The resource to process
 * @param parent_resource The parent, if any
 * @return                NULL on success
 */
static dav_error *dav_ns_get_parent(const dav_resource *resource,
        dav_resource **parent_resource)
{
    char *sfn;
    request_rec *dup_rec;

    /* The parent of the root is itself (kinda) */
    if (strcmp(resource->info->sfn, "/") == 0) {
        *parent_resource = (dav_resource*) resource;
        return NULL ;
    }

    /* Strip last element of the path */
    sfn = dav_ns_dirname(resource->pool, resource->info->sfn);

    /* Duplicate request, removing query from the parsed URI.
     * This is because on PUT requests with query string, it will remain
     * in the parent, making get_resource to behave like it does not exist :-(
     */
    dup_rec = apr_pcalloc(resource->pool, sizeof(request_rec));
    *dup_rec = *resource->info->request;
    dup_rec->parsed_uri.query = NULL;

    /* Return whatever the internal says */
    return dav_ns_internal_get_resource(dup_rec, sfn, resource->info->ctx, parent_resource);
}

/**
 * Compare two resources to check if they are the same
 * @param res1 Resource 1
 * @param res2 Resource 2
 * @return     1 if they are the same
 */
static int dav_ns_is_same(const dav_resource *res1, const dav_resource *res2)
{
    const char *destination;
    apr_uri_t uri1, uri2;

    /* Parse resource 1 */
    apr_uri_parse(res1->pool, res1->uri, &uri1);

    /* Parse resource 2 */
    destination = apr_table_get(res2->info->request->headers_in, "destination");
    if (destination)
        apr_uri_parse(res2->pool, destination, &uri2);
    else
        apr_uri_parse(res2->pool, res2->uri, &uri2);

    /* Host may not appear */
    if (!uri1.hostname)
        uri1.hostname = res1->info->request->server->server_hostname;
    if (!uri2.hostname)
        uri2.hostname = res2->info->request->server->server_hostname;

    /* Compare */
    return strcmp(uri1.hostname, uri2.hostname) == 0
            && strcmp(uri1.path, uri2.path) == 0;
}

/**
 * Check if a given resource is the parent of another one
 * @param res1 Parent to check
 * @param res2 Child
 * @return     1 if res1 is the parent of res2
 */
static int dav_ns_is_parent(const dav_resource *res1, const dav_resource *res2)
{
    char *res2_parent_sfn;

    res2_parent_sfn = dav_ns_dirname(res2->pool, res2->info->sfn);

    return strcmp(res1->info->sfn, res2_parent_sfn) == 0;
}

/**
 * Open a stream. Used for PUT
 * @param resource The resource to open
 * @param mode     The mode (basically, truncate or random, which we don't support)
 * @param stream   Where to put the created structure
 * @return         NULL on success
 */
static dav_error *dav_ns_open_stream(const dav_resource *resource,
        dav_stream_mode mode, dav_stream **stream)
{
    (void) mode;

    dav_resource_private *info;
    unsigned has_range;
    const char *range, *length, *nchunks;
    off64_t contentLength = 0;
    int errCode;

    info = resource->info;

    /* Must be writable (if we are here, they want to write) */
    if (!(info->d_conf->flags & DAV_NS_WRITE))
        return dav_shared_new_error(info->request, NULL, HTTP_FORBIDDEN,
                "Configured as read-only endpoint (%s)", resource->uri);

    /* If content-length is 0, call create instead of put! */
    length = apr_table_get(info->request->headers_in, "content-length");
    if (length != NULL ) {
        contentLength = atol(length);
    }

    /* NS alone doesn't support PUTs with content! */
    if (info->s_conf->type == DAV_NS_NODE_LFC) {
        return dav_shared_new_error(info->request, NULL, HTTP_NOT_IMPLEMENTED,
                "LFC does not support PUTs");
    }

    /*  Range header? */
    range = apr_table_get(info->request->headers_in, "content-range");
    has_range = (range != NULL );
    if (has_range) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request, "Range: %s",
                range);
    }

    /* Pass expected content-length if available */
    if (contentLength) {
        dmlite_any* any = dmlite_any_new_u64(contentLength);
        errCode = dmlite_set(info->ctx, "requested_size", any);
        dmlite_any_free(any);
        if (errCode)
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, info->request,
                    "Tried to set the requested size, but failed");
        else
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
                    "Set requested_size to %ld", contentLength);
    }

    /* Pass requested number of chunks if available */
    nchunks = apr_table_get(info->request->headers_in, "x-s3-upload-nchunks");
    if (nchunks) {
      int64_t nnchunks = atol(nchunks);
      dmlite_any* any = dmlite_any_new_u64(nnchunks);
      errCode = dmlite_set(info->ctx, "x-s3-upload-nchunks", any);
      dmlite_any_free(any);
      if (errCode)
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, info->request,
                      "Tried to set the x-s3-upload-nchunks, but failed");
        else
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
                        "Set x-s3-upload-nchunks to %ld", nnchunks);
    }
    
    /*  S3 huge upload uploadId. This may come from Davix or apps in general */
    const char *s3uploadid = apr_table_get(info->request->headers_in, "x-s3-uploadid");
    if (s3uploadid) {
      ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request, "s3uploadid: %s",
                    s3uploadid);
    }
    
    /*  Ugr/Dynafed pluginID. This may come from Davix or apps in general */
    const char *ugrpluginid = apr_table_get(info->request->headers_in, "x-ugrpluginid");
    if (ugrpluginid) {
      ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request, "ugrpluginid: %s",
                    ugrpluginid);
    }
    
    if ( (ugrpluginid != NULL) != (s3uploadid != NULL) )
      return dav_shared_new_error(info->request, NULL, HTTP_NOT_ACCEPTABLE,
      "ugrpluginid and s3uploadid must come together. Invalid headers for PUT");
    
    if (ugrpluginid) {
      dmlite_any* anyugrpluginid;
      anyugrpluginid = dmlite_any_new_string(ugrpluginid);
      dmlite_set(info->ctx, "x-ugrpluginid", anyugrpluginid);
      dmlite_any_free(anyugrpluginid);
    }
    
    if (s3uploadid) {
      dmlite_any* anys3uploadid;
      anys3uploadid = dmlite_any_new_string(s3uploadid);
      dmlite_set(info->ctx, "x-s3-uploadid", anys3uploadid);
      dmlite_any_free(anys3uploadid);
    }
    
    /* Call dmlite_put to see where to go */
    dmlite_location *location;
    location = dmlite_put(info->ctx, info->sfn);

    errCode = dmlite_errno(info->ctx);
    int httpCode = 0;
    if (errCode == EINVAL)
        httpCode = HTTP_BAD_REQUEST;
    if (errCode != 0) {
        return dav_shared_new_error(info->request, info->ctx, httpCode, "Can not put '%s' user: '%s' mech: '%s' fqan[0]: '%s'",
                                    info->sfn,
                                    (info->user_creds->client_name ? info->user_creds->client_name : ""),
                                    (info->user_creds->mech ? info->user_creds->mech : ""),
                                    ((info->user_creds->fqans && info->user_creds->fqans[0]) ? info->user_creds->fqans[0] : "")
                                   );
    }
    

    if ( (location->nchunks > 1) && ugrpluginid && s3uploadid ) {

      
      // In this case (typically dynafed-related) we have been returned
      // several pre-signed S3 URLs that will be used to upload a very large file
      //
      // We return the URLs as body of a 200 response, which in Apache seems
      // to be the only way to return a clean body
      apr_bucket_brigade *bbout = apr_brigade_create(info->request->pool, info->request->connection->bucket_alloc);
      
      int i;
      int64_t l = 0;
      // Precalculate the size of the final answer. This is horrible
      for (i = 0; i < location->nchunks; i++) {
        int32_t l1;
                
        char s3url[1024];
        dmlite_url_serialize(&location->chunks[i].url, s3url, sizeof(s3url));
        l1 = strlen(s3url);
        l += l1;      
      
        l++;
        
        
      }
      
      // Set the content-length header too. Horrible as well
      apr_table_setn(info->request->headers_out, "Content-Length", apr_psprintf(info->request->pool, "%ld", l));
      
      // Now redo the real thing and produce the body
      for (i = 0; i < location->nchunks; i++) {
        int32_t l1;
        
        char s3url[1024];
        dmlite_url_serialize(&location->chunks[i].url, s3url, sizeof(s3url));
        l1 = strlen(s3url);
        apr_brigade_write(bbout, NULL, NULL, s3url, l1);
        
        apr_brigade_write(bbout, NULL, NULL, "\n", 1);
        
        apr_status_t status = ap_pass_brigade(info->request->output_filters, bbout);
        if (status != APR_SUCCESS) {
          /* no way to know what type of error occurred */
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, status, info->request,
                        "dav_ns_open_stream: ap_pass_brigade returned %i", status);
          
          return dav_shared_new_error(info->request, info->ctx, HTTP_INTERNAL_SERVER_ERROR,
                                      "Can not return chunks to client", "");
        }
        apr_brigade_cleanup(bbout);
      }
      
      return NULL;
    }
    
    
    /* Redirect */
    if (location->chunks[0].chunkid[0])
      apr_table_set(info->request->err_headers_out, "x-ugrpluginid", location->chunks[0].chunkid);
    if (location->chunks[0].url_alt[0])
      apr_table_set(info->request->err_headers_out, "x-ugrs3posturl", location->chunks[0].url_alt);
    
    info->redirect = dav_shared_build_url(resource->pool,
            &location->chunks[0].url, &info->d_conf->redirect, info->force_secure);

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
            "PUT request to be done in %s", info->redirect);

    apr_table_set(info->request->err_headers_out, "Location", info->redirect);

    dmlite_location_free(location);

    /* To allow web clients to handle themselves the Location header, if
     * X-No-Redirect is present, use 202 Accepted instead of 307 Temporary redirect
     */
    int status_code = HTTP_TEMPORARY_REDIRECT;
    if (apr_table_get(info->request->headers_in, "x-no-redirect")) {
        status_code = HTTP_ACCEPTED;
    }

    /* Standard says 301/302, but some clients will retry in that case with a GET.
     * 307 is unambiguous */
    return dav_shared_new_error(info->request, NULL, status_code,
            "=> %s", info->redirect);
}

/**
 * Close a stream
 * @param stream The stream to close
 * @param commit Commit the change
 * @return       NULL on success
 */
static dav_error *dav_ns_close_stream(dav_stream *stream, int commit)
{
    (void) stream;
    (void) commit;

    /* Nothing to do here */
    return NULL ;
}

/**
 * Write to a stream
 * @param stream  The stream where to write
 * @param buf     The data to write
 * @param bufsize How much data there is in the buffer
 * @return        NULL on success
 */
static dav_error *dav_ns_write_stream(dav_stream *stream, const void *buf,
        apr_size_t bufsize)
{
    (void) stream;
    (void) buf;
    (void) bufsize;
    /* We never enter here (either redirected, either empty PUT's) */
    return NULL ;
}

/**
 * Moves to an specific position within a stream
 * @param stream       The stream to move
 * @param abs_position Where to move
 * @return             NULL on success
 */
static dav_error *dav_ns_seek_stream(dav_stream *stream, apr_off_t abs_position)
{
    (void) stream;
    (void) abs_position;
    /* Nothing to do here */
    return NULL ;
}

/**
 * Sets the HTTP headers, like the size of the content, the modification date...
 * @param r        The request
 * @param resource The resource generated previously
 * @return         NULL on success
 */
static dav_error *dav_ns_set_headers(request_rec *r,
        const dav_resource *resource)
{
    dav_resource_private *info;
    dav_error *err;
    const char *etag;
    char buffer[1024];
    
    /* Null resource */
    if (!resource->exists)
        return NULL ;

    info = resource->info;

    /* ETag */
    etag = resource->hooks->getetag(resource);
    apr_table_setn(r->headers_out, "ETag", etag);

    /* Content-Length if HEAD */
    if (r->header_only && !resource->collection) {
        // Apache by default unset Content-Lenght for HEAD requests and patch
        // with HttpContentLengthHeadZero was never merged in apache sources.
        // As a workaround we set Content-Length to "00" in case file size is
        // zero, because Apache filter search only for "0" to unset zero header.
        if (info->stat.stat.st_size)
            apr_table_setn(r->headers_out, "Content-Length", apr_psprintf(r->pool, "%ld", info->stat.stat.st_size));
        else
            apr_table_setn(r->headers_out, "Content-Length", "00");
    }

    /* NS saves the time in seconds since epoch, while Apache expects microseconds */
    ap_update_mtime(r, info->stat.stat.st_mtime * 1000000);
    ap_set_last_modified(r);

    
    /* Checksum
     * If the checksum calculation suggests a redirection, make sure it happens.
     */
    buffer[0] = '\0';
    err = dav_ns_digest_header(r, resource, buffer, sizeof(buffer));
    
    if (err != NULL) {
      if ( (err->status != HTTP_MOVED_TEMPORARILY) &&
        (err->status != HTTP_MOVED_PERMANENTLY) )
        return err;
      else
        // We are here if the checksum calculation gave a
        // place where to redirect to the location of the file
        // Confirm that the redir destination is not empty and just return it
        if (buffer[0])
          return err;
        
        // We are here if the checksum call did not give nor a checksum nor a destination
        // where to redirect. There will be no checksum in the response.

    }
    else if (buffer[0]) {
      apr_table_set(r->headers_out, "Digest", buffer);
    }
    
    
    if ( !resource->collection &&
        !resource->info->metalink &&
        !resource->info->is_virtual &&
        (!r->header_only) ) {
      
        if ((err = dav_ns_get_location(r->pool, info, info->force_secure)))
            return err;

        if (!info->is_virtual) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "Resource %s located at %s", info->sfn, info->redirect);

            apr_table_setn(r->headers_out, "Location", info->redirect);
#ifdef WITH_METALINK
            snprintf(buffer, sizeof(buffer),
                    "<%s?metalink>; rel=describedby; type=\"application/metalink+xml\"",
                    info->request->uri);
            apr_table_set(r->err_headers_out, "Link", buffer);
#endif
            return dav_shared_new_error(r, NULL, HTTP_MOVED_TEMPORARILY, "=> %s",
                    info->redirect);
        }
        else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "Resource %s is a virtual file", info->sfn);
        }
    }

    /* Need to redirect? (Missing trailing /) */
    if (info->redirect) {
        apr_table_setn(r->headers_out, "Location", info->redirect);
        return dav_shared_new_error(r, NULL, HTTP_MOVED_PERMANENTLY, "=> %s",
                info->redirect);
    }

    /* Content-type */
    if (resource->info->metalink) {
        // If application/metalink+xml not explicitly accepted, assume it is a browser and
        // give generic application/xml so it can be seen
        if (dav_shared_request_accepts(r, "application/metalink+xml"))
            ap_set_content_type(r, "application/metalink+xml");
        else
            ap_set_content_type(r, "application/xml");

        snprintf(buffer, sizeof(buffer), "inline; filename=\"%s.metalink\"",
                info->stat.name);
        apr_table_set(r->headers_out, "Content-Disposition", buffer);
    }
    else if (resource->collection) {
        ap_set_content_type(r, "text/html");
    }
    else if (resource->info->is_virtual) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "Resource %s is a virtual file", info->sfn);
    }

    /* Nice thing is, Apache takes care of implementing this */
    apr_table_setn(r->headers_out, "Accept-Ranges", "bytes");

    return NULL ;
}

/**
 * This is the second most important function, as it delivers the content of the
 * file (or the directory if not DAV)
 * @param resource The resource generated previously
 * @param output   Here is where the output must be written
 * @return         NULL on success
 */
static dav_error *dav_ns_deliver(const dav_resource *resource,
        ap_filter_t *output)
{
    apr_bucket_brigade *bb;
    apr_bucket *bkt;
    dav_error *err;

    bb = apr_brigade_create(resource->pool, output->c->bucket_alloc);


    if (resource->collection) {
      err = dav_ns_deliver_collection(resource,
              resource->info->request->output_filters, bb);
    }
    else if (resource->info->metalink) {
      err = dav_ns_deliver_metalink(resource,
              resource->info->request->output_filters, bb);
    }
    else if (resource->info->is_virtual) {
      err = dav_ns_deliver_virtual(resource,
              resource->info->request->output_filters, bb);
    }
    else {
      err = dav_shared_new_error(resource->info->request, NULL,
              HTTP_INTERNAL_SERVER_ERROR,
              "NS should not be trying to deliver files!");
    }
    

    if (err != NULL )
        return err;

    /* Close and flush the output */
    bkt = apr_bucket_eos_create(output->c->bucket_alloc);
    APR_BRIGADE_INSERT_TAIL(bb, bkt);
    if (ap_pass_brigade(resource->info->request->output_filters,
            bb) != APR_SUCCESS)
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_INTERNAL_SERVER_ERROR, "Could not write EOS to filter.");

    /* All OK */
    return NULL ;
}

/**
 * Create a new collection (i.e. directory)
 * @param resource The resource to be created
 * @return         NULL on success
 */
static dav_error *dav_ns_create_collection(dav_resource *resource)
{
    /* Must be writable */
    if (!(resource->info->d_conf->flags & DAV_NS_WRITE))
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_FORBIDDEN, "Configured as read-only endpoint (%s)",
                resource->uri);

    /* Must not exist */
    if (resource->exists)
        return dav_shared_new_error(resource->info->request, 0, HTTP_CONFLICT,
                "The file already exists (%s)", resource->info->sfn);

    /* Create */
    switch (dmlite_mkdir(resource->info->ctx, resource->info->sfn, 0775)) {
        case 0:
            break;
        case ENOENT:
            return dav_shared_new_error(resource->info->request,
                    resource->info->ctx, HTTP_NOT_FOUND,
                    "Could not create the directory %s - Likely one of the parents is missing.", resource->info->sfn);
        case EEXIST:
            return dav_shared_new_error(resource->info->request,
                    resource->info->ctx, HTTP_METHOD_NOT_ALLOWED,
                    "Could not create the directory %s", resource->info->sfn);
        default:
            return dav_shared_new_error(resource->info->request,
                    resource->info->ctx, 0, "Could not create the directory %s",
                    resource->info->sfn);
    }

    resource->exists = 1;
    resource->collection = 1;
    if (dmlite_statx(resource->info->ctx, resource->info->sfn,
            &(resource->info->stat)) != 0) {
        /* It has just been created, we shouldn't enter here, but just in case */
        return dav_shared_new_error(resource->info->request,
                resource->info->ctx, 0,
                "dm_xstat failed just after the creation of %s",
                resource->info->sfn);
    }

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
            "%s created", resource->info->sfn);

    return NULL ;
}





/* Validate the delegated credentials, ask for new if needed */
static dav_error *dav_ns_check_delegation(const dav_resource* res, char** uproxy)
{
  const char *client_name;
  
  client_name = dmlite_get_security_context(res->info->ctx)->credentials->client_name;
  
  if (!client_name) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, res->info->request,
                "Not doing the delegation (no client name)");
    return NULL;
  }
  
  
  /* Get delegation proxy */
  *uproxy = dav_deleg_get_proxy(res->info->request,
                                res->info->d_conf->proxy_cache, client_name);
  
  /* Proxy exists and it is valid */
  if (*uproxy) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, res->info->request,
                  "Using delegated proxy '%s'", *uproxy);
    return NULL;
  }
  /* Proxy does not exist or it isn't valid, but requested no delegation */
  else if (apr_table_get(res->info->request->headers_in, "X-No-Delegate") != NULL) {
    /* X-No-Delegate header is deprecated, instead use Credential header */
    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, res->info->request,
                  "Not doing the delegation (deprecated X-No-Delegate header present)");
    return NULL;
  }
  
  /* Already asked for a proxy and still didn't get one
   * Probably the client does not understand this semantics */
  if (res->info->copy_already_redirected) {
    return dav_shared_new_error(res->info->request, NULL,
                                419, // Need credentials
                                "Could not find a delegated proxy after an explicit request for delegation.\n"
                                "Probably your client do not support the header 'X-Delegate-To'?");
  }
  /* Delegation service configured
   * Ask for credentials to the client */
  else if (res->info->d_conf->delegation_service != NULL) {
    const char* redirect_with_delegation;
    redirect_with_delegation = apr_pstrcat(res->pool,
                                           res->info->request->unparsed_uri, "&copyRedirected=1", NULL );
    
    apr_table_setn(res->info->request->err_headers_out, "X-Delegate-To",
                   res->info->d_conf->delegation_service);
    apr_table_setn(res->info->request->err_headers_out, "Location",
                   redirect_with_delegation);
    
    return dav_shared_new_error(res->info->request, NULL,
                                HTTP_MOVED_TEMPORARILY, "Could not find a delegated proxy.");
  }
  /*  No credentials, and no delegation endpoint configured :( */
  else {
    return dav_shared_new_error(res->info->request, NULL,
                                HTTP_INTERNAL_SERVER_ERROR,
                                "Could not find a delegated proxy, and there is no delegation endpoint configured.");
  }
}




/** Structure used to pass copy-related data */
typedef struct
{
  apr_bucket_brigade *brigade;
  ap_filter_t *output;
  request_rec *request;
  long stdout_parsed; // Helps parsing the stdout from the external copy process
  const char *source;
  const char *dest;
} copyprocess_data;

/**
 * Polls the handler, and sends feedback to the client
 */
static dav_error *dav_ns_check_copyprogress(int status, dmlite_xferinfo *prog, copyprocess_data *cpdata)
{
  dav_error *error = NULL;
  const char *error_string = NULL;
  int doflush = 0, doend = 1;
  
  if ( status == EAGAIN ) {
    
    /* In the first go we need to set the reply */
    if (cpdata->request->status == 0) {
      cpdata->request->status = HTTP_ACCEPTED;
      ap_set_content_type(cpdata->request, "text/plain");
      doflush = 1;
      doend = 0;
    }
    
    // If there's a perf marker, push it to the output
    if (prog->timestamp) {
      apr_brigade_printf(cpdata->brigade, ap_filter_flush, cpdata->output,
                       "Perf Marker\n"
                       "\tTimestamp: %ld\n"
                       "\tStripe Index: %u\n"
                       "\tStripe Bytes Transferred: %ld\n"
                       "\tTotal Stripe Count: %d\n"
                       "End\n", prog->timestamp, prog->stripeindex, prog->stripexferred, 1);
    
      doflush = 1;
      doend = 0;
      prog->timestamp = 0;
    }
    
  }
  
  if (!status) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, cpdata->request,
                  "Remote copy finished successfully (%d): '%s' => '%s'",
                  status, cpdata->source, cpdata->dest);
    /* 200 is OK, but 201 fits better */
    if (cpdata->request->status == 0)
      cpdata->request->status = HTTP_OK;

    apr_brigade_printf(cpdata->brigade, ap_filter_flush, cpdata->output, "success: Created\n");
    
    doflush = 1;
    
  }
  else if (status != EAGAIN) {    
    if (cpdata->request->status == 0) {
      error_string = apr_psprintf(cpdata->request->pool, "Failed: Remote copy failed with status code %d. '%s' => '%s'", status, cpdata->source, cpdata->dest);
      
      if (error_string) {
        apr_table_setn(cpdata->request->err_headers_out, "Content-Length",
                      apr_psprintf(cpdata->request->pool, "%ld", strlen(error_string)));
        apr_brigade_write(cpdata->brigade, NULL, NULL, error_string, strlen(error_string));
        
      }
      
        
      return dav_shared_new_error(cpdata->request, NULL,
                                   status,
                                   "Failed remote copy (%d): %s\n",
                                   status,
                                   error_string ? error_string : "");
    }
    else
      apr_brigade_printf(cpdata->brigade, ap_filter_flush, cpdata->output,
                         "failure: Remote copy could not be performed (status: %d): %s\n",
                         status,
                         error_string ? error_string : "");
      
    doflush = 1; 
  }
  
  
  if (doflush) {
    if (ap_fflush(cpdata->output, cpdata->brigade) == APR_SUCCESS) {

      ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, cpdata->request,
                    "COPY flush '%s' %lu/%lu", cpdata->request->uri,
                    (unsigned long) prog->stripexferred,
                    (unsigned long) prog->stripexferred);
      
      if (doend) {
        apr_bucket *bkt;
        bkt = apr_bucket_eos_create(cpdata->brigade->bucket_alloc);
        APR_BRIGADE_INSERT_TAIL(cpdata->brigade, bkt);
        ap_pass_brigade(cpdata->output, cpdata->brigade);
      }
    }
  }
  
  return error;
}




/**
 * Trigger a remote copy using the dmlite calls. This is a PUSH copy
 * @param src       The source. It must exist
 * @param dst       The destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error *dav_ns_remote_copy_dmlite(const dav_resource *src, const char *dst,
                                     dav_response **response)
{
  
  if (!(src->info->d_conf->flags & DAV_NS_REMOTE_COPY)) {
    return dav_shared_new_error(src->info->request, NULL,
                                HTTP_METHOD_NOT_ALLOWED,
                                "This service does not allow remote copies");
  }

  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, src->info->request,
                "Performing COPY of '%s' to '%s'", src->info->sfn,
                dst);
  
  int dmliteres = 0;
  dav_error *de = NULL;
  time_t timenow = time(0);
  dmlite_xferinfo progressdata;
  memset(&progressdata, 0, sizeof(progressdata));
  
  // Various Apache-related fields, useful to send responses
  copyprocess_data cpdata;
  cpdata.output = src->info->request->output_filters;
  cpdata.brigade = apr_brigade_create(src->info->request->pool, cpdata.output->c->bucket_alloc);
  cpdata.request = src->info->request;
  cpdata.stdout_parsed = 0;
  cpdata.source = src->info->sfn;
  cpdata.dest = dst;
  
  // Check if there is a delegated proxy to pass. If there is a macaroon don't fail
  char *uproxy = NULL;
  const char *credential = apr_table_get(src->info->request->headers_in, "Credential");
  
//   if (!src->info->user_creds->mech ||
//     strcmp(src->info->user_creds->mech, MACAROON_MECH)) {
//     const char *xferhdrauth = apr_table_get(src->info->request->headers_in, "TransferHeaderAuthorization");
//     // But don't do the delegation if the TransferHeaderAuthorization had been set
//     if (!xferhdrauth) {
//       dav_error *error = dav_ns_check_delegation(src, &uproxy);
//       if (error != NULL) {
//         return error;
//       }
//     }
//     
//   }
    
  if (!credential || strcmp(credential, "gridsite") == 0) {
    dav_error *error = dav_ns_check_delegation(src, &uproxy);
    if (error != NULL) {
      if (!src->info->user_creds->mech ||
        strcmp(src->info->user_creds->mech, MACAROON_MECH))
        return error;
    }
  }
  
  // Hint: this is used to pass the location of the user's proxy certificate to any mechanism that does third party copy
  dmlite_any* anyuproxy;
  if (uproxy) {
    anyuproxy = dmlite_any_new_string(uproxy);

    dmlite_set(src->info->ctx, "x509_delegated_proxy_path", anyuproxy);
    dmlite_any_free(anyuproxy);
  }
  
  cpdata.request->status = 0;
  
   do {

     // This one is supposed to return on success, error, or after a few seconds with EAGAIN
     dmliteres = dmlite_copypush(src->info->ctx, src->info->sfn, dst, 0, (char *)"", &progressdata);
    
     // The call may return HTTP codes ( >200 ) or UNIX codes ( <35 )
     // Let's translate the main UNIX codes into HTTP
     switch (dmliteres) {
       case EEXIST:
         dmliteres = HTTP_CONFLICT;
         break;
       case ENOENT:
         dmliteres = HTTP_NOT_FOUND;
         break;
       case EPERM:
       case EACCES:
         dmliteres = HTTP_FORBIDDEN;
         break;
     }
     
     // Here the perf markers should be output to the client
     de = dav_ns_check_copyprogress(dmliteres, &progressdata, &cpdata);
     
     
   } while (!de && (dmliteres == EAGAIN) && (time(0) - timenow < 3600)); // arbitrary, one hour timeout as an extreme measure
      
  if (dmliteres) {
    if (de)
      return de;
    
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, src->info->request,
                  "Cannot perform COPY of '%s' to '%s'. dmliteres: %d Expired: %ld", src->info->sfn,
                  dst, dmliteres, time(0) - timenow);
    return dav_shared_new_error(src->info->request, src->info->ctx,
                                HTTP_UNPROCESSABLE_ENTITY,
                                "Could not COPY '%s' to '%s'. dmliteres: %d Expired: %ld",
                                src->info->sfn, dst, dmliteres, time(0) - timenow);
  }
  
  int errid = 0;
  if (de) errid = de->error_id;
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, src->info->request,
                "Finished COPY (%d) of '%s' to '%s'", errid, src->info->sfn, dst);
  // All Ok
  return de;

}

/**
 * Trigger a remote copy (a push)
 * @param src       The source. It must exist
 * @param dst       The destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error *dav_ns_remote_copy(const dav_resource *src, const char *dst,
                                     dav_response **response)
{
  (void) dst;
  (void) response;
  
  if (!(src->info->d_conf->flags & DAV_NS_REMOTE_COPY)) {
    return dav_shared_new_error(src->info->request, NULL,
                                HTTP_METHOD_NOT_ALLOWED,
                                "This server does not allow remote copies");
  }
  
  if (src->info->d_conf->flags & DAV_NS_REMOTE_COPY_DMLITE) {
    // If the remotecopydmlite flag is set in the Apache config then the copy request
    // will be managed by dmlite
    dav_error *e;
    e = dav_ns_remote_copy_dmlite(src, dst, response);
    apr_table_unset(src->info->request->headers_out, "Location");
    return e;
  }
  
  dav_error *error;
  error = dav_ns_get_location(src->info->request->pool, src->info, 1);
  if (error)
    return error;
  
  
  apr_table_setn(src->info->request->headers_out, "Location",
                 src->info->redirect);
  
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, src->info->request,
                "COPY of '%s' redirected to '%s'", src->info->sfn,
                src->info->redirect);
  
  return dav_shared_new_error(src->info->request, NULL,
                              HTTP_TEMPORARY_REDIRECT, "=> %s", src->info->redirect);
}



/**
 * Trigger a remote fetch using the dmlite calls. This is a PULL copy
 * @param src       The remote source.
 * @param dst       The local destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error* dav_ns_remote_fetch_dmlite(const char *src,
                                      const dav_resource *dst, dav_response **response)
{
  if (dst->exists) {
    return dav_shared_new_error(dst->info->request, NULL,
                                HTTP_CONFLICT, "File exists %s", dst->uri);
  }
  
  if (!(dst->info->d_conf->flags & DAV_NS_REMOTE_COPY)) {
    return dav_shared_new_error(dst->info->request, NULL,
                                HTTP_METHOD_NOT_ALLOWED,
                                "This server does not allow remote fetches");
  }
  
  
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, dst->info->request,
                "Performing COPY of '%s' to '%s'", src, dst->info->sfn);
  int dmliteres = 0;
  dav_error *de = NULL;
  time_t timestart = time(0);
  dmlite_xferinfo progressdata;
  memset(&progressdata, 0, sizeof(progressdata));
  
  // Various Apache-related fields, useful to send responses
  copyprocess_data cpdata;
  cpdata.output = dst->info->request->output_filters;
  cpdata.brigade = apr_brigade_create(dst->info->request->pool, cpdata.output->c->bucket_alloc);
  cpdata.request = dst->info->request;
  cpdata.stdout_parsed = 0;
  cpdata.source = src;
  cpdata.dest = dst->info->sfn;
  
  // Check if there is a delegated proxy to pass. If there is a macaroon don't fail
  char *uproxy = NULL;
  const char *credential = apr_table_get(dst->info->request->headers_in, "Credential");
//   if (!dst->info->user_creds->mech ||
//     strcmp(dst->info->user_creds->mech, MACAROON_MECH)) {
//     
//     const char *xferhdrauth = apr_table_get(dst->info->request->headers_in, "TransferHeaderAuthorization");
//     // But don't do the delegation if the TransferHeaderAuthorization had been set
//     if (!xferhdrauth) {
//       dav_error *error = dav_ns_check_delegation(dst, &uproxy);
//       if (error != NULL) {
//         if (!dst->info->user_creds->mech ||
//           strcmp(dst->info->user_creds->mech, MACAROON_MECH))
//           return error;
//       }
//     }
//   
//   }
  
  if (!credential || strcmp(credential, "gridsite") == 0) {
     dav_error *error = dav_ns_check_delegation(dst, &uproxy);
     if (error != NULL) {
        if (!dst->info->user_creds->mech ||
          strcmp(dst->info->user_creds->mech, MACAROON_MECH))
          return error;
      }
  }
  
  // Hint: this is used to pass the location of the user's proxy certificate to any mechanism that does third party copy
  dmlite_any* anyuproxy;
  if (uproxy) {
    anyuproxy = dmlite_any_new_string(uproxy);
  
    dmlite_set(dst->info->ctx, "x509_delegated_proxy_path", anyuproxy);
    dmlite_any_free(anyuproxy);
  }
  
  cpdata.request->status = 0;
  
  do {
    
    // This one is supposed to return on success, error, or after a few seconds with EAGAIN
    dmliteres = dmlite_copypull(dst->info->ctx, dst->info->sfn, src, 0, (char *)"", &progressdata);
    
    // The call may return HTTP codes ( >200 ) or UNIX codes ( <35 )
    // Let's translate the main UNIX codes into HTTP
    switch (dmliteres) {
      case EEXIST:
        dmliteres = HTTP_CONFLICT;
        break;
      case ENOENT:
        dmliteres = HTTP_NOT_FOUND;
        break;
      case EPERM:
      case EACCES:
        dmliteres = HTTP_FORBIDDEN;
        break;
    }
    
    // Here the perf markers should be output to the client
    de = dav_ns_check_copyprogress(dmliteres, &progressdata, &cpdata);
    
  } while (!de && (dmliteres == EAGAIN) && (time(0) - timestart < 3600)); // arbitrary, one hour timeout as an extreme measure
  
  if (dmliteres) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, dst->info->request,
                  "Cannot perform COPY of '%s' to '%s'. dmliteres: %d Expired: %ld", src,
                  dst->info->sfn, dmliteres, time(0) - timestart);
    return dav_shared_new_error(dst->info->request,
                                dst->info->ctx, 0, "Could not COPY '%s' to '%s'. dmliteres: %d Expired: %ld",
                                src, dst->info->sfn,
                                dst->info->sfn, dmliteres, time(0) - timestart);
  }
  
  int errid = 0;
  if (de) errid = de->error_id;
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, dst->info->request,
                "Finished COPY (%d) of '%s' to '%s'", errid, src, dst->info->sfn);
  // All Ok
  return de;
 
}


/**
 * Trigger a remote fetch
 * @param src       The remote source.
 * @param dst       The local destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error* dav_ns_remote_fetch(const char *src,
        const dav_resource *dst, dav_response **response)
{
    if (dst->exists) {
        return dav_shared_new_error(dst->info->request, NULL,
                HTTP_CONFLICT, "File exists %s", dst->uri);
    }

    if (!(dst->info->d_conf->flags & DAV_NS_REMOTE_COPY)) {
        return dav_shared_new_error(dst->info->request, NULL,
                HTTP_METHOD_NOT_ALLOWED,
                "This server does not allow remote fetches");
    }

    if (dst->info->d_conf->flags & DAV_NS_REMOTE_COPY_DMLITE) {
      // If the remotecopydmlite flag is set in the Apache config then the copy request
      // will be managed by dmlite
      return dav_ns_remote_fetch_dmlite(src, dst, response);
    }
    
    // Delegate to the write handler, since it does pretty much what we
    // want to do
    apr_table_unset(dst->info->request->headers_in, "content-range");
    apr_table_unset(dst->info->request->headers_in, "content-length");
    return dav_ns_open_stream(dst, DAV_MODE_WRITE_TRUNC, NULL);
}

/**
 * Local copies not allowed
 * @param src       The source. It must exist
 * @param dst       The destination. Must not exist
 * @param depth     Copy in depth? (Collections)
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error *dav_ns_copy(const dav_resource *src, dav_resource *dst,
        int depth, dav_response **response)
{
    (void) depth;
    return dav_ns_remote_copy(src, dst->uri, response);
}

/**
 * Move a resource
 * @param src       The source. It must exist
 * @param dst       The destination. It must not exist
 * @param response  The response to the move process
 * @return          NULL on success
 */
static dav_error *dav_ns_move(dav_resource *src, dav_resource *dst,
        dav_response **response)
{
    /* Must be writable */
    if (!(src->info->d_conf->flags & DAV_NS_WRITE))
        return dav_shared_new_error(dst->info->request, 0, HTTP_FORBIDDEN,
                "Configured as read-only endpoint (%s)", dst->uri);

    /* Change the name, that's all */
    if (dmlite_rename(src->info->ctx, src->info->sfn, dst->info->sfn) != 0) {
        return dav_shared_new_error(dst->info->request, src->info->ctx, 0,
                "Could not move from %s to %s", src->info->sfn, dst->info->sfn);
    }

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, src->info->request,
            "Moved %s to %s", src->info->sfn, dst->info->sfn);

    *response = NULL;
    return NULL ;
}

/**
 * Remove a resource from DPM, either file or directory
 * NOT recursive, resource has to be a leaf, or this will fail
 * @param resource The resource to remove
 * @return         NULL on success
 */
static dav_error *dav_ns_internal_remove(const dav_resource *resource)
{
    int error = 0;

    if (resource->collection) {
        /* Remove a directory is straigh-forward */
        if (dmlite_rmdir(resource->info->ctx, resource->info->sfn) != 0) {
            error = 1;
        }
        else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                    "Directory %s removed", resource->info->sfn);
        }
    }
    else {
        /* A file is not much more difficult :-) */
        if (dmlite_unlink(resource->info->ctx, resource->info->sfn) != 0) {
            error = 1;
        }
        else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                    "File %s removed", resource->info->sfn);
        }
    }

    if (error) {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, resource->info->request,
                "Could not remove %s (%s)", resource->info->sfn,
                dmlite_error(resource->info->ctx));
        return dav_shared_new_error(resource->info->request,
                resource->info->ctx, 0, "Could not remove %s",
                resource->info->sfn);
    }

    return NULL ;
}

/**
 * Callback function for remove walking
 * @param wres      The walking resource
 * @param calltype  Calltype
 * @return          NULL on success
 */
static dav_error *dav_ns_remove_callback(dav_walk_resource *wres, int calltype)
{
    /* Only remove elements that exist, and empty directories */
    if (wres->resource->exists
            && (!wres->resource->collection || calltype == DAV_CALLTYPE_POSTFIX)) {

        dav_error *err;

        err = dav_ns_internal_remove(wres->resource);

        /* We add to the multistatus, not as return */
        if (err)
            dav_shared_add_response(wres, err);
    }
    return NULL ;
}

/**
 * Remove a resource
 * @param resource The resource to remove
 * @param response The response to the request
 * @return         NULL on success
 */
static dav_error *dav_ns_remove(dav_resource *resource, dav_response **response)
{
    dav_error *err;

    /* Be careful: mod_dav will try to remove the file when copying, if source
     * and dest happen to have the same path (even if the host is different).
     * Just ignore those requests.
     */
    if (resource->info->request->method_number == M_COPY) {
        ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, resource->info->request,
                "Ignoring remove requests on COPY");
        return NULL ;
    }

    /* Must be writable */
    if (!(resource->info->d_conf->flags & DAV_NS_WRITE))
        return dav_shared_new_error(resource->info->request, 0, HTTP_FORBIDDEN,
                "Configured as read-only endpoint (%s)", resource->uri);

    if (resource->collection) {
        /* Recursive if collection */
        dav_walk_params params = {
                0, NULL, NULL, NULL, NULL, NULL };
        dav_response *multi_status;

        params.walk_type = DAV_WALKTYPE_POSTFIX;
        params.func = dav_ns_remove_callback;
        params.pool = resource->pool;
        params.root = resource;

        if ((err = dav_ns_walk(&params, DAV_INFINITY, &multi_status)) != NULL )
            return err;

        if ((*response = multi_status) != NULL ) {
            /* Multistatus */
            return dav_shared_new_error(resource->info->request, NULL,
                    HTTP_MULTI_STATUS, "Errors on multistatus");
        }

        resource->exists = 0;
        resource->collection = 0;

        return NULL ;
    }
    else {
        /* File is pretty easy */
        err = dav_ns_internal_remove(resource);
        if (err)
            return err;
        resource->exists = 0;
        resource->collection = 0;
        return NULL ;
    }
}

/**
 * Recursive function to walk through the filesystem
 * @param ctx The context
 * @param depth The maximum depth
 * @return NULL on success
 */
static dav_error *dav_ns_walker(dav_ns_walker_context *ctx, int depth)
{
    dav_error *err = NULL;
    void *dir;
    dmlite_xstat *entry;
    apr_pool_t *pool;
    const dav_resource *this_res;
    dav_resource reusable;
    apr_pool_t *subpool;

    pool = ctx->params->pool;

    /* Save for later */
    this_res = ctx->walk_resource.resource;

    /* Call the function with the current root */
    err = (*ctx->params->func)(&(ctx->walk_resource),
            ctx->walk_resource.resource->collection ?
                    DAV_CALLTYPE_COLLECTION : DAV_CALLTYPE_MEMBER);
    if (err)
        return err;
    if (depth == 0 || !ctx->walk_resource.resource->collection)
        return NULL ;

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0,
            ctx->walk_resource.resource->info->request, "Descending into %s",
            ctx->sfn.buf);

    /* Open the directory */
    if ((dir = dmlite_opendir(this_res->info->ctx, ctx->sfn.buf)) == NULL ) {
        dav_shared_add_response(&ctx->walk_resource,
                dav_shared_new_error(this_res->info->request,
                        this_res->info->ctx, 0,
                        "Could not open the directory %s", ctx->sfn.buf));
        return NULL ;
    }

    /* Create the subpool */
    apr_pool_create(&subpool, ctx->walk_resource.pool);

    /* Initialize the reusable resource */
    reusable = *this_res;
    reusable.info = NULL;
    reusable.uri = NULL;
    reusable.info = apr_pcalloc(reusable.pool, sizeof(dav_resource_private));
    reusable.info->request = this_res->info->request;
    reusable.info->ctx = this_res->info->ctx;
    reusable.info->s_conf = this_res->info->s_conf;
    reusable.info->d_conf = this_res->info->d_conf;
    reusable.pool = subpool;

    ctx->walk_resource.resource = &reusable;

    /* Scan the directory */
    while ((entry = dmlite_readdirx(this_res->info->ctx, dir))) {
        apr_size_t uri_prev_len;
        apr_size_t path_prev_len;
        int len;

        apr_pool_clear(subpool);

        /* Append the file to path and URI */
        len = strlen(entry->name);
        dav_buffer_place_mem(pool, &ctx->uri, entry->name, len + 1, 1);
        dav_buffer_place_mem(pool, &ctx->sfn, entry->name, len + 1, 1);

        reusable.info->sfn = ctx->sfn.buf;

        /* dav_buffer_place_mem do NOT adjust len */
        uri_prev_len = ctx->uri.cur_len;
        path_prev_len = ctx->sfn.cur_len;
        ctx->sfn.cur_len += len;
        ctx->uri.cur_len += len;

        if (entry->stat.st_mode & S_IFDIR) {
            ctx->sfn.cur_len++;
            ctx->uri.cur_len++;

            ctx->uri.buf[ctx->uri.cur_len - 1] = ctx->sfn.buf[ctx->sfn.cur_len
                    - 1] = '/';
            ctx->uri.buf[ctx->uri.cur_len] = ctx->sfn.buf[ctx->sfn.cur_len] =
                    '\0';
        }

        /* Set "entry-related" fields */
        reusable.uri = ctx->uri.buf;
        reusable.collection = S_ISDIR(entry->stat.st_mode);

        reusable.info->stat = *entry;

        /* Attach or descend */
        if (!ctx->walk_resource.resource->collection) {
            /* Add file */
            err = ctx->params->func(&(ctx->walk_resource), DAV_CALLTYPE_MEMBER);
            if (err)
                break;
        }
        else {
            /* Recursive into child */
            if ((err = dav_ns_walker(ctx, depth - 1)) != NULL )
                break;
        }

        /* Restore length, so next dav_buffer_place_mem overrites current entry */
        ctx->uri.cur_len = uri_prev_len;
        ctx->sfn.cur_len = path_prev_len;
    }

    /* Release resources */
    if (dmlite_closedir(this_res->info->ctx, dir))
        return dav_shared_new_error(this_res->info->request,
                this_res->info->ctx, 0, "Could not close the directory %s",
                ctx->sfn.buf);

    if (err)
        return err;

    apr_pool_destroy(subpool);

    /* Recover */
    ctx->walk_resource.resource = this_res;
    ctx->uri.buf[ctx->uri.cur_len] = '\0';
    ctx->sfn.buf[ctx->sfn.cur_len] = '\0';

    /* Postfix iteration */
    if (ctx->params->walk_type & DAV_WALKTYPE_POSTFIX) {
        return ctx->params->func(&(ctx->walk_resource), DAV_CALLTYPE_POSTFIX);
    }

    /* There we go */
    return NULL ;
}

/**
 * Walk through the file system
 * @param params    Walk parameters
 * @param depth     In depth?
 * @param response  The response
 * @return          NULL on success
 */
static dav_error *dav_ns_walk(const dav_walk_params *params, int depth,
        dav_response **response)
{
    dav_ns_walker_context ctx;
    dav_error *err;

    memset(&ctx, 0, sizeof(ctx));

    ctx.params = params;

    ctx.walk_resource.walk_ctx = params->walk_ctx;
    ctx.walk_resource.pool = params->pool;
    ctx.walk_resource.resource = params->root;

    dav_buffer_init(params->pool, &ctx.sfn, params->root->info->sfn);
    dav_buffer_init(params->pool, &ctx.uri, params->root->uri);

    err = dav_ns_walker(&ctx, depth);

    *response = ctx.walk_resource.response;

    return err;
}

/**
 * Get an ETag
 * @param resource The resource from where retrieve the tag
 * @return         The tag value
 */
static const char *dav_ns_getetag(const dav_resource *resource)
{
    if (resource->info->new_ui) {
        return apr_psprintf(resource->pool, "%lx-%lx-ui.new",
                        (unsigned long) resource->info->stat.stat.st_ino,
                        (unsigned long) resource->info->stat.stat.st_mtime);
    }
    else {
        return apr_psprintf(resource->pool, "%lx-%lx",
                (unsigned long) resource->info->stat.stat.st_ino,
                (unsigned long) resource->info->stat.stat.st_mtime);
    }
}

/** Hooks */
const dav_hooks_repository dav_ns_hooks_repository = {
    1, 1,
    dav_ns_get_resource,
    dav_ns_get_parent,
    dav_ns_is_same,
    dav_ns_is_parent,
    dav_ns_open_stream,
    dav_ns_close_stream,
    dav_ns_write_stream,
    dav_ns_seek_stream,
    dav_ns_set_headers,
    dav_ns_deliver,
    dav_ns_create_collection,
    dav_ns_copy,
    dav_ns_move,
    dav_ns_remove,
    dav_ns_walk,
    dav_ns_getetag,
    NULL,
    dav_ns_remote_copy,
    dav_ns_remote_fetch,
    dav_ns_handle_post,
};
