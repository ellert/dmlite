/*
 * Copyright (c) CERN 2020
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_LCGDM_NS_OIDC_H
#define MOD_LCGDM_NS_OIDC_H

#include "../mod_dav/mod_dav.h"


#define OIDC_MECH "oidc"

/**
 * Fill the info from an OIDC bearer token
 * @return 0: OK, done and this was a valid oidc bearer token
 *         1: Can't find a token here
 *         >1: there was an invalid token
 */
int dav_ns_init_info_from_oidc(dav_error **ret, dav_resource_private *info);

#endif // MOD_LCGDM_NS_OIDC_H
