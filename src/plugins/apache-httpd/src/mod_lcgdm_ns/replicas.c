/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <jansson.h>
#include "replicas.h"
#include "../shared/utils.h"

char* dav_ns_serialize_replicas(request_rec *req, int nreplicas,
        dmlite_replica *replicas)
{
    int i;
    char *serial;
    char buffer[1024];

    apr_pool_t *pool = req->pool;

    serial = "[\n";

    for (i = 0; i < nreplicas; ++i) {
        serial = apr_psprintf(pool, "%s"
                "{\n"
                "\t\"server\"    : \"%s\",\n"
                "\t\"rfn\"       : \"%s\",\n"
                "\t\"atime\"     : %lu,\n"
                "\t\"status\"    : \"%c\",\n"
                "\t\"type\"      : \"%c\",\n"
                "\t\"ltime\"     : %lu", serial, replicas[i].server,
                replicas[i].rfn, replicas[i].atime,
                replicas[i].status ? replicas[i].status : '?',
                replicas[i].type ? replicas[i].type : '?', replicas[i].ltime);

        if (replicas[i].extra != NULL ) {
            serial = apr_psprintf(pool, "%s,\n"
                    "\t\"extra\": %s\n", serial,
                    dmlite_any_dict_to_json(replicas[i].extra, buffer,
                            sizeof(buffer)));
        }

        if (i + 1 < nreplicas)
            serial = apr_pstrcat(pool, serial, "},\n", NULL );
        else
            serial = apr_pstrcat(pool, serial, "}\n", NULL );
    }

    serial = apr_pstrcat(pool, serial, "]", NULL );

    return serial;
}


dav_error* dav_ns_deserialize_replicas(request_rec *req, const char* json_str,
        struct dav_ns_replica_array* replicas)
{
    json_error_t error;
    json_t *json = json_loads(json_str, 0, &error);
    int i;
    apr_pool_t *pool = req->pool;

    /* Just in case */
    if (json == NULL )
        return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                "Could not parse the JSON string");

    /* Must be an array! */
    if (!json_is_array(json)) {
        json_decref(json);
        return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                "First-level JSON Object must be an array");
    }

    /* Initialize the array */
    replicas->nreplicas = json_array_size(json);
    replicas->replicas =
            apr_pcalloc(pool, sizeof(dmlite_replica) * replicas->nreplicas);
    replicas->action = apr_pcalloc(pool, sizeof(char) * replicas->nreplicas);

    /* Iterate and copy */
    for (i = 0; i < replicas->nreplicas; ++i) {
        json_t *json_replica = json_array_get(json, i);
        if (!json_is_object(json_replica)) {
            json_decref(json);
            return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                    "Malformed JSON replica object");
        }

        /* Get the json objects */
        json_t *server = json_object_get(json_replica, "server");
        json_t *rfn = json_object_get(json_replica, "rfn");
        json_t *status = json_object_get(json_replica, "status");
        json_t *type = json_object_get(json_replica, "type");
        json_t *action = json_object_get(json_replica, "action");
        json_t *ltime = json_object_get(json_replica, "ltime");
        json_t *atime = json_object_get(json_replica, "atime");
        json_t *extra = json_object_get(json_replica, "extra");

        /* Get the actual values */
        if (json_is_string(server)) {
            strncpy(replicas->replicas[i].server,
                    json_string_value(server), HOST_NAME_MAX);
            replicas->replicas[i].server[HOST_NAME_MAX - 1] = '\0';
        }
        if (json_is_string(rfn)) {
            strncpy(replicas->replicas[i].rfn, json_string_value(rfn), URL_MAX);
            replicas->replicas[i].rfn[URL_MAX - 1] = '\0';
        }
        if (json_is_string(status))
            replicas->replicas[i].status = json_string_value(status)[0];
        if (json_is_string(type))
            replicas->replicas[i].type = json_string_value(type)[0];
        if (json_is_integer(ltime))
            replicas->replicas[i].ltime = json_integer_value(ltime);
        if (json_is_integer(atime))
            replicas->replicas[i].atime = json_integer_value(atime);

        if (json_is_string(action))
            replicas->action[i] = toupper(json_string_value(action)[0]);
        else
            replicas->action[i] = 'M';

        /* Pass the 'extra' */
        if (json_is_object(extra)) {
            char *extra_str = json_dumps(extra, 0);
            replicas->replicas[0].extra = dmlite_any_dict_from_json(extra_str);
            free(extra_str);
            if (replicas->replicas[0].extra == NULL )
                return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                        "Could not parse the JSON extra string");
        }
        else {
            replicas->replicas[0].extra = NULL;
        }
    }

    json_decref(json);

    return NULL ;
}
