/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _HTEXT_H
#define	_HTEXT_H

#include <stddef.h>
#include <stdio.h>

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct htext_handle htext_handle;

/* Options */
typedef enum
{
    HTEXTOP_SOURCEURL = 0, /* The source URL. Can be a local or a remote file */
    HTEXTOP_DESTINATIONURL, /* The source URL. Can be a local or a remote file */
    HTEXTOP_NUMBEROFSTREAMS, /* Number of streams to use */

    HTEXTOP_USERCERTIFICATE, /* User certificate          */
    HTEXTOP_USERPRIVKEY, /* User private key          */
    HTEXTOP_USERPRIVKEYPASS, /* User private key password */
    HTEXTOP_PROXYLIFE, /* Delegated proxy lifetime in minutes */

    HTEXTOP_CAPATH, /* CA Path */
    HTEXTOP_CAFILE, /* CA File */
    HTEXTOP_CRLPATH, /* CRL Path */
    HTEXTOP_CRLFILE, /* CRL File */
    HTEXTOP_CRLMISSOK, /* CRL Missing OK */
    HTEXTOP_VERIFYPEER, /* Validate (!0) or not (0) the remote certificate */

    HTEXTOP_CLIENTID, /* Client ID for the requests */

    HTEXTOP_LOGCALLBACK, /* If set, steps will be logged to this function */
    HTEXTOP_LOGCALLBACK_DATA, /* Passed tot he log callback */
    HTEXTOP_VERBOSITY, /* 0 minimal, 1 answers, 2 debug */

    HTEXTOP_BUFFERSIZE, /* Buffer size. Restricted by underlying libcurl */
    HTEXTOP_LOW_SPEED_TIME, /* low speed limit time period */
    HTEXTOP_LOW_SPEED_LIMIT, /* low speed limit in bytes per low speed time period */

    HTEXTOP_DELEGATION_URL, /* Delegation URL. It can contain a %s where the
     * host will be inserted */

    HTEXTOP_IO_HANDLER, /* Allows to specify custom I/O routines.
     * Have a look into the struct htext_io_handler */
    HTEXTOP_IO_HANDLER_DATA, /* Additional data to pass to open and size */

    HTEXTOP_NOHEAD, /* Disable HEAD requests */

    HTEXTOP_USE_COPY_FROM_SOURCE, /* Use Copy Source (fetching) instead of Copy Destination (pushing) */
    HTEXTOP_COPY_DONE, /* If set, called after COPY operation is done */
    HTEXTOP_COPY_DONE_DATA, /* Additional data to pass to transfer done function */

    HTEXTOP_SENTINEL, /* To mark the last one */

} HTEXT_OPTION;

/* Possible statuses */
typedef enum
{
    HTEXTS_CREATED = 0,
    HTEXTS_STARTING,
    HTEXTS_WAITING,
    HTEXTS_RUNNING,
    HTEXTS_SUCCEEDED,
    HTEXTS_FAILED,
    HTEXTS_ABORTED
} HTEXT_STATUS;

/* Types of logging */
typedef enum
{
    HTEXT_LOG = 0, HTEXT_HEADER_IN, HTEXT_HEADER_OUT, HTEXT_BODY, HTEXT_TEXT
} HTEXT_LOG_TYPE;

/**
 * Prototype of the log callback
 * @param handle The handle that triggers the callback
 * @param type   The type of the logging (see HTEXT_LOG_TYPE)
 * @param msg    The message to log
 * @param size   The length of the message
 * @param ud     The user defined data
 */
typedef void (*htext_log_callback)(htext_handle *handle, HTEXT_LOG_TYPE type,
        const char *msg, size_t size, void *ud);

/* Custom I/O routines
 * Prototypes follows fopen/fclose/fread/fwrite...
 */
struct htext_io_handler
{
    off_t (*size)(const char *path, void* udata); /**< Returns -1 on failure */
    void* (*open)(const char *path, const char *mode, void* udata);
    int (*close)(void*);
    size_t (*read)(void*, size_t, size_t, void*);
    size_t (*write)(const void*, size_t, size_t, void*);
    int (*seek)(void*, long, int);
    long (*tell)(void*);
    int (*eof)(void*);
};

/**
 * Initializes internals. To be call before any thread starts, and before
 * any call to htext_init()
 * @return 0 on success.
 */
int htext_global_init();

/**
 * Create a new handle
 * @return A new handle, or NULL if there is not enough memory
 */
extern htext_handle* htext_init();

/**
 * Set an option
 * @param handle The handle where to set the option
 * @param option The option (see HTEXT_OPTION)
 * @param ...    The value. If it is a pointer, be sure it won't be freed
 *               while the handle is running.
 * @return 0 on success. 1 on failure, and errno will be set.
 */
extern int htext_setopt(htext_handle *handle, unsigned option, ...);

/**
 * Add a HTTP header
 * @param handle The handle where to set the option
 * @param header Header (i.e. Content-Type)
 * @param value Value
 * @return 0 on success. 1 on failure, and errno will be set.
 */
extern int htext_addheader(htext_handle *handle, const char *header, const char *value);

/**
 * Perform the appropiate action. This means, put, get or copy
 * depending on the source and URL
 * @param handle
 * @return 0 on success. Otherwise, errno is set.
 */
extern int htext_perform(htext_handle *handle);

/**
 * Destroy the handle and free the associated memory
 * @param handle The handle to free
 */
extern void htext_destroy(htext_handle *handle);

/**
 * Abort the transfer
 * @param handle The handle to abort
 */
extern void htext_abort(htext_handle *handle);

/**
 * Return the status of the handle
 * @param handle
 * @return See HTEXT_STATUS
 */
extern int htext_status(htext_handle *handle);

/**
 * Put into total and done the current progress
 * @param handle The handle
 * @param n      Number of streams being used
 * @param total  A pointer to n partial totals will be set
 * @param done   A pointer to n partial dones will be set
 * @return 0 on success. Otherwise, errno is set.
 */
extern int htext_progress(htext_handle *handle, size_t *n, size_t **total,
        size_t **done, char ***rconn);

/**
 * Return the error string, if status is failed or aborted
 * @param handle
 * @return The error string, or NULL if not available
 */
extern const char *htext_error_string(htext_handle *handle);

/**
 * Return the HTTP return code. Useful in case an error happens.
 * @param handle
 * @return 0 if not set or the action failed before the server could be contacted.
 */
extern int htext_http_code(htext_handle *handle);

#ifdef	__cplusplus
}
#endif

#endif	/* _HTEXT_H */
