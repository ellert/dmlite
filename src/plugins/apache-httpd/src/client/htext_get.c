/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "htext.h"
#include "htext_private.h"

/**
 * Implements a partial download
 * @param pp A pointer to a htext_partial structure
 * @return NULL on return
 */
static void* htext_get_subthread(void *pp)
{
    htext_chunk *partial = (htext_chunk*) pp;
    char err_buffer[CURL_ERROR_SIZE];
    char range_buffer[128];

    /* Set the function's user data */
    curl_easy_setopt(partial->curl, CURLOPT_HEADERDATA, partial);
#if LIBCURL_VERSION_NUM >= 0x073200
    curl_easy_setopt(partial->curl, CURLOPT_XFERINFODATA, partial);
#else
    curl_easy_setopt(partial->curl, CURLOPT_PROGRESSDATA, partial);
#endif
    curl_easy_setopt(partial->curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(partial->curl, CURLOPT_WRITEFUNCTION,
            GETIO(partial->handle)->write);
    curl_easy_setopt(partial->curl, CURLOPT_WRITEDATA, partial->fd);
    curl_easy_setopt(partial->curl, CURLOPT_DEBUGDATA, partial);
    curl_easy_setopt(partial->curl, CURLOPT_SSL_CTX_DATA, partial);

    /* Range */
    if (partial->nchunks > 1) {
        sprintf(range_buffer, "Range: bytes=%zu-%zu", partial->start, partial->end);
        partial->headers = curl_slist_append(partial->headers, range_buffer);
    }
    curl_easy_setopt(partial->curl, CURLOPT_HTTPHEADER, partial->headers);

    /* Perform */
    if (curl_easy_perform(partial->curl) != CURLE_OK) {
        partial->error_string = strdup(err_buffer);
        partial->handle->status = HTEXTS_FAILED;
    }

    /* Curl blocks, so here we are done */
    return NULL ;
}

void *htext_get_method(void *h)
{
    htext_handle *handle = (htext_handle*) h;
    CURL *curl;
    htext_chunk *partial_array;
    size_t fsize = 0;
    size_t unused, stream_size, last_size;
    char err_buffer[CURL_ERROR_SIZE];
    unsigned i, npartials;
    int canceled;
    curl_version_info_data *curl_ver = curl_version_info(CURLVERSION_NOW);

    /* Create and initialize CURL handle with common stuff */
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, htext_header_callback);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
    curl_easy_setopt(curl, CURLOPT_URL, GETSTR(handle, HTEXTOP_SOURCEURL));
    curl_easy_setopt(curl, CURLOPT_USERAGENT, GETSTR(handle, HTEXTOP_CLIENTID));
    curl_easy_setopt(curl, CURLOPT_CAPATH, GETSTR(handle, HTEXTOP_CAPATH));
    curl_easy_setopt(curl, CURLOPT_CAINFO, GETSTR(handle, HTEXTOP_CAFILE));
    // CURL doesn't provide direct interface for "CRLPATH", use openssl context directly
    curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, htext_sslctx_callback);
    curl_easy_setopt(curl, CURLOPT_CRLFILE, GETSTR(handle, HTEXTOP_CRLFILE));
    curl_easy_setopt(curl, CURLOPT_SSLCERT,
            GETSTR(handle, HTEXTOP_USERCERTIFICATE));
    curl_easy_setopt(curl, CURLOPT_SSLKEY, GETSTR(handle, HTEXTOP_USERPRIVKEY));
    curl_easy_setopt(curl, CURLOPT_SSLKEYPASSWD,
            GETSTR(handle, HTEXTOP_USERPRIVKEYPASS));
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER,
            GETINT(handle, HTEXTOP_VERIFYPEER));
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);

    if (GETINT(handle, HTEXTOP_VERBOSITY) > 1) {
        curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, htext_debug_callback);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    }

    if (GETINT(handle, HTEXTOP_BUFFERSIZE)> 0) {
        curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, GETINT(handle, HTEXTOP_BUFFERSIZE));
    }

#if LIBCURL_VERSION_NUM >= 0x072600
    if (curl_ver->version_num >= 0x072600) {
        // Require a minimum speed from the transfer
        // (default: must move at least 1MB every 2 minutes - roughly 8KB/s)
        if (GETINT(handle, HTEXTOP_LOW_SPEED_TIME) > 0 && GETINT(handle, HTEXTOP_LOW_SPEED_LIMIT) > 0) {
            curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, GETINT(handle, HTEXTOP_LOW_SPEED_TIME));
            curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, GETINT(handle, HTEXTOP_LOW_SPEED_LIMIT));
        }
    }
#endif

    /* Shared */
    curl_easy_setopt(curl, CURLOPT_SHARE, handle->curl_share);

    /* Do a HEAD to get the size */
    if (!GETINT(handle, HTEXTOP_NOHEAD)) {
        htext_chunk head;

        htext_partial_init(&head);
        head.handle = handle;
        head.chunk_total = &fsize;
        head.chunk_done = &unused;
        head.headers = htext_copy_slist(handle->headers);

        curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, &head);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, head.headers);
        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &head);
        curl_easy_setopt(curl, CURLOPT_SSL_CTX_DATA, &head);

        handle->status = HTEXTS_WAITING;
        htext_log(handle, "Asking for the size");
        if (curl_easy_perform(curl) != CURLE_OK) {
            htext_log(handle, "Failed to get the file size, so no streaming: %s", err_buffer);
        }

        if (head.http_status >= 400 || handle->status == HTEXTS_FAILED) {
            htext_log(handle, "Failed to get the file size, so no streaming code: %d (%s)",
                      head.http_status, head.http_response ? head.http_response : "no response header");
        }
        /* Did we get the location, and is it cacheable? */
        else if (head.location && head.redirect_is_cacheable) {
            curl_easy_setopt(curl, CURLOPT_URL, head.location);
        }

        /* Explicitly reset pointers to released head data structure */
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, NULL);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, NULL);
        curl_easy_setopt(curl, CURLOPT_SSL_CTX_DATA, NULL);

        htext_partial_clean(&head);
    }

    /* Calculate stream size and final stream size */
    if (fsize) {
        npartials = GETINT(handle, HTEXTOP_NUMBEROFSTREAMS);
        if (fsize % npartials == 0) {
            stream_size = last_size = fsize / npartials;
        }
        else {
            stream_size = fsize / npartials;
            last_size = stream_size + (fsize % npartials);
        }
    }
    else {
        /* No size? Too bad, won't be able to stream */
        npartials = 1;
        stream_size = last_size = 0;
        htext_log(handle, "No Content-Length, so no multistream possible");
    }

    /* Set progress function */
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
#if LIBCURL_VERSION_NUM >= 0x073200
    if (curl_ver->version_num >= 0x073200) {
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, htext_progress_callback);
    } else {
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, htext_progress_old_callback);
    }
#else
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, htext_progress_old_callback);
#endif

    /* Set up partials */
    handle->partial_total = calloc(sizeof(size_t), npartials);
    handle->partial_done = calloc(sizeof(size_t), npartials);
    handle->partial_rconn = calloc(sizeof(char*), npartials);
    handle->partials = npartials;

    /* Spawn a thread per partial */
    htext_log(handle, "Downloading (%i streams)", npartials);
    handle->status = HTEXTS_RUNNING;
    partial_array = calloc(sizeof(htext_chunk), npartials);

    for (i = 0; i < npartials; ++i) {
        htext_partial_init(&(partial_array[i]));
        partial_array[i].index = i;
        partial_array[i].handle = handle;
        partial_array[i].curl = curl_easy_duphandle(curl);
        partial_array[i].chunk_total = &(handle->partial_total[i]);
        partial_array[i].chunk_done = &(handle->partial_done[i]);
        partial_array[i].chunk_rconn = &(handle->partial_rconn[i]);
        partial_array[i].headers = htext_copy_slist(handle->headers);

        /* duphandle doesn't duplicate this :( */
        curl_easy_setopt(partial_array[i].curl, CURLOPT_SHARE,
                handle->curl_share);

        /* Open */
        partial_array[i].fd = GETIO(handle) ->open(
                GETSTR(handle, HTEXTOP_DESTINATIONURL),
                "w",
                GETPTR(handle, HTEXTOP_IO_HANDLER_DATA));
        if (!partial_array[i].fd) {
            handle->status = HTEXTS_FAILED;
            break;
        }

        /* Range and seek */
        partial_array[i].start = i * stream_size;
        if (i < handle->partials - 1)
            partial_array[i].end = partial_array[i].start + stream_size - 1;
        else
            partial_array[i].end = partial_array[i].start + last_size - 1;

        *(partial_array[i].chunk_total) = partial_array[i].end - partial_array[i].start;

        if (GETIO(handle) ->seek(partial_array[i].fd, partial_array[i].start, SEEK_SET) < 0) {
            handle->status = HTEXTS_FAILED;
            break;
        }

        /* Launch */
        pthread_create(&(partial_array[i].thread), NULL, htext_get_subthread,
                &(partial_array[i]));
    }

    /* Exited on error? */
    if (handle->status == HTEXTS_FAILED) {
        htext_strerror_r(errno, err_buffer, sizeof(err_buffer));
        htext_error(handle, err_buffer);
    }

    /* Wait for all of them */
    for (i = 0; i < npartials; ++i) {
        canceled = 0;
        if (partial_array[i].thread != 0) {
            /* Wait, or kill if failed */
            if (handle->status == HTEXTS_FAILED) {
                if (partial_array[i].error_string)
                    htext_log(handle, "Cancelling get thread idx %d, http status %d: %s", i, partial_array[i].http_status, partial_array[i].error_string);
                else
                    htext_log(handle, "Cancelling get thread idx %d, http status %d", i, partial_array[i].http_status);
                pthread_cancel(partial_array[i].thread);
                canceled = 1;
            }

            pthread_join(partial_array[i].thread, NULL );
        }

        if (handle->status != HTEXTS_FAILED) {
            handle->http_status = partial_array[i].http_status;

            if (handle->http_status < 200 || handle->http_status >= 300) {
                handle->status = HTEXTS_FAILED;

                if (!handle->error_string) {
                    /* Propagate first error */
                    if (partial_array[i].error_string)
                        handle->error_string = strdup(partial_array[i].error_string);
                    else if (partial_array[i].http_response)
                        handle->error_string = strdup(partial_array[i].http_response);
                }

                htext_log(handle, "Bad HTTP code %d in get thread idx %d: %s", i, handle->http_status, handle->error_string ? handle->error_string : "");
            }
        }
        else if (!canceled && partial_array[i].error_string) {
            /* Report all errors for finished threads */
            htext_log(handle, "Error in get thread idx %d: %s", i, partial_array[i].error_string);

            if (!handle->error_string)
                handle->error_string = strdup(partial_array[i].error_string);
        }

        /* Clean partial */
        htext_partial_clean(&(partial_array[i]));
    }

    /* Done */
    if (handle->status == HTEXTS_RUNNING) {
        handle->status = HTEXTS_SUCCEEDED;
    }

    /* Call transfer done callback */
    if (GETPTR(handle, HTEXTOP_COPY_DONE)) {
        void (*copy_done)(htext_handle *, int, void *) = GETPTR(handle, HTEXTOP_COPY_DONE);
        copy_done(handle, handle->status, GETPTR(handle, HTEXTOP_COPY_DONE_DATA));
    }

    /* Clean up */
    curl_easy_cleanup(curl);
    free(partial_array);

    return NULL ;
}
