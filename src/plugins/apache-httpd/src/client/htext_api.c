/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "htext.h"
#include "htext_private.h"

/** Internal size function */
off_t htext_fsize(const char *path, void* udata)
{
    (void) udata;

    struct stat st;
    if (stat(path, &st) != 0)
        return -1;
    return st.st_size;
}

/** Default I/O handler */
struct htext_io_handler default_io_handler = {
    htext_fsize,
    (void*(*)(const char*, const char*, void*))(fopen),
    (int(*)(void*))(fclose),
    (size_t(*)(void*, size_t, size_t, void*))fread,
    (size_t(*)(const void*, size_t, size_t, void*))(fwrite),
    (int(*)(void*, long, int))(fseek),
    (long(*)(void*))(ftell),
    (int(*)(void*))(feof)
};

/**
 * Dummy callback
 */
void dummy_callback()
{
    return;
}

/* Types associated with the options */
static option_entry option_definitions[] = {
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_SOURCEURL       */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_DESTINATIONURL  */
    { OT_INT, (option_value) 1 }, /* HTEXTOP_NUMBEROFSTREAMS */

    { OT_STRING, (option_value) NULL }, /* HTEXTOP_USERCERTIFICATE */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_USERPRIVKEY     */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_USERPRIVKEYPASS */
    { OT_INT, (option_value) 720 }, /* HTEXTOP_PROXYLIFE       */

    { OT_STRING, (option_value) NULL }, /* HTEXTOP_CAPATH     */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_CAFILE     */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_CRLPATH    */
    { OT_STRING, (option_value) NULL }, /* HTEXTOP_CRLFILE    */
    { OT_INT, (option_value) 1 }, /* HTEXTOP_CRLMISSOK */
    { OT_INT, (option_value) 1 }, /* HTEXTOP_VERIFYPEER */

    { OT_STRING, (option_value) "htext" }, /* HTEXTOP_CLIENTID */

    { OT_POINTER, (option_value) (void*) dummy_callback }, /* HTEXTOP_LOGCALLBACK */
    { OT_POINTER, (option_value) NULL }, /* HTEXTOP_LOGCALLBACK_DATA       */
    { OT_INT, (option_value) 0 }, /* HTEXT_VERBOSITY         */

    { OT_INT, (option_value) 0 }, /* HTEXT_BUFFERSIZE        */
    { OT_INT, (option_value) (2*60) }, /* HTEXTOP_LOW_SPEED_TIME */
    { OT_INT, (option_value) (10*1024) }, /* HTEXTOP_LOW_SPEED_LIMIT */

    { OT_STRING, (option_value) NULL }, /* HTEXT_DELEGATION_URL */

    { OT_POINTER, (option_value) NULL }, /* HTEXTOP_IO_HANDLER */
    { OT_POINTER, (option_value) NULL }, /* HTEXTOP_IO_FOPEN_DATA */

    { OT_INT, (option_value) 0 }, /* HTEXTOP_NOHEAD         */
    { OT_INT, (option_value) 0 }, /* HTEXTOP_USE_COPY_FROM_SOURCE */
    { OT_POINTER, (option_value) NULL }, /* HTEXTOP_COPY_DONE */
    { OT_POINTER, (option_value) NULL }, /* HTEXTOP_COPY_DONE_DATA */
};

/**
 * Checks if url is a remote path
 * @return 1 if remote. 0 if local.
 */
static int is_remote(const char *url)
{
    return strncmp(url, "https:", 6) == 0 || strncmp(url, "http:", 5) == 0;
}

int htext_global_init()
{
    if (htext_locking_setup() != 0)
        return 1;

    return curl_global_init(CURL_GLOBAL_ALL);
}

htext_handle* htext_init()
{
    htext_handle *handle;
    int i;

    /* Alloc */
    handle = calloc(1, sizeof(htext_handle));

    /* Copy defaults */
    handle->options = calloc(HTEXTOP_SENTINEL, sizeof(option_entry));
    for (i = 0; i < HTEXTOP_SENTINEL; ++i) {
        handle->options[i] = option_definitions[i];
        if (handle->options[i].type == OT_STRING
                && option_definitions[i].value.s)
            handle->options[i].value.s = strdup(option_definitions[i].value.s);
    }

    /* Initialize fields */
    handle->curl_share = curl_share_init();
    curl_share_setopt(handle->curl_share, CURLSHOPT_SHARE,
            CURL_LOCK_DATA_SSL_SESSION);

    /* Done */
    handle->status = HTEXTS_CREATED;
    return handle;
}

void htext_destroy(htext_handle *handle)
{
    int i;

    if (handle->thread)
        pthread_join(handle->thread, NULL );

    switch (handle->status) {
        case HTEXTS_WAITING:
        case HTEXTS_STARTING:
        case HTEXTS_RUNNING:
            htext_abort(handle);
    }

    curl_share_cleanup(handle->curl_share);
    free(handle->error_string);
    free(handle->partial_total);
    free(handle->partial_done);
    for (i = 0; i < handle->partials; ++i) {
        free(handle->partial_rconn[i]);
    }
    free(handle->partial_rconn);

    for (i = 0; i < HTEXTOP_SENTINEL; ++i) {
        if (handle->options[i].type == OT_STRING)
            free(handle->options[i].value.s);
    }

    free(handle->options);
    free(handle);
}


/** If url happens to start with dav/davs, replace with http/https,
 *  or CURL won't understand
 */
static char* htext_normalize_scheme(const char* url)
{
    if (strncmp("http://", url, 7) == 0 || strncmp("https://", url, 8) == 0)
        return strdup(url);

    size_t new_len = strlen(url) + 1;
    char *new_url = calloc(sizeof(char), new_len + 1);
    if (strncmp("dav://", url, 6) == 0)
        snprintf(new_url, new_len + 1, "http://%s", url + 6);
    else if (strncmp("davs://", url, 7) == 0)
        snprintf(new_url, new_len + 1, "https://%s", url + 7);
    else
        strncpy(new_url, url, new_len);
    return new_url;
}

int htext_setopt(htext_handle *handle, unsigned option, ...)
{
    va_list list;
    option_entry *op;
    const char *c;

    /* Valid option? */
    if (option >= HTEXTOP_SENTINEL) {
        errno = EINVAL;
        return 1;
    }

    /* Handle in valid status? */
    switch (handle->status) {
        case HTEXTS_WAITING:
        case HTEXTS_STARTING:
        case HTEXTS_RUNNING:
            errno = EBADFD;
            return 1;
    }

    /* Set */
    op = &(handle->options[option]);

    va_start(list, option);
    switch (op->type) {
        case OT_STRING:
            c = va_arg(list, const char*);
            free(op->value.s);
            if (option == HTEXTOP_SOURCEURL || option == HTEXTOP_DESTINATIONURL)
                op->value.s  = c? htext_normalize_scheme(c) : NULL;
            else
                op->value.s = c ? strdup(c) : NULL;
            break;
        case OT_POINTER:
            op->value.p = va_arg(list, void*);
            break;
        case OT_INT:
            op->value.i = va_arg(list, int);
            break;
        default:
            errno = ENOSYS;
            return 1;
    }
    va_end(list);

    return 0;
}

int htext_addheader(htext_handle *handle, const char *header, const char *value)
{
    size_t bufsize = strlen(header) + strlen(value) + 3;
    char *buffer = calloc(1, bufsize);
    snprintf(buffer, bufsize, "%s: %s", header, value);
    handle->headers = curl_slist_append(handle->headers, buffer);
    free(buffer);
    return 0;
}

int htext_perform(htext_handle *handle)
{
    void* (*performer)(void *);
    const char *performer_str;
    int i;

    /* Check status */
    switch (handle->status) {
        case HTEXTS_WAITING:
        case HTEXTS_STARTING:
        case HTEXTS_RUNNING:
            errno = EBADFD;
            return 1;
    }

    /* Free internal pointers, in case they were used already */
    free(handle->error_string);
    free(handle->partial_total);
    free(handle->partial_done);
    for (i = 0; i < handle->partials; ++i) {
        free(handle->partial_rconn[i]);
    }
    free(handle->partial_rconn);
    handle->error_string = NULL;
    handle->partial_done = NULL;
    handle->partial_total = NULL;
    handle->partial_rconn = NULL;
    handle->partials = 0;

    /* Check we have source and destination */
    if (!GETSTR(handle, HTEXTOP_SOURCEURL)) {
        htext_error(handle, "Source not specified");
        errno = EINVAL;
        return 1;
    }
    if (!GETSTR(handle, HTEXTOP_DESTINATIONURL)) {
        htext_error(handle, "Destination not specified");
        errno = EINVAL;
        return 1;
    }

    /* Guess the method */
    if (is_remote(GETSTR(handle, HTEXTOP_SOURCEURL))) {
        if(is_remote(GETSTR(handle, HTEXTOP_DESTINATIONURL)))
        handle->http_method = HTTP_COPY;
        else
        handle->http_method = HTTP_GET;
    }
    else {
        if(is_remote(GETSTR(handle, HTEXTOP_DESTINATIONURL)))
        handle->http_method = HTTP_PUT;
        else
        handle->http_method = HTTP_UNSUPPORTED;
    }

    /* Delegate */
    switch (handle->http_method) {
        case HTTP_GET:
            performer = htext_get_method;
            performer_str = "GET";
            break;
        case HTTP_PUT:
            performer = htext_put_method;
            performer_str = "PUT";
            break;
        case HTTP_COPY:
            performer = htext_copy_method;
            performer_str = "COPY";
            break;
        default:
            errno = ENOSYS;
            return 1;
    }

    /* Make sure needed values are there */
    if (GETPTR(handle, HTEXTOP_IO_HANDLER)== NULL)
    htext_setopt(handle, HTEXTOP_IO_HANDLER, &default_io_handler);

    /* Launch thread and return */
    handle->status = HTEXTS_STARTING;
    htext_log(handle, "Delegating to the proper handle (%s %s %s)", performer_str,
              GETSTR(handle, HTEXTOP_SOURCEURL), GETSTR(handle, HTEXTOP_DESTINATIONURL));
    return pthread_create(&(handle->thread), NULL, performer, handle);
}

void htext_abort(htext_handle *handle)
{
    /* TODO: Transfer is aborted by returning non-zero value from progress
       function (this doesn't happen immediately and with curl easy interface
       there is no better option except closing directly transfer socket) */
    handle->status = HTEXTS_ABORTED;
}

int htext_status(htext_handle *handle)
{
    return handle->status;
}

int htext_progress(htext_handle *handle, size_t *n, size_t **total,
        size_t **done, char ***rconn)
{
    if (handle->status == HTEXTS_CREATED) {
        errno = EBADFD;
        return 1;
    }
    if (n)
        *n = handle->partials;
    if (done)
        *done = handle->partial_done;
    if (total)
        *total = handle->partial_total;
    if (rconn)
        *rconn = handle->partial_rconn;
    return 0;
}

const char *htext_error_string(htext_handle *handle)
{
    return handle->error_string;
}

int htext_http_code(htext_handle *handle)
{
    return handle->http_status;
}
