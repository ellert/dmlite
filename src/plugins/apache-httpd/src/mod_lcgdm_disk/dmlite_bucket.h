/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DMLITE_BUCKET_H_
#define	DMLITE_BUCKET_H_

#include <apr_buckets.h>
#include <dmlite/c/io.h>

/* 4MB */
#define MAX_BUCKET_SIZE (0x400000)

struct apr_bucket_dmlite
{
    apr_bucket_refcount refcount;
    dmlite_fd *fd;
    apr_pool_t *pool;
};
typedef struct apr_bucket_dmlite apr_bucket_dmlite;

/** Hooks */
extern const apr_bucket_type_t apr_bucket_type_dmlite;

/**
 * Behaves as apr_bucket_file_create, but using dm_fd instead.
 * @note It will take care of closing fd.
 */
apr_bucket* dmlite_bucket_create(dmlite_fd *fd, apr_off_t offset,
        apr_size_t lenght, apr_pool_t *p, apr_bucket_alloc_t *list);

/**
 * Behaves as apr_brigade_insert_file, but using dm_fd instead.
 * @note It will take care of closing fd.
 */
apr_bucket* apr_brigade_insert_dmlite(apr_bucket_brigade *bb, dmlite_fd *fd,
        apr_off_t start, apr_off_t length, apr_pool_t *p);

#endif	/* DMLITE_BUCKET_H_ */
