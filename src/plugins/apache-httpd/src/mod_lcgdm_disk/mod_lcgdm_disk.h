/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_DAV_DPM_H_
#define MOD_DAV_DPM_H_

#include <apr_tables.h>
#include <dmlite/c/dmlite.h>
#include <dmlite/c/io.h>
#include <httpd.h>
#include <http_config.h>
#include <mod_lcgdm_dav.h>
#include "../shared/security.h"
#include "use_module.h"

#define DAV_DISK_WRITE       0x01
#define DAV_DISK_REMOTE_COPY 0x02
#define DAV_DISK_NOAUTHN     0x04

typedef unsigned char dir_flags;

/* Data that needs to be available outside. */
extern module AP_MODULE_DECLARE_DATA lcgdm_disk_module;
extern const dav_hooks_repository dav_disk_hooks_repository;
extern const dav_liveprop_group dav_disk_liveprop_group;
extern const dav_hooks_db dav_disk_hooks_db;

/** Data structure for server configuration. */
struct dav_disk_server_conf
{
    dmlite_manager *manager;
};
typedef struct dav_disk_server_conf dav_disk_server_conf;

/** Data structure for directory configuration. */
struct dav_disk_dir_conf
{
    const char *info_key;
    dmlite_manager *manager;
    char *anon_user, *anon_group;
    const char *proxy_cache;
    dir_flags flags;
    const char *delegation_service;
    const char *capath;
    const char *cafile;
    const char *crlpath;
    const char *crlfile;
    const char *crlcheck;
    int crlmissok;
    int low_speed_time;
    int low_speed_limit;
};
typedef struct dav_disk_dir_conf dav_disk_dir_conf;

/**
 * @brief This structure handles the data the module needs to keep track of
 *        a resource coming from a DPM service typedef done in mod_dav.h.
 */
struct dav_resource_private
{
    request_rec *request;
    dav_disk_server_conf *s_conf;
    dav_disk_dir_conf *d_conf;
    dmlite_context *ctx;
    int method;
    size_t fsize;
    dmlite_location loc;
    dmlite_fd *fd;
    int copy_already_redirected;
    char *namespace_path;
    const char *content_type;
    const char *content_disposition;
    const char *query_str;
};

/**
 * Insert all supported liveprops into the output.
 * @param r        The request.
 * @param resource The resource being processed.
 * @param what     What to insert.
 * @param phdr     Where to insert.
 */
void dav_disk_insert_all_liveprops(request_rec *r, const dav_resource *resource,
        dav_prop_insert what, apr_text_header *phdr);

/**
 * Find the property ID so the value can be retrieved later.
 * @param resource The resource being processed.
 * @param ns_uri   The property namespace (e.g. DAV:)
 * @param name     The property name      (e.g. getcontentlength)
 * @param hooks    Where to put the liveprop hooks.
 * @return         The property ID, or 0 if not found.
 */
int dav_disk_find_liveprop(const dav_resource *resource, const char *ns_uri,
        const char *name, const dav_hooks_liveprop **hooks);

/**
 * Performs a remote copy.
 * @param src    The original resource.
 * @param dst    The remote URL.
 * @return       NULL on success.
 */
dav_error *dav_disk_remote_copy(const dav_resource *src, const char *dst);

/**
 * Performs a remote fetch.
 * @param src    The original resource.
 * @param dst    The local resource.
 * @return       NULL on success.
 */
dav_error *dav_disk_remote_fetch(const char *src, const dav_resource *dst);

/**
 * Get the location of a file, and set the redirect if needed.
 * @param info Input/output parameter. sfn must be set. location will be set.
 * @param pool Pool for memory allocation.
 * @return     NULL on success.
 */
dav_error *dav_disk_get_location(dav_resource_private *info, apr_pool_t *pool);

/**
 * Generates the checksum for the given file
 * @return 1 if the checksums were put into output
 */
dav_error* dav_disk_digest_header(request_rec *r, const dav_resource *resource,
        char* output, size_t outsize);

#endif
