/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/checksums.h>
#include <httpd.h>
#include <regex.h>
#include "shared/utils.h"
#include <http_log.h>
#include "mod_lcgdm_disk.h"


dav_error* dav_disk_digest_header(request_rec *r, const dav_resource *resource,
        char* output, size_t outsize)
{
    char sorted_digests[1024], *digest_name, *first_digest_name = NULL;
    char digest[1024], full_digest_name[64];
    int chksum_timeout = -1; // negative time can be interpreted as a request for already calculated checksum value

    dav_shared_sorted_digests(resource->info->request, sorted_digests, 1024);
    if (strlen(sorted_digests) == 0) {
        return NULL;
    };

    digest_name = strtok(sorted_digests, ",");
    while (digest_name) {
        snprintf(full_digest_name, sizeof(full_digest_name), "checksum.%s", digest_name);

        int ret = dmlite_getchecksum(resource->info->ctx,
            resource->info->namespace_path, // logical name
            full_digest_name, digest, sizeof(digest),
            resource->info->loc.chunks[0].url.path, // replica
            0, // 0 = do not recalculate
            chksum_timeout // negative time can be interpreted as a request for already calculated checksum value
            );

        if (ret == 0 && digest[0] != '\0') {
          // Beware, the representation for some checksum types
          // has to be base64
          
          char digest_base64_value[1024];
          size_t digest_length = dav_shared_hexdigesttobase64(digest_name,
                                                              digest,
                                                              digest_base64_value);
          if (!digest_length) {
            return dav_shared_new_error(resource->info->request, resource->info->ctx,
                                        HTTP_INTERNAL_SERVER_ERROR, "Cannot handle digest '%s:%s'", full_digest_name, digest);
          }
          
          
          
          size_t len;
          len = snprintf(output, outsize, "%s=%s,", digest_name, digest_base64_value);
          output += len;
          outsize -= len;
          dmlite_fseek(resource->info->fd, 0, SEEK_SET);
        } else if (DMLITE_ERRNO(dmlite_errno(resource->info->ctx)) == EAGAIN) {
            return dav_shared_new_error(resource->info->request, resource->info->ctx,
                202, "Checksum not available yet. Try again later.");
        } else if (dmlite_errno(resource->info->ctx) != 0) {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, resource->info->request,
                "Failed to get the checksum %s: %s", digest_name, dmlite_error(resource->info->ctx));
        } else {
            if (!first_digest_name) {
                // first checksum with empty value signalize it may be necessary
                // to calculate missing checksum in case we don't find any other
                // that was already calculated and it is now stored in database
                first_digest_name = digest_name;
            } else if (!(chksum_timeout < 0)) {
                // only report problems in case of an attempt to calculate missing checksum
                ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, resource->info->request,
                    "Failed to get the checksum %s: empty value", digest_name);
            }
        }

        // forced checksum calculation is the last step after we already processed all digest names
        if (!(chksum_timeout < 0))
            break;

        digest_name = strtok(NULL, ",");
        if (!digest_name && first_digest_name) {
            digest_name = first_digest_name;
            chksum_timeout = 0; // calculate missing checksum with default timeout
        }
    }

    if (*(output-1) == ',') {
        --output;
    }
    *output = '\0';

    return NULL;
}
