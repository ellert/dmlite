/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_file_io.h>
#include <apr_strings.h>
#include <ctype.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/pool.h>
#include <httpd.h>
#include <http_config.h>
#include <http_log.h>
#include <http_protocol.h>
#include <http_request.h>
#include <inttypes.h>

/* For core_dir_config */
#define CORE_PRIVATE
#include <http_core.h>

#include "dmlite_bucket.h"
#include "mod_lcgdm_disk.h"
#include "../shared/utils.h"

/** Internal structure for streams (typedef by mod_dav.h) */
struct dav_stream
{
    const dav_resource *resource;
    dmlite_fd *fd;
    unsigned has_range;
    apr_size_t written;
};

/**
 * As strcmp, but check for NULLS
 */
static int safe_strcmp(const char* a, const char* b)
{
    if (a == NULL && b == NULL)
        return 0;
    else if (a == NULL)
        return -1;
    else if (b == NULL)
        return 1;
    else
        return strcmp(a, b);
}

/**
 * Return the parent path of "path"
 * @param pool Where to allocate space
 * @param path The path
 * @return     The parent path
 */
static char *dav_disk_dirname(apr_pool_t *pool, const char *path)
{
    char *parent;
    int i;

    parent = apr_pstrcat(pool, path, NULL );

    /* We skip the last character so if path is a directory, we are fine */
    for (i = strlen(parent) - 2; i >= 0; --i) {
        if (parent[i] == '/') {
            parent[i + 1] = '\0'; /* Keep the slash */
            break;
        }
    }

    return parent;
}

/**
 * Used internally to populate the extra parameters
 * @param udata
 * @param key
 * @param value
 * @return
 */
static int populate_extra(void *udata, const char *key, const char *value)
{
    dav_resource_private *info = (dav_resource_private*) udata;
    dmlite_any* any;

    char *unescaped = apr_pstrdup(info->request->pool, value);
    ap_unescape_url(unescaped);

    any = dmlite_any_new_string(unescaped);
    dmlite_any_dict_insert(info->loc.chunks[0].url.query, key, any);
    dmlite_any_free(any);

    return 1;
}

/**
 * Finishes the PUT, and cleans the cached file
 */
static int dav_finish_writing(dav_resource_private* info)
{
    int e = dmlite_donewriting(info->ctx, &info->loc);
    if (e)
        dmlite_put_abort(info->ctx, &info->loc);
    
    if (info->fd) {
        apr_pool_cleanup_kill(info->request->connection->pool, info->fd, dav_shared_fclose);
        dmlite_fclose(info->fd);
        info->fd = NULL;
    }
    apr_table_unset(info->request->connection->notes, info->d_conf->info_key);
    return e;
}

/**
 * This function is used internally to create new resources.
 * @param r          The request to associate.
 * @param pfn        The PFN to link to the resource.
 * @param uri        The URI (without the query!)
 * @param resource   Where to put it.
 * @return           NULL on success.
 */
static dav_error *dav_disk_internal_get_resource(request_rec *r,
        const char *pfn, dav_resource **resource)
{
    dav_resource_private *info;
    apr_table_t *query;
    unsigned nargs;
    const char *sfn;

    /* Create the and initialize the resource */
    *resource = apr_pcalloc(r->pool, sizeof(dav_resource));
    (*resource)->type = DAV_RESOURCE_TYPE_REGULAR;
    (*resource)->exists = 0;
    (*resource)->collection = 0;
    (*resource)->uri = pfn;
    (*resource)->info = NULL;
    (*resource)->hooks = &dav_disk_hooks_repository;
    (*resource)->pool = r->pool;

    /* Try to re-use a previous one */
    dav_disk_dir_conf* d_conf = ap_get_module_config(r->per_dir_config, &lcgdm_disk_module);

    info = (dav_resource_private*) apr_table_get(r->connection->notes, d_conf->info_key);
    if (info != NULL) {
        info->request = r;
        (*resource)->info = info;

        /* If the PFN, query or the method does not match, close file descriptor and keep going */
        if (safe_strcmp(info->loc.chunks[0].url.path, pfn) != 0
                || safe_strcmp(info->query_str, r->parsed_uri.query) != 0
                || info->method != r->method_number) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "Recovered info can not be reused. Cleaning up.");

            apr_pool_cleanup_kill(r->connection->pool, info->fd, dav_shared_fclose);
            dmlite_fclose(info->fd);
            info->fd = NULL;
            (*resource)->exists = 0;
        }
        /* The file descriptor may be NULL because the file didn't exist! */
        else if (info->fd == NULL) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "Can reuse the recovered info, but the file did not exist");
            (*resource)->exists = 0;
            return NULL;
        }
        /* If they do, just adjust the request and return */
        else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                    "Can reuse the recovered info");
            (*resource)->exists = 1;
            return NULL ;
        }
    }
    else {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "No recovered info from the connection");
        info = apr_pcalloc(r->connection->pool, sizeof(dav_resource_private));
        (*resource)->info = info;

        info->loc.chunks =
                apr_pcalloc(r->connection->pool, sizeof(dmlite_chunk));
    }

    /* (Re?)Initialize info */
    info->request = r;
    info->method = r->method_number;
    info->query_str = apr_pstrdup(r->connection->pool, r->parsed_uri.query);
    info->loc.nchunks = 1;
    strncpy(info->loc.chunks[0].url.path, pfn, PATH_MAX);
    info->loc.chunks[0].url.path[PATH_MAX - 1] = '\0';

    info->s_conf = ap_get_module_config(r->server->module_config, &lcgdm_disk_module) ;
    info->d_conf = d_conf;

    /* Instantiate the context */
    if (info->ctx == NULL) {
        dmlite_manager* manager;
        if (info->d_conf->manager)
            manager = info->d_conf->manager;
        else if (info->s_conf->manager)
            manager = info->s_conf->manager;
        else
            return dav_shared_new_error(r, NULL, HTTP_INTERNAL_SERVER_ERROR,
                    "No dmlite manager found. Probably the server is misconfigured.");

        info->ctx = dmlite_context_new(manager);
        if (info->ctx == NULL ) {
            return dav_shared_new_error(r, NULL, HTTP_INTERNAL_SERVER_ERROR,
                    "Could not instantiate a context: %s",
                    dmlite_manager_error(manager));
        }
        dmlite_any* protocol;
        if (is_ssl_used(info->request))
            protocol = dmlite_any_new_string("https");
        else
            protocol = dmlite_any_new_string("http");
        dmlite_set(info->ctx, "protocol", protocol);
        dmlite_any_free(protocol);

        apr_pool_pre_cleanup_register(r->connection->pool,
                info->ctx, dav_shared_context_free);

        if (!(info->d_conf->flags & DAV_DISK_NOAUTHN)) {
            apr_pool_t *subpool;
            dmlite_credentials *user;

            apr_pool_create(&subpool, r->pool);
            user = dav_shared_get_user_credentials(subpool, r,
                    info->d_conf->anon_user, info->d_conf->anon_group, NULL );
            if (!user) {
                return dav_shared_new_error(r, NULL, HTTP_FORBIDDEN,
                        "Can not authenticate the user");
            }

            if (dmlite_setcredentials(info->ctx, user) != 0) {
                return dav_shared_new_error(r, info->ctx, HTTP_FORBIDDEN,
                        "Could not set credentials");
            }
            dmlite_any_dict_free(user->extra);
            apr_pool_destroy(subpool);
        }
    }

    query = dav_shared_parse_query(r->pool, r->parsed_uri.query, &nargs);

    /* Check the existence of copyRedirected */
    if (apr_table_get(query, "copyRedirected") != NULL ) {
        info->copy_already_redirected = 1;
        apr_table_unset(query, "copyRedirected");
    }

    /* Namespace URL */
    sfn = apr_table_get(query, "dav_sfn");
    if (sfn != NULL ) {
        info->namespace_path = apr_pstrdup(r->connection->pool, sfn);
        ap_unescape_url(info->namespace_path);
        // this should be passed to dmlite according to LCGDM-1508
        //apr_table_unset(query, "dav_sfn");
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Namespace URL found: %s",
                info->namespace_path);
    }
    else {
        info->namespace_path = NULL;
    }

    /* If we got a sfn, call any registered type checker to get
     * the mime-type. Then, cache it to avoid reprocessing if the same
     * file is accessed */
    if (r->method_number == M_GET && info->namespace_path) {
        r->filename = info->namespace_path;
        ap_run_type_checker(r);
        info->content_type = apr_pstrdup(r->connection->pool, r->content_type);

        /* Create the content-disposition too */
        const char *fname = strrchr(info->namespace_path, '/');
        if (fname)
            fname++;
        else
            fname = info->namespace_path;

        info->content_disposition = apr_pstrcat(r->connection->pool,
                "filename=\"", fname, "\"", NULL );
    }

    /* Populate key/value array used by dmlite_fopen */
    if (nargs > 0) {
        info->loc.chunks[0].url.query = dmlite_any_dict_new();
        apr_table_do(populate_extra, info, query, NULL );
        apr_pool_pre_cleanup_register(r->connection->pool,
                info->loc.chunks[0].url.query, dav_shared_dict_free);
    }
    else {
        info->loc.chunks[0].url.query = NULL;
    }

    /* Open for read only by default */
    info->fd = dmlite_fopen(info->ctx, info->loc.chunks[0].url.path, O_RDONLY,
            info->loc.chunks[0].url.query);
    if (info->fd == NULL) {
        switch (dmlite_errno(info->ctx)) {
            case ENOENT:
                (*resource)->exists = 0;
                break;
            case EACCES:
                return dav_shared_new_error(r, info->ctx, HTTP_FORBIDDEN,
                        "Access forbidden for %s", info->loc.chunks[0].url.path);
            default:
                return dav_shared_new_error(r, info->ctx,
                        HTTP_INTERNAL_SERVER_ERROR, "Can not stat %s",
                        info->loc.chunks[0].url.path);
        }
    }
    else {
        struct stat fstat;

        (*resource)->exists = 1;

        dmlite_fstat(info->fd, &fstat);
        info->fsize = fstat.st_size;

        /* Keep it open as long as the connection lives */
        apr_pool_pre_cleanup_register(r->connection->pool, info->fd, dav_shared_fclose);
    }

    /* Cache the info, but not for COPY (need to re-process on redirection) */
    if (r->method_number != M_COPY)
        apr_table_setn(r->connection->notes, d_conf->info_key, (char*) info);

    /* Log and exit */
    if ((*resource)->exists)
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "Resource for %s (%s) found (dir=%d)", (*resource)->uri,
                (*resource)->info->loc.chunks[0].url.path,
                (*resource)->collection);
    else
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                "NULL resource for %s (%s) created", (*resource)->uri,
                (*resource)->info->loc.chunks[0].url.path);

    return NULL ;
}

/**
 * Generate the resource used for later operations
 * First security checkings can be done here (like user mapping)
 * @param r                The request coming from Apache
 * @param root_dir         The root dir of the request (as configured in
 *                         Apache with Location)
 * @param label            Used with version control. Not relevant.
 * @param use_checked_in   Used with version control. Not relevant.
 * @param resource         Where to put the generated resource
 * @return                 NULL on success. A dav_error instance if not.
 */
static dav_error *dav_disk_get_resource(request_rec *r, const char *root_dir,
        const char *label, int use_checked_in, dav_resource **resource)
{
    (void) label;
    (void) use_checked_in;

    char *pfn;
    dav_error *err;
    int len;
    const char *x_header;

    /* Compose path */
    len = strlen(root_dir);
    if (root_dir[len - 1] == '/') {
        char *tmp = apr_pstrdup(r->pool, root_dir);
        --len;
        tmp[len] = '\0';
        root_dir = tmp;
    }

    pfn = r->parsed_uri.path ? r->parsed_uri.path : "";

    /* Create resource */
    err = dav_disk_internal_get_resource(r, pfn, resource);
    if (err)
        return err;

    /* Forbidden directories! */
    if ((*resource)->collection)
        return dav_shared_new_error(r, NULL, HTTP_FORBIDDEN,
                "The disk node can not serve directories");

    /* If we are here, the file can be copies */
    dav_lcgdm_notify_support_external_copy(r);

    /* If it is a GET/HEAD, check the X-Multistreams header to see if we must
     * close */
    if (r->method_number == M_GET) {
        x_header = apr_table_get(r->headers_in, "X-Multistreams");
        if (x_header != NULL && strcasecmp("close", x_header) == 0) {
            int e;
            dav_resource_private *info = (*resource)->info;

            e = dav_finish_writing(info);

            if (e != 0)
                return dav_shared_new_error(r, info->ctx,
                        HTTP_INTERNAL_SERVER_ERROR,
                        "Failed when closing the writing through HEAD %s (%d)",
                        info->loc.chunks[0].url.path, e);
        }
    }

    /* We are done here */
    return NULL ;
}

/**
 * Get the parent of a resource (if any)
 * @param resource        The resource to process
 * @param parent_resource The parent, if any
 * @return                NULL on success
 */
static dav_error *dav_disk_get_parent(const dav_resource *resource,
        dav_resource **parent_resource)
{
    char *pfn;
    request_rec *dup_rec;

    /* The parent of the root is itself (kinda) */
    if (strcmp(resource->info->loc.chunks[0].url.path, "/") == 0) {
        *parent_resource = (dav_resource*) resource;
        return NULL ;
    }

    /* Strip last element of the path */
    pfn = dav_disk_dirname(resource->pool,
            resource->info->loc.chunks[0].url.path);

    /* Duplicate request */
    dup_rec = apr_pcalloc(resource->pool, sizeof(request_rec));
    *dup_rec = *resource->info->request;
    dup_rec->parsed_uri.query = NULL;
    dup_rec->method_number = M_GET;

    /* Since we are isolated from the backend storage system, we have to
     * assume we can access (we will fail later if not) and that it does exist
     * (plugin must create it at some point, whether on the whereToWrite or on the
     *  opening of the file).
     */
    dav_resource_private *info =
            apr_pcalloc(resource->pool, sizeof(dav_resource_private));

    info->loc.nchunks = 1;
    info->loc.chunks = apr_pcalloc(resource->pool, sizeof(dmlite_chunk));

    info->ctx = resource->info->ctx;
    info->d_conf = resource->info->d_conf;
    strncpy(info->loc.chunks[0].url.path, pfn,
            sizeof(info->loc.chunks[0].url.path));
    info->loc.chunks[0].url.path[sizeof(info->loc.chunks[0].url.path) - 1] =
            '\0';
    info->request = resource->info->request;
    info->s_conf = resource->info->s_conf;
    info->fsize = 0;
    info->fd = 0;

    *parent_resource = apr_pcalloc(resource->pool, sizeof(dav_resource));
    (*parent_resource)->type = DAV_RESOURCE_TYPE_REGULAR;
    (*parent_resource)->exists = 1;
    (*parent_resource)->collection = 1;
    (*parent_resource)->uri = pfn;
    (*parent_resource)->info = info;
    (*parent_resource)->hooks = &dav_disk_hooks_repository;
    (*parent_resource)->pool = resource->pool;

    return NULL ;
}

/**
 * Compare two resources to check if they are the same
 * @param res1 Resource 1
 * @param res2 Resource 2
 * @return     1 if they are the same
 */
static int dav_disk_is_same(const dav_resource *res1, const dav_resource *res2)
{
    const char *destination;
    apr_uri_t uri1, uri2;

    /* Parse resource 1 */
    apr_uri_parse(res1->pool, res1->uri, &uri1);

    /* Parse resource 2 */
    destination = apr_table_get(res2->info->request->headers_in, "destination");
    if (destination)
        apr_uri_parse(res2->pool, destination, &uri2);
    else
        apr_uri_parse(res2->pool, res2->uri, &uri2);

    /* Host may not appear */
    if (!uri1.hostname)
        uri1.hostname = res1->info->request->server->server_hostname;
    if (!uri2.hostname)
        uri2.hostname = res2->info->request->server->server_hostname;

    /* Compare */
    return strcmp(uri1.hostname, uri2.hostname) == 0
            && strcmp(uri1.path, uri2.path) == 0;
}

/**
 * Check if a given resource is the parent of another one
 * @param res1 Parent to check
 * @param res2 Child
 * @return     1 if res1 is the parent of res2
 */
static int dav_disk_is_parent(const dav_resource *res1,
        const dav_resource *res2)
{
    char *res2_parent_sfn;

    res2_parent_sfn = dav_disk_dirname(res2->pool,
            res2->info->loc.chunks[0].url.path);

    return strcmp(res1->info->loc.chunks[0].url.path, res2_parent_sfn) == 0;
}

/**
 * Open a stream. Used for PUT
 * @param resource The resource to open
 * @param mode     The mode (basically, trucate or random, which we don't support)
 * @param stream   Where to put the created structure
 * @return         NULL on success
 */
static dav_error *dav_disk_open_stream(const dav_resource *resource,
        dav_stream_mode mode, dav_stream **stream)
{
    (void) mode;

    dav_resource_private *info = resource->info;;

    if (!(info->d_conf->flags & DAV_DISK_WRITE)) {
        return dav_shared_new_error(info->request, info->ctx,
                HTTP_FORBIDDEN, "Write mode disabled");
    }

    /*  Range header? */
    unsigned has_range;
    const char *range;

    range = apr_table_get(info->request->headers_in, "content-range");
    has_range = (range != NULL );
    if (has_range) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request, "Range: %s",
                range);
    }

    /* This is the destination, so create an internal descriptor
     * and there we go */
    *stream = apr_pcalloc(resource->pool, sizeof(dav_stream));
    (*stream)->resource = resource;
    (*stream)->has_range = has_range;
    (*stream)->written = 0;

    (*stream)->fd = dmlite_fopen(info->ctx, info->loc.chunks[0].url.path,
            O_WRONLY | O_CREAT, info->loc.chunks[0].url.query, 0660);
    if ((*stream)->fd == NULL ) {
        return dav_shared_new_error(info->request, info->ctx,
                HTTP_INTERNAL_SERVER_ERROR, "Could not open %s",
                resource->info->loc.chunks[0].url.path);
    }

    return NULL ;
}

/**
 * Close a stream
 * @param stream The stream to close
 * @param commit Commit the change
 * @return       NULL on success
 */
static dav_error *dav_disk_close_stream(dav_stream *stream, int commit)
{
    dav_resource_private *info;

    dmlite_fclose(stream->fd);
    stream->fd = NULL;
    info = stream->resource->info;

    if (commit && !stream->has_range) {
        /* Commit the change */
        int e;

        e = dav_finish_writing(info);

        if (e != 0) {
            return dav_shared_new_error(info->request, info->ctx,
                HTTP_INTERNAL_SERVER_ERROR,
                "Failed to close the writing for %s (%d)",
                info->loc.chunks[0].url.path, e);
        }

        /* That is! */
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
                "dmlite_donewriting on %s (%zu written)",
                info->loc.chunks[0].url.path, stream->written);
    }
    else if (!commit) {
        /* Remove the file */
        /* Won't work with files that are not in the filesystem */
        /* apr_file_remove(info->pfn, stream->resource->pool); */
    }
    else {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
                "Commit with range, so do not finish yet (%zu written)",
                stream->written);
    }

    return NULL ;
}

/**
 * Write to a stream
 * @param stream  The stream where to write
 * @param buf     The data to write
 * @param bufsize How much data there is in the buffer
 * @return        NULL on success
 */
static dav_error *dav_disk_write_stream(dav_stream *stream, const void *buf,
        apr_size_t bufsize)
{
    if (dmlite_fwrite(stream->fd, buf, bufsize) < 0) {
        return dav_shared_new_error(stream->resource->info->request,
                stream->resource->info->ctx, HTTP_INTERNAL_SERVER_ERROR,
                "Error writing to %s",
                stream->resource->info->loc.chunks[0].url.path);
    }
    stream->written += bufsize;
    return NULL ;
}

/**
 * Moves to an specific position within a stream
 * @param stream       The stream to move
 * @param abs_position Where to move
 * @return             NULL on success
 */
static dav_error *dav_disk_seek_stream(dav_stream *stream,
        apr_off_t abs_position)
{
    if (dmlite_fseek(stream->fd, abs_position, SEEK_SET) != 0) {
        return dav_shared_new_error(stream->resource->info->request,
                stream->resource->info->ctx, HTTP_INTERNAL_SERVER_ERROR,
                "Error seeking %s (%lu)",
                stream->resource->info->loc.chunks[0].url.path, abs_position);
    }
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, stream->resource->info->request,
            "Seek %s (%lu)", stream->resource->info->loc.chunks[0].url.path,
            (unsigned long) abs_position);

    return NULL ;
}

#include <http_request.h>

/**
 * Sets the HTTP headers, like the size of the content, the modification date...
 * @param r        The request
 * @param resource The resource generated previously
 * @return         NULL on success
 */
static dav_error *dav_disk_set_headers(request_rec *r,
        const dav_resource *resource)
{
    char buffer[512] = {0};

    if (!resource->exists) {
        return NULL;
    }

    ap_set_content_length(r, resource->info->fsize);

    dav_error *error = dav_disk_digest_header(r, resource, buffer, sizeof(buffer));
    if (error != NULL) {
        return error;
    }
    if (*buffer != '\0') {
        apr_table_set(r->headers_out, "Digest", buffer);
    }

    if (r->content_type == NULL )
        r->content_type = resource->info->content_type;

    if (resource->info->content_disposition)
        apr_table_set(r->headers_out, "Content-Disposition",
                resource->info->content_disposition);

    /* Nice thing is, Apache takes care of implementing this if we use buckets */
    apr_table_setn(r->headers_out, "Accept-Ranges", "bytes");

    return NULL ;
}

/**
 * This is the second most important function, as it delivers the content of the
 * file.
 * @param resource The resource generated previously
 * @param output   Here is where the output must be written
 * @return         NULL on success
 */
static dav_error *dav_disk_deliver(const dav_resource *resource,
        ap_filter_t *output)
{
    apr_bucket_brigade *bb;
    apr_bucket *bkt;
    dav_resource_private *info;
    int fileno;

    if (resource->collection) {
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_FORBIDDEN, 0, "Can not list the content of a disk");
    }

    info = resource->info;
    bb = apr_brigade_create(resource->pool, output->c->bucket_alloc);
    fileno = dmlite_fileno(info->fd);

    /* Apache core dir config */
    core_dir_config *coreconf = (core_dir_config *) ap_get_module_config(
            info->request->per_dir_config, &core_module);

    /* Try to see if we can use sendfile */
    if (fileno > -1 && coreconf->enable_sendfile == ENABLE_SENDFILE_ON) {
        apr_file_t* apr_file = NULL;
        if (apr_os_file_put(&apr_file, &fileno,
                APR_FOPEN_READ | APR_FOPEN_SENDFILE_ENABLED,
                info->request->pool) != APR_SUCCESS) {
            return dav_shared_new_error(resource->info->request, NULL,
                    HTTP_INTERNAL_SERVER_ERROR, 0,
                    "Could not bind the file descriptor to the apr_file");
        }
        // apr_file_close should be used to release apr_file, but this function
        // also close associated filehandle which would be closed second time
        // by dmlite_fclose (called by dav_shared_fclose registered in
        // dav_disk_internal_get_resource). Function apr_os_file_put is called
        // with flags that doesn't currently allocate any additioanal resources
        // and it is therefore safe (but still a bit danger) not to register
        // apr_file_close clenup function
        //apr_pool_pre_cleanup_register(info->request->pool, apr_file,
        //        (apr_status_t (*)(void *))apr_file_close);

        /* Split in chunks sendfile can handle
         * Adapted from mod_xsendfile */
        apr_off_t length = info->fsize;
        if (sizeof(apr_off_t) == sizeof(apr_size_t) || length < AP_MAX_SENDFILE) {
            bkt = apr_bucket_file_create(apr_file, 0, length,
                    info->request->pool, bb->bucket_alloc);
        }
        else {
            bkt = apr_bucket_file_create(apr_file, 0, AP_MAX_SENDFILE,
                    info->request->pool, bb->bucket_alloc);

            while (length > AP_MAX_SENDFILE) {
                apr_bucket *ce;
                apr_bucket_copy(bkt, &ce);
                APR_BRIGADE_INSERT_TAIL(bb, ce);
                bkt->start += AP_MAX_SENDFILE;
                length -= AP_MAX_SENDFILE;
            }

            bkt->length = (apr_size_t) length;
        }

        APR_BRIGADE_INSERT_TAIL(bb, bkt);

        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                "Sending %s using sendfile", resource->uri);
    }
    /* Else, fallback to dmlite bucket implementation */
    else {
        bkt = apr_brigade_insert_dmlite(bb, info->fd, 0, info->fsize, resource->pool);

        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                "Sending %s using dmlite IO", resource->uri);
    }

    /* MMAP */
    if (coreconf->enable_mmap == ENABLE_MMAP_ON) {
      apr_bucket_file_enable_mmap(bkt, 0);
    }

    /* Close and flush the output */
    bkt = apr_bucket_eos_create(output->c->bucket_alloc);
    APR_BRIGADE_INSERT_TAIL(bb, bkt);
    if (ap_pass_brigade(output, bb) != APR_SUCCESS)
        return dav_shared_new_error(info->request, NULL,
                HTTP_INTERNAL_SERVER_ERROR, "Could not write EOS to filter.");

    /* All OK */
    return NULL ;
}

/**
 * Create a new collection (i.e. directory)
 * @param resource The resource to be created
 * @return         NULL on success
 */
static dav_error *dav_disk_create_collection(dav_resource *resource)
{
    return dav_shared_new_error(resource->info->request, NULL, HTTP_FORBIDDEN,
            0, "Can not create collections on a disk");
}

/**
 * Trigger a remote copy.
 * @param src       The source. It must exist
 * @param dst       The destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error *dav_disk_remote_copy_hook(const dav_resource *src, const char *dst,
        dav_response **response)
{
    *response = NULL;
#ifdef BUILD_HTCOPY
    return dav_disk_remote_copy(src, dst);
#else
    (void) dst;
    return dav_shared_new_error(src->info->request, NULL,
            HTTP_METHOD_NOT_ALLOWED, "lcgdm-dav compiled with HTCOPY disabled");
#endif
}

/**
 * Trigger a remote fetch
 * @param src       The remote source.
 * @param dst       The local destination.
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error* dav_disk_remote_fetch_hook( const char *src,
        const dav_resource *dst, dav_response **response)
{
    *response = NULL;
#ifdef BUILD_HTCOPY
    return dav_disk_remote_fetch(src, dst);
#else
    (void) dst;
    return dav_shared_new_error(dst->info->request, NULL,
            HTTP_METHOD_NOT_ALLOWED, "lcgdm-dav compiled with HTCOPY disabled");
#endif
}

/**
 * Local copies not allowed.
 * @param src       The source. It must exist
 * @param dst       The destination. Must not exist.
 * @param depth     Copy in depth? (Collections)
 * @param response  The response to the copy process
 * @return          NULL on success
 */
static dav_error *dav_disk_copy(const dav_resource *src, dav_resource *dst,
        int depth, dav_response **response)
{
    (void) response;
    return dav_disk_remote_copy_hook(src, dst->uri, response);
}

/**
 * Move a resource
 * @param src       The source. It must exist
 * @param dst       The destination. It must not exist
 * @param response  The response to the move process
 * @return          NULL on success
 */
static dav_error *dav_disk_move(dav_resource *src, dav_resource *dst,
        dav_response **response)
{
    (void) dst;
    (void) response;

    return dav_shared_new_error(src->info->request, NULL, HTTP_FORBIDDEN,
            "Can not rename the content of a disk");
}

/**
 * Remove a resource
 * @param resource The resource to remove
 * @param response The response to the request
 * @return         NULL on success
 */
static dav_error *dav_disk_remove(dav_resource *resource,
        dav_response **response)
{
    (void) resource;
    (void) response;
    return dav_shared_new_error(resource->info->request, NULL, HTTP_FORBIDDEN,
            "Can not remove the content of a disk");
}

/**
 * Walk through the file system
 * @param params    Walk parameters
 * @param depth     In depth?
 * @param response  The response
 * @return          NULL on success
 */
static dav_error *dav_disk_walk(const dav_walk_params *params, int depth,
        dav_response **response)
{
    (void) depth;
    (void) response;
    return dav_shared_new_error(params->root->info->request, NULL,
            HTTP_FORBIDDEN, "Can not list the content of a disk");
}

/**
 * Dummy etag
 */
static const char* dav_disk_getetag(const dav_resource *resource)
{
    (void) resource;
    return "";
}

/** Hooks */
const dav_hooks_repository dav_disk_hooks_repository = {
    1, 0,
    dav_disk_get_resource,
    dav_disk_get_parent,
    dav_disk_is_same,
    dav_disk_is_parent,
    dav_disk_open_stream,
    dav_disk_close_stream,
    dav_disk_write_stream,
    dav_disk_seek_stream,
    dav_disk_set_headers,
    dav_disk_deliver,
    dav_disk_create_collection,
    dav_disk_copy,
    dav_disk_move,
    dav_disk_remove,
    dav_disk_walk,
    dav_disk_getetag,
    NULL,
    dav_disk_remote_copy_hook,
    dav_disk_remote_fetch_hook
};
