/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <gridsite.h>
#include <httpd.h>
#include <http_log.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <time.h>
#include "delegation.h"

#if OPENSSL_VERSION_NUMBER < 0x10100000L
EVP_MD_CTX *EVP_MD_CTX_new(void)
{
    return calloc(1, sizeof(EVP_MD_CTX));
}

void EVP_MD_CTX_free(EVP_MD_CTX *ctx)
{
    EVP_MD_CTX_cleanup(ctx);
    free(ctx);
}

const unsigned char * ASN1_STRING_get0_data(ASN1_STRING *x)
{
    return ASN1_STRING_data(x);
}
#endif

typedef enum
{
    VALID = 0, NOTFOUND, BADFORMAT, EXPIRED, START_FUTURE, EXPIRES_SOON
} cert_validation;

char *dav_deleg_client_name_encode(apr_pool_t *pool, const char *in)
{
    char *out, *q;
    const char *p;
    int outlen;

    outlen = 3 * strlen(in) + 4;
    out = apr_pcalloc(pool, outlen);

    p = in;
    q = out;

    snprintf(q, outlen, "%%3A");
    q += 3;
    outlen -= 3;

    while (*p != '\0') {
        if (isalnum(*p) || (*p == '.') || (*p == '_') || (*p == '-')) {
            *q = *p;
            ++q;
            --outlen;
        }
        else if (*p == ' ') {
            /* Spaces are replaced by + by GridSite */
            snprintf(q, outlen, "%%2B");
            q = &q[3];
            outlen -= 3;
        }
        else if (*p == ':') {
            /* Colons are encoded twice
             * See https://its.cern.ch/jira/browse/LCGDM-2117
             */
            snprintf(q, outlen, "%%253A");
            q = &q[5];
            outlen -= 5;
        }
        else {
            snprintf(q, outlen, "%%%2X", *p);
            q = &q[3];
            outlen -= 3;
        }
        ++p;
    }

    *q = '\0';
    return out;
}

// C&P from GRSTx509MakeDelegationID (GRSTx509MakeDelegationID), since
// that one is not thread safe
// Returns a malloc'd string with Delegation ID made by SHA1-hashing the
// values of the compact credentials exported by mod_gridsite
char* dav_deleg_make_delegation_id(apr_pool_t* pool, apr_table_t* table)
{
    unsigned char hash_delegation_id[EVP_MAX_MD_SIZE];
    int  i;
    unsigned int delegation_id_len;
    char cred_name[14], *delegation_id;
    const char *cred_value;
    const EVP_MD *m;
    EVP_MD_CTX *ctx;

    ctx = EVP_MD_CTX_new();

    m = EVP_sha1();
    if (m == NULL) {
        EVP_MD_CTX_free(ctx);
        return NULL;
    }

    EVP_DigestInit(ctx, m);

    for (i=0; i <= 999; ++i)
    {
        snprintf(cred_name, sizeof(cred_name), "GRST_CRED_%d", i);
        if ((cred_value = apr_table_get(table, cred_name)) == NULL)
            break;
        EVP_DigestUpdate(ctx, cred_value, strlen(cred_value));
    }
    EVP_DigestFinal(ctx, hash_delegation_id, &delegation_id_len);
    EVP_MD_CTX_free(ctx);

    delegation_id = apr_palloc(pool, sizeof(char) * 17);

    for (i=0; i <=7; ++i)
        sprintf(&delegation_id[i*2], "%02x", hash_delegation_id[i]);

    delegation_id[16] = '\0';
    return delegation_id;
}


static time_t ASN1_TIME_2_time_t(ASN1_TIME* asn1_time)
{
    const char* data = (const char*) ASN1_STRING_get0_data(asn1_time);
    size_t len = strlen(data);
    struct tm time_tm;
    char zone = 0;

    memset(&time_tm, 0, sizeof(struct tm));

    if (len == 13
            && sscanf(data, "%02d%02d%02d%02d%02d%02d%c", &(time_tm.tm_year),
                    &(time_tm.tm_mon), &(time_tm.tm_mday), &(time_tm.tm_hour),
                    &(time_tm.tm_min), &(time_tm.tm_sec), &zone) != 7) {
        return 0;
    }
    else if (len == 15
            && sscanf(data, "20%02d%02d%02d%02d%02d%02d%c", &(time_tm.tm_year),
                    &(time_tm.tm_mon), &(time_tm.tm_mday), &(time_tm.tm_hour),
                    &(time_tm.tm_min), &(time_tm.tm_sec), &zone) != 7) {
        return 0;
    }

    if (zone != 'Z')
        return 0;

    if (time_tm.tm_year < 90)
        time_tm.tm_year += 100;
    --(time_tm.tm_mon);

    return timegm(&time_tm);
}

static cert_validation dav_deleg_proxy_is_valid(const char* proxy)
{
    FILE* fd;
    X509* cert;

    if (!proxy)
        return 0;

    fd = fopen(proxy, "r");
    if (!fd)
        return NOTFOUND;
    cert = PEM_read_X509(fd, NULL, NULL, NULL );
    fclose(fd);

    if (!cert)
        return BADFORMAT;

    time_t start = ASN1_TIME_2_time_t(X509_get_notBefore(cert));
    time_t end = ASN1_TIME_2_time_t(X509_get_notAfter(cert));

    X509_free(cert);

    time_t now = time(NULL );

    /* At least, one hour into the future */
    if (start > now)
        return START_FUTURE;
    else if (end < now)
        return EXPIRED;
    else if (end < (now + 3600))
        return EXPIRES_SOON;
    else
        return VALID;
}

char* dav_deleg_get_proxy(request_rec* r, const char* proxy_dir,
        const char* user_dn)
{
    apr_pool_t* subpool;

    apr_pool_create(&subpool, r->pool);

    char *proxy = NULL, *valid_proxy = NULL;
    char *delegation_id = dav_deleg_make_delegation_id(subpool,
            r->subprocess_env);
    char *encoded_uname = dav_deleg_client_name_encode(subpool, user_dn);

    proxy = apr_pstrcat(subpool, proxy_dir, "/", encoded_uname, "/",
            delegation_id, "/userproxy.pem", NULL );

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Expected proxy location: %s",
            proxy);

    const char* log_msg = NULL;
    switch (dav_deleg_proxy_is_valid(proxy)) {
        case VALID:
            valid_proxy = apr_pstrdup(r->pool, proxy);
            log_msg = "Found a valid proxy. No need for delegation.";
            break;
        case NOTFOUND:
            log_msg = "Proxy not found on disk";
            break;
        case BADFORMAT:
            log_msg = "Stored proxy corrupted";
            break;
        case EXPIRED:
            log_msg = "The proxy expired";
            break;
        case START_FUTURE:
            log_msg = "The proxy starts in the future";
            break;
        case EXPIRES_SOON:
            log_msg = "The proxy is valid, but its remaining life is too short";
            break;
        default:
            log_msg = "UNKNOWN INTERNAL STATE!";
    }

    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "%s (%s)", log_msg, delegation_id);

    apr_pool_destroy(subpool);

    return valid_proxy;
}
