/* 
 * File:   config.h.cmake
 * Author: Alejandro Álvarez Ayllón
 */
#ifndef CONFIG_H_
#define	CONFIG_H_


/* Version */
#define LCGDM_DAV_MAJOR @lcgdm-dav_MAJOR@
#define LCGDM_DAV_MINOR @lcgdm-dav_MINOR@
#define LCGDM_DAV_PATCH @lcgdm-dav_PATCH@

#define LCGDM_DAV_VERSION "@lcgdm-dav_MAJOR@.@lcgdm-dav_MINOR@.@lcgdm-dav_PATCH@"

/* Flags */
#cmakedefine WITH_METALINK
#cmakedefine USE_GNUTLS

#endif	/* CONFIG_H_ */
