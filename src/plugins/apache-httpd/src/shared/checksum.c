/*
 * Copyright (c) CERN 2013-2017
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/checksums.h>
#include <httpd.h>
#include <regex.h>
#include "shared/utils.h"
#include <http_log.h>
#include <strings.h>

#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#define RFC3230_REGEXP "^([[:alnum:]-]+)(;q=[[:digit:]]+\\.?[[:digit:]]*)?([,]?)"
#define RFC3230_REGEXP_SINGLE "^[[:space:]]*([[:alnum:]-]+)(;q=([[:digit:]]+\\.?[[:digit:]]*))?[[:space:]]*$"

 struct digest_list {
    char *digest;
    double q;
    struct digest_list *next;
};


static char* dav_shared_strcpy_lower(char *out, const char *in, size_t max)
{
    size_t i;
    for (i = 0; in[i] && i < max; ++i)
        out[i] = tolower(in[i]);
    return out;
}


int dav_shared_next_digest(const char **want_digest, char *output, size_t outsize)
{
    int status;
    regex_t regex;
    regmatch_t matches[3];

    if (regcomp(&regex, RFC3230_REGEXP, REG_EXTENDED) != 0)
        abort();

    while (isspace(**want_digest))
        ++(*want_digest);

    status = regexec(&regex, *want_digest, 3, matches, 0);
    regfree(&regex);

    if (status == 0) {
        size_t digest_len = matches[1].rm_eo - matches[1].rm_so;
        const char *digest = (*want_digest) + matches[1].rm_so;

        if (outsize < digest_len) {
            digest_len = outsize;
        }

        dav_shared_strcpy_lower(output, digest, digest_len);
        output[digest_len] = '\0';

        *want_digest += matches[0].rm_eo;

        return 1;
    }

    return 0;
}


void dav_shared_sorted_digests(request_rec *r, char *output, size_t max)
{
    char *supported_digests[] = {
        "md5", "crc32", "adler32", NULL
    };

    const char *want_digest;
    char *s, *token;
    struct digest_list *dl = NULL;

    int status;
    regex_t regex;
    regmatch_t matches[4];

    output[0] = '\0';
    want_digest = apr_table_get(r->headers_in, "Want-Digest");

    if (!want_digest) {
        return;
    }

    if (regcomp(&regex, RFC3230_REGEXP_SINGLE, REG_EXTENDED) != 0)
        abort();

    // split want-digest string by comma
    s = strdup(want_digest);
    token = strtok(s, ",");

    while (token) {
        // parse checksum type and preference
        status = regexec(&regex, token, 4, matches, 0);

        if (status == 0) {
            size_t digest_len = matches[1].rm_eo - matches[1].rm_so;
            char *digest;
            double q = 1;
            int supported = 0;
            int i;

            digest = malloc(digest_len+1);
            dav_shared_strcpy_lower(digest, token+matches[1].rm_so, digest_len);
            digest[digest_len] = '\0';

            for (i = 0; supported_digests[i]; ++i) {
                if (strcmp(supported_digests[i], digest) == 0) {
                    supported = 1;
                    break;
                }
            }

            if (supported) {
                if (matches[3].rm_so < matches[3].rm_eo) {
                    const char *q_start = token + matches[3].rm_so;
                    token[matches[3].rm_eo] = '\0';
                    q = atof(q_start);
                }
                if (q < 0) q = 0;
                if (q > 1) q = 1;

                // add new checksum time in a list sorted by preference
                struct digest_list *d = (struct digest_list*) malloc(sizeof(struct digest_list));
                d->digest = digest;
                d->q = q;
                d->next = NULL;

                if (dl) {
                    struct digest_list *p = dl;
                    while (p->q >= q && p->next) p = p->next;
                    d->next = p->next;
                    p->next = d;
                } else {
                  dl = d;
                }

            } else {
                ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Skipping unsupported checksum type %s", token);
            }

        } else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Skipping unparsable checksum algorithm %s", token);
        }

        token = strtok(NULL, ",");
    }

    regfree(&regex);
    free(s);

    while (dl) {
        if (strlen(output) + strlen(dl->digest) + 1 < max) {
            if (output[0] != '\0') strcat(output, ",");
            strcat(output, dl->digest);
        } else {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Skipping digest %s because it exceeds max digest list size %lu", dl->digest, (unsigned long) max);
        }
        free(dl->digest);
        dl = dl->next;
    }
}


static int
char_to_int(char c)
{
  if (isdigit(c)) {
    return c - '0';
  } else {
    c = tolower(c);
    if (c >= 'a' && c <= 'f') {
      return c - 'a' + 10;
    }
    return -1;
  }
}

// Decode a hex digest array to base64 if needed. Returns the
// length of the resulting string, 0 if error
//
int dav_shared_hexdigesttobase64(const char *digest_name,
                                 const char *input, char *output) {
  int digest_length = strlen(input);
    
  // Check if we have to convert it or not
  if (!strcasecmp(digest_name, "adler32")) {
    strcpy(output, input);
    return digest_length;
  }
  
  if (strcasecmp(digest_name, "md5")) {
    // Let's check now the less probable algorithms
    if ( strcasecmp(digest_name, "SHA") &&
      strcasecmp(digest_name, "SHA") &&
      strcasecmp(digest_name, "SHA-256") &&
      strcasecmp(digest_name, "SHA-512") ) {
      
      if (strcasecmp(digest_name, "UNIXcksum")) {
        strcpy(output, input);
        return digest_length;
      }
    }
  }
  
  // We are here if the digest has to be converted, e.g. md5, sha
  char tmpout[1024];
  
  // Decode a hex digest array to raw bytes.
  int idx;
  for (idx=0; idx < digest_length; idx += 2) {
    int upper =  char_to_int(input[idx]);
    int lower =  char_to_int(input[idx+1]);
    if ((upper < 0) || (lower < 0) ||
      (upper > 15) || (lower > 15)) {
      return 0; // Error in the input data, not hex
    }
    tmpout[idx/2] = (upper << 4) + lower;
  }
  
  // Now the raw bytes into base64
  BIO *bmem, *b64;
  BUF_MEM *bptr;
       
  output[0] = '\0';
  
  b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  bmem = BIO_new(BIO_s_mem());
  BIO_push(b64, bmem);
  BIO_write(b64, tmpout, digest_length/2);
  
  if (BIO_flush(b64) <= 0) {
    BIO_free_all(b64);
    return 0;
  }
  
  BIO_get_mem_ptr(b64, &bptr);
  
  
  memcpy(output, bptr->data, bptr->length);
  output[bptr->length] = '\0';
  
  int ret = bptr->length;
  
  BIO_free_all(b64);
  
  return ret;
}

