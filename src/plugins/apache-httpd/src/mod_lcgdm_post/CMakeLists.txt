cmake_minimum_required (VERSION 2.6)

add_library(mod_lcgdm_post MODULE mod_lcgdm_post.c
                                  form_helper.c
                                  form_filter.c)

set_target_properties(mod_lcgdm_post PROPERTIES PREFIX "")

install(TARGETS     mod_lcgdm_post
        DESTINATION usr/lib${LIB_SUFFIX}/httpd/modules)
