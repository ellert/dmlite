/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <httpd.h>
#include <http_request.h>
#include "form_helper.h"

static int form_readline(ap_filter_t *input, apr_bucket_brigade *bb,
        char *buffer, size_t bsize)
{
    int got_line = 0;
    int got_eos = 0;
    size_t i = 0;
    do {
        apr_status_t rc;

        rc = ap_get_brigade(input, bb, AP_MODE_READBYTES, APR_BLOCK_READ, 1);
        if (rc != APR_SUCCESS)
            return -1;

        apr_bucket *b;
        for (b = APR_BRIGADE_FIRST(bb);
                i < bsize && b != APR_BRIGADE_SENTINEL(bb) ; b =
                        APR_BUCKET_NEXT(b)) {
            const char* data;
            apr_size_t len;

            if (APR_BUCKET_IS_EOS(b)) {
                got_eos = 1;
                break;
            }
            else if (APR_BUCKET_IS_METADATA(b)) {
                continue;
            }
            else {
                rc = apr_bucket_read(b, &data, &len, APR_BLOCK_READ);
                if (rc != APR_SUCCESS)
                    return -1;

                if (data[0] == '\n') {
                    buffer[i--] = 0;
                    if (buffer[i] == '\r')
                        buffer[i] = '\0';
                    got_line = 1;
                    break;
                }
                else {
                    buffer[i++] = data[0];
                }
            }
        }
        apr_brigade_cleanup(bb);
    } while (i < bsize && !got_line && !got_eos);

    return got_line;
}

static void post_set_header(apr_pool_t *pool, apr_table_t *table,
        const char *line)
{
    const char* p = strchr(line, ':');
    if (p) {
        const char *key = apr_pstrndup(pool, line, p - line);
        ++p;
        while (isspace(*p))
            ++p;
        apr_table_set(table, key, p);
    }
}

static int is_open_boundary(const char *line, const char *boundary)
{
    return strcmp(line + 2, boundary) == 0;
}

static char* from_disposition(apr_pool_t *pool, const char* disp,
        const char *field)
{
    if (!disp)
        return NULL ;

    size_t len = strlen(field);
    char *p = apr_pstrdup(pool, strstr(disp, field));

    if (!p || p[len] != '"')
        return NULL ;
    p += (len + 1);

    int i = 0;
    while (p[i] != '"' && p[i] != '\0')
        ++i;
    p[i] = '\0';

    return p;
}

const char* post_form_next(request_rec *r, const char *boundary,
        apr_table_t *attrs)
{
    enum
    {
        kBoundary, kHead, kBody
    } state = kBoundary;

    apr_bucket_brigade *bb;
    bb = apr_brigade_create(r->pool, r->connection->bucket_alloc);

    char line[128];
    int rs = form_readline(r->input_filters, bb, line, sizeof(line));

    while (rs == 1 && state != kBody) {

        switch (state) {
            case kBoundary:
                if (is_open_boundary(line, boundary))
                    state = kHead;
                break;
            case kHead:
                if (line[0] != '\0')
                    post_set_header(r->pool, attrs, line);
                else
                    state = kBody;
                break;
            case kBody:
                /* Nothing */
                break;
        }

        if (state != kBody)
            rs = form_readline(r->input_filters, bb, line, sizeof(line));
    }

    apr_brigade_destroy(bb);

    if (rs < 0)
        return NULL ;

    const char *disposition = apr_table_get(attrs, "Content-Disposition");
    return from_disposition(r->pool, disposition, "name=");
}

const char* post_form_filename(apr_pool_t *pool, apr_table_t *attrs)
{
    const char *disposition = apr_table_get(attrs, "Content-Disposition");
    char *filename = from_disposition(pool, disposition, "filename=");
    if (filename)
        ap_unescape_url(filename);
    return filename;
}
