#ifndef DMLITE_DSI_INTERNAL_H
#define DMLITE_DSI_INTERNAL_H

#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#ifndef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#endif

/* This is necessary for O_LARGEFILE */
#ifndef _USE_LARGEFILE64
#define _USE_LARGEFILE64
#endif

#include <gssapi.h>
#include <globus_gsi_credential.h>
#include <voms_apic.h>

#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <sys/stat.h>

#include <dmlite/c/any.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/pool.h>
#include <dmlite/c/io.h>

#include "dmlite_gridftp.h"

/* structure for data associate to voms */

typedef struct gss_name_desc_struct gss_name_desc;

typedef struct gss_cred_id_desc_struct {
	globus_gsi_cred_handle_t	cred_handle;
	gss_name_desc *			globusid;
	gss_cred_usage_t		cred_usage;
	SSL_CTX *			ssl_context;
} gss_cred_id_desc;

typedef struct gssapi_voms_ctx {
	char *voname;
	char **fqan;
	int nbfqan;
} gssapi_voms_ctx_t;

/* Logging and error handling functions */
void dmlite_gfs_log(dmlite_handle_t * handle, globus_gfs_log_type_t type, char * format, ...);

globus_result_t dmlite_error2gfs_result(const char * name, dmlite_handle_t * handle, struct dmlite_context * context);

globus_result_t posix_error2gfs_result(const char * name, dmlite_handle_t * handle, int errnum, char * format, ...);

/* Utility functions used by several commands */

int get_voms_creds(gssapi_voms_ctx_t *ctx, dmlite_handle_t *dmlite_handle);

void dmlite_gfs_hostid2host(char * host_id, char * host);

int dmlite_gfs_node_cmp(void * datum, void * arg);

const char * dmlite_gfs_fixpath(const char * path, globus_bool_t hostname);

char * dmlite_gfs_gethostname(const char * path);

void dmlite_stat2gfs(char * name, struct stat * stat, globus_gfs_stat_t * gfs_stat);

struct dmlite_context * dmlite_get_context(dmlite_handle_t * dmlite_handle, int * reason);

int dmlite_gfs_check_node(char **host, dmlite_handle_t * dmlite_handle_obj, const char * path, int flags);

globus_bool_t dmlite_gfs_fstat(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, const char * path, struct stat* buf);

dmlite_fd * dmlite_gfs_open(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, const char * path, int flags);

int dmlite_gfs_putdone(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, globus_bool_t ok);

int dmlite_gfs_close(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, globus_bool_t ok);

globus_result_t dmlite_gfs_compute_checksum(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, const char * pathname,
					    const char * algorithm, globus_off_t offset, globus_off_t length, char * checksum, size_t checksummaxlen);

globus_result_t dmlite_gfs_get_checksum(struct dmlite_context * dmlite_context_obj, dmlite_handle_t * dmlite_handle_obj, const char * pathname,
					    const char * algorithm, globus_off_t offset, globus_off_t length, char * checksum, size_t checksummaxlen);

#endif /* DMLITE_DSI_INTERNAL_H */
