/// @file   DomeAdapterIO.h
/// @brief  Filesystem IO using dome.
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>
#ifndef DOME_ADAPTER_HEAD_CATALOG_H
#define DOME_ADAPTER_HEAD_CATALOG_H

#include <dmlite/cpp/dmlite.h>
#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/dummy/DummyCatalog.h>
#include <fstream>
#include "utils/DavixPool.h"
#include "utils/DomeTalker.h"

namespace dmlite {
  extern Logger::bitmask domeadapterlogmask;
  extern Logger::component domeadapterlogname;

  class DomeAdapterHeadCatalog;

  class DomeAdapterHeadCatalogFactory : public CatalogFactory {
  public:
    DomeAdapterHeadCatalogFactory();
    virtual ~DomeAdapterHeadCatalogFactory();

    void configure(const std::string& key, const std::string& value)  ;
    Catalog *createCatalog(PluginManager* pm)  ;
  private:
    std::string domehead_;

    DavixCtxFactory davixFactory_;
    DavixCtxPool davixPool_;

    friend class DomeAdapterHeadCatalog;
  };

  class DomeAdapterHeadCatalog : public Catalog {
  public:
    DomeAdapterHeadCatalog(DomeAdapterHeadCatalogFactory *factory);
    virtual ~DomeAdapterHeadCatalog();

    std::string getImplId() const throw();

    void setStackInstance(StackInstance* si)  ;

    void setSecurityContext(const SecurityContext* ctx)  ;

    void changeDir(const std::string& path)  ;
    std::string getWorkingDir()  ;

    DmStatus extendedStat(ExtendedStat &xstat, const std::string&, bool)  ;
    ExtendedStat extendedStat(const std::string&, bool followSym = true)  ;
    ExtendedStat extendedStatByRFN(const std::string& rfn)   ;

    bool access(const std::string&, int)  ;
    bool accessReplica(const std::string& replica, int mode)  ;

    void addReplica   (const Replica&)  ;
    void deleteReplica(const Replica&)  ;
    std::vector<Replica> getReplicas(const std::string&)  ;

    void symlink(const std::string&, const std::string&)  ;
    std::string readLink(const std::string& path)  ;

    void unlink(const std::string& path)  ;

    void create(const std::string& path, mode_t mode)  ;

    // mode_t umask   (mode_t)                           throw ();
    void   setMode (const std::string&, mode_t)        ;
    void   setOwner(const std::string&, uid_t, gid_t, bool)  ;

    void setSize(const std::string& path, size_t newSize)  ;
    void setChecksum(const std::string& lfn, 
                     const std::string& ctype,
                     const std::string& cval)  ;

    void setAcl(const std::string&, const Acl&)  ;

    void utime(const std::string&, const struct utimbuf*)  ;

    Replica getReplica   (int64_t rid)  ;
    // Replica getReplica   (const std::string& sfn)  ;







    virtual void getChecksum(const std::string& path,
                             const std::string& csumtype,
                             std::string& csumvalue,
                             const std::string& pfn,
                             const bool forcerecalc = false, const int waitsecs = 0)  ;






    std::string getComment(const std::string& path)  ;
    void setComment(const std::string& path, const std::string& comment)  ;
    // void deleteComment(const std::string& path)  ;

    void setGuid(const std::string& path, const std::string& guid)  ;

    void updateExtendedAttributes(const std::string&,
                                  const Extensible&)  ;

    Directory* openDir (const std::string&)  ;
    void       closeDir(Directory*)          ;

    ExtendedStat*  readDirx(Directory*)  ;
    struct dirent* readDir(Directory* dir)  ;

    void makeDir(const std::string& path, mode_t mode)  ;

    void rename(const std::string& oldPath, const std::string& newPath)  ;
    void removeDir(const std::string& path)  ;

    Replica getReplicaByRFN(const std::string& rfn)  ;
    void    updateReplica(const Replica& replica)  ;

   private:
     std::string absPath(const std::string &relpath);

     struct DomeDir : public Directory {
       std::string path_;
       size_t pos_;
       std::vector<dmlite::ExtendedStat> entries_;
       std::vector<dirent> dirents_;

       virtual ~DomeDir() {}
       DomeDir(std::string path) : path_(path), pos_(0) {}
     };

     std::string cwdPath_;

     const SecurityContext* secCtx_;
     DomeTalker *talker__;
     
     StackInstance* si_;

     DomeAdapterHeadCatalogFactory &factory_;
  };
}

#endif
