/// @file    core/builtin/Authn.h
/// @brief   User and group mapping using the system's ones.
/// @details This will be used by default when no other Authn
///          implementeation is loaded.
/// @author  Alejandro Álvarez Ayllon <aalvarez@cern.ch>
#ifndef BUILTIN_AUTHN_H
#define	BUILTIN_AUTHN_H

#include <dmlite/cpp/authn.h>

namespace dmlite {
  
  class BuiltInAuthn: public Authn {
   public:
    BuiltInAuthn(const std::string&, const std::string&);
    ~BuiltInAuthn();

    std::string getImplId(void) const throw();

    virtual SecurityContext* createSecurityContext(const SecurityCredentials& cred)  ;
    virtual SecurityContext* createSecurityContext()  ;

    GroupInfo newGroup   (const std::string& gname)  ;
    GroupInfo getGroup   (gid_t gid)  ;
    GroupInfo getGroup   (const std::string& groupName)  ;
    GroupInfo getGroup   (const std::string& key,
                          const boost::any& value)  ;
    void      updateGroup(const GroupInfo& group)  ;
    void      deleteGroup(const std::string& groupName)  ;

    UserInfo newUser   (const std::string& uname)     ;
    UserInfo getUser   (const std::string& userName)  ;
    UserInfo getUser   (const std::string& userName,
                        gid_t* group)  ;
    UserInfo getUser   (const std::string& key,
                        const boost::any& value)  ;
    void     updateUser(const UserInfo& user)  ;
    void     deleteUser(const std::string& userName)  ;
    
    std::vector<GroupInfo> getGroups(void)  ;
    std::vector<UserInfo>  getUsers (void)  ;

    void getIdMap(const std::string& userName,
                  const std::vector<std::string>& groupNames,
                  UserInfo* user,
                  std::vector<GroupInfo>* groups)  ;
   private:
    std::string nobody_;
    std::string nogroup_;
  };

  class BuiltInAuthnFactory: public AuthnFactory {
   public:
    BuiltInAuthnFactory();
    ~BuiltInAuthnFactory();

    void configure(const std::string& key, const std::string& value)  ;

    Authn* createAuthn(PluginManager* pm)  ;
    
   private:
    std::string nobody_;
    std::string nogroup_;
  };
  
};

#endif	// BUILTIN_AUTHN_H
