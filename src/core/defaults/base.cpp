#include <dmlite/cpp/base.h>
#include "NotImplemented.h"


using namespace dmlite;



BaseInterface::~BaseInterface()
{
  // Nothing
}



BaseFactory::~BaseFactory()
{
  // Nothing
}

FACTORY_NOT_IMPLEMENTED(void BaseFactory::configure(const std::string&, const std::string&)  );



void BaseInterface::setStackInstance(BaseInterface* i, StackInstance* si)  
{
  if (i != NULL) i->setStackInstance(si);
}



void BaseInterface::setSecurityContext(BaseInterface* i, const SecurityContext* ctx)  
{
  if (i != NULL) i->setSecurityContext(ctx);
}



NOT_IMPLEMENTED(void BaseInterface::setStackInstance(StackInstance*)  );
NOT_IMPLEMENTED(void BaseInterface::setSecurityContext(const SecurityContext*)  );
