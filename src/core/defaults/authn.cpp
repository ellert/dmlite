#include <dmlite/cpp/authn.h>
#include "NotImplemented.h"

using namespace dmlite;



AuthnFactory::~AuthnFactory()
{
  // Nothing
}



Authn* AuthnFactory::createAuthn(AuthnFactory* f, PluginManager* pm)  
{
  return f->createAuthn(pm);
}



FACTORY_NOT_IMPLEMENTED(Authn* AuthnFactory::createAuthn(PluginManager*)  );



Authn::~Authn()
{
  // Nothing
}



NOT_IMPLEMENTED(SecurityContext* Authn::createSecurityContext(const SecurityCredentials&)  );
NOT_IMPLEMENTED(SecurityContext* Authn::createSecurityContext(void)  )
NOT_IMPLEMENTED(GroupInfo Authn::newGroup(const std::string&)  );
NOT_IMPLEMENTED(GroupInfo Authn::getGroup(const std::string&)  );
NOT_IMPLEMENTED(GroupInfo Authn::getGroup(const std::string&, const boost::any&)  );
NOT_IMPLEMENTED(std::vector<GroupInfo> Authn::getGroups(void)  );
NOT_IMPLEMENTED(void Authn::updateGroup(const GroupInfo&)  );
NOT_IMPLEMENTED(void Authn::deleteGroup(const std::string&)  );
NOT_IMPLEMENTED(UserInfo Authn::newUser(const std::string&)  );
NOT_IMPLEMENTED(UserInfo Authn::getUser(const std::string&)  );
NOT_IMPLEMENTED(UserInfo Authn::getUser(const std::string&, const boost::any&)  );
NOT_IMPLEMENTED(std::vector<UserInfo> Authn::getUsers(void)  );
NOT_IMPLEMENTED(void Authn::updateUser(const UserInfo&)  );
NOT_IMPLEMENTED(void Authn::deleteUser(const std::string&)  );
NOT_IMPLEMENTED(void Authn::getIdMap(const std::string&, const std::vector<std::string>&,
                UserInfo*, std::vector<GroupInfo>*)  );
