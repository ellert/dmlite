/// @file    core/dummy/DummyCatalog.cpp
/// @brief   DummyCatalog implementation.
/// @details It makes sense as a base for other decorator plug-ins.
/// @author  Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#include <dmlite/cpp/dummy/DummyCatalog.h>

using namespace dmlite;



/// Little of help here to avoid redundancy
#define DELEGATE(method, ...) \
if (this->decorated_ == NULL)\
  throw DmException(DMLITE_SYSERR(ENOSYS), "There is no plugin in the stack that implements "#method);\
this->decorated_->method(__VA_ARGS__);


/// Little of help here to avoid redundancy
#define DELEGATE_RETURN(method, ...) \
if (this->decorated_ == NULL)\
  throw DmException(DMLITE_SYSERR(ENOSYS), "There is no plugin in the stack that implements "#method);\
return this->decorated_->method(__VA_ARGS__);



DummyCatalog::DummyCatalog(Catalog* decorated)  
{
  this->decorated_ = decorated;
}



DummyCatalog::~DummyCatalog()
{
  delete this->decorated_;
}



void DummyCatalog::setStackInstance(StackInstance* si)  
{
  BaseInterface::setStackInstance(this->decorated_, si);
}



void DummyCatalog::setSecurityContext(const SecurityContext* ctx)  
{
  BaseInterface::setSecurityContext(this->decorated_, ctx);
}



void DummyCatalog::changeDir(const std::string& path)  
{
  DELEGATE(changeDir, path);
}



std::string DummyCatalog::getWorkingDir(void)  
{
  DELEGATE_RETURN(getWorkingDir);
}


DmStatus DummyCatalog::extendedStat(ExtendedStat &xstat, const std::string& path, bool follow)  
{
  DELEGATE_RETURN(extendedStat, xstat, path, follow);
}


ExtendedStat DummyCatalog::extendedStat(const std::string& path, bool follow)  
{
  DELEGATE_RETURN(extendedStat, path, follow);
}



ExtendedStat DummyCatalog::extendedStatByRFN(const std::string& rfn)  
{
  DELEGATE_RETURN(extendedStatByRFN, rfn);
}



bool DummyCatalog::access(const std::string& path, int mode)  
{
  DELEGATE_RETURN(access, path, mode);
}



bool DummyCatalog::accessReplica(const std::string& replica, int mode)  
{
  DELEGATE_RETURN(accessReplica, replica, mode);
}



void DummyCatalog::addReplica(const Replica& replica)  
{
  DELEGATE(addReplica, replica);
}



void DummyCatalog::deleteReplica(const Replica& replica)  
{
  DELEGATE(deleteReplica, replica);
}



std::vector<Replica> DummyCatalog::getReplicas(const std::string& path)  
{
  DELEGATE_RETURN(getReplicas, path);
}



void DummyCatalog::symlink(const std::string& oldpath, const std::string& newpath)  
{
  DELEGATE(symlink, oldpath, newpath);
}



std::string DummyCatalog::readLink(const std::string& path)  
{
  DELEGATE_RETURN(readLink, path);
}



void DummyCatalog::unlink(const std::string& path)  
{
  DELEGATE(unlink, path);
}



void DummyCatalog::create(const std::string& path, mode_t mode)  
{
  DELEGATE(create, path, mode);
}



mode_t DummyCatalog::umask(mode_t mask)
{
  DELEGATE_RETURN(umask, mask);
}



void DummyCatalog::setMode(const std::string& path, mode_t mode)  
{
  DELEGATE(setMode, path, mode);
}



void DummyCatalog::setOwner(const std::string& path, uid_t newUid, gid_t newGid, bool fs)  
{
  DELEGATE(setOwner, path, newUid, newGid, fs);
}



void DummyCatalog::setSize(const std::string& path, size_t newSize)  
{
  DELEGATE(setSize, path, newSize);
}



void DummyCatalog::setChecksum(const std::string& path,
                                  const std::string& csumtype,
                                  const std::string& csumvalue)  
{
  DELEGATE(setChecksum, path, csumtype, csumvalue);
}

void DummyCatalog::getChecksum(const std::string& path,
                         const std::string& csumtype,
                         std::string& csumvalue,
                         const std::string& pfn, const bool forcerecalc, const int waitsecs)  
{

  DELEGATE(getChecksum, path, csumtype, csumvalue, pfn, forcerecalc, waitsecs);
  
}
                         
void DummyCatalog::setAcl(const std::string& path, const Acl& acls)  
{
  DELEGATE(setAcl, path, acls);
}



void DummyCatalog::utime(const std::string& path, const struct utimbuf* buf)  
{
  DELEGATE(utime, path, buf);
}



std::string DummyCatalog::getComment(const std::string& path)  
{
  DELEGATE_RETURN(getComment, path);
}



void DummyCatalog::setComment(const std::string& path, const std::string& comment)  
{
  DELEGATE(setComment, path, comment);
}



void DummyCatalog::setGuid(const std::string& path, const std::string& guid)  
{
  DELEGATE(setGuid, path, guid);
}



void DummyCatalog::updateExtendedAttributes(const std::string& path,
                                            const Extensible& attr)  
{
  DELEGATE(updateExtendedAttributes, path, attr);
}



Directory* DummyCatalog::openDir(const std::string& path)  
{
  DELEGATE_RETURN(openDir, path);
}



void DummyCatalog::closeDir(Directory* dir)  
{
  DELEGATE(closeDir, dir);
}



struct dirent* DummyCatalog::readDir(Directory* dir)  
{
  DELEGATE_RETURN(readDir, dir);
}



ExtendedStat* DummyCatalog::readDirx(Directory* dir)  
{
  DELEGATE_RETURN(readDirx, dir);
}



void DummyCatalog::makeDir(const std::string& path, mode_t mode)  
{
  DELEGATE(makeDir, path, mode);
}



void DummyCatalog::rename(const std::string& oldPath, const std::string& newPath)  
{
  DELEGATE(rename, oldPath, newPath);
}



void DummyCatalog::removeDir(const std::string& path)  
{
  DELEGATE(removeDir, path);
}



Replica DummyCatalog::getReplicaByRFN(const std::string& rfn)  
{
  DELEGATE_RETURN(getReplicaByRFN, rfn);
}



void DummyCatalog::updateReplica(const Replica& replica)  
{
  DELEGATE(updateReplica, replica);
}
