#
# This module detects if xrootd is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# XROOTD_LIBRARIES   = full path to the xrootd libraries
# XROOTD_INCLUDE_DIR = include dir to be used when using the xrootd library
# XROOTD_VERSION     = version of xrootd library found
# XROOTD_FOUND       = set to true if xrootd was found successfully
#
# XROOTD_LOCATION
#   setting this enables search for xrootd libraries / headers in this location


# -----------------------------------------------------
# XROOTD Libraries
# -----------------------------------------------------
find_library(XROOTD_LIBRARIES
    NAMES XrdUtils-5 XrdUtils-4 XrdUtils
    HINTS $ENV{XROOTD_LOCATION}/lib $ENV{XROOTD_LOCATION}/lib64 $ENV{XROOTD_LOCATION}/lib32
    DOC "The main xrootd library"
)

find_library(XROOTD_LIBRARIES_HTTP
    NAMES XrdHttp-5 XrdHttp-4 XrdHttp
    HINTS $ENV{XROOTD_LOCATION}/lib $ENV{XROOTD_LOCATION}/lib64 $ENV{XROOTD_LOCATION}/lib32
    DOC "The xrootd library for HTTP support"
)

message(STATUS "xrootd libs: ${XROOTD_LIBRARIES}")
message(STATUS "xrootd HTTP libs: ${XROOTD_LIBRARIES_HTTP}")

# -----------------------------------------------------
# XROOTD Include Directories
# -----------------------------------------------------
find_path(XROOTD_INCLUDE_DIR 
    NAMES xrootd/XrdCms/XrdCmsClient.hh
    HINTS $ENV{XROOTD_LOCATION} $ENV{XROOTD_LOCATION}/include
    DOC "The xrootd include directory"
)
if(XROOTD_INCLUDE_DIR)
    message(STATUS "xrootd includes found in ${XROOTD_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set XROOTD_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(xrootd DEFAULT_MSG XROOTD_LIBRARIES XROOTD_INCLUDE_DIR)
mark_as_advanced(XROOTD_INCLUDE_DIR XROOTD_LIBRARIES)

# -----------------------------------------------------
# XROOTD Version
# -----------------------------------------------------
if(XROOTD_INCLUDE_DIR)
    file(STRINGS ${XROOTD_INCLUDE_DIR}/xrootd/XrdVersion.hh XROOTD_VERSIONTMP
             REGEX "[0-9].[0-9].[0-9]")
    string(REGEX MATCH "[0-9].[0-9].[0-9]" XROOTD_VERSION ${XROOTD_VERSIONTMP})
endif()


