/// @file   include/dmlite/cpp/poolmanager.h
/// @brief  Pool API.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef DMLITE_CPP_POOLMANAGER_H
#define DMLITE_CPP_POOLMANAGER_H

#include "dmlite/common/config.h"
#include "base.h"
#include "exceptions.h"
#include "pooldriver.h"
#include "utils/extensible.h"
#include "dmlite/c/pool.h"
#include <string>
#include <vector>

namespace dmlite {
  
  // Forward declarations.
  class StackInstance;
  
  /// Internal interface for handling pool metadata.
  class Pool: public Extensible {
   public:
    std::string name;
    std::string type;
    
    bool operator == (const Pool&) const;
    bool operator != (const Pool&) const;
    bool operator <  (const Pool&) const;
    bool operator >  (const Pool&) const;
  };

  /** @brief Progress markers for file copies. FTS jargon calls these "FTS performance markers"
      beware, we assume that this structure is identical to the one defined in the C API*/
  typedef struct xferprogmarker {
    int64_t xferred;
    // Other fields I have no idea, the ones from FTS look weird to me
  } xferprogmarker;
  
  /// Interface for pool types.
  class PoolManager: public virtual BaseInterface {
   public:
    enum PoolAvailability { kAny, kNone, kForRead, kForWrite, kForBoth};

    /// Destructor.
    virtual ~PoolManager();

    /// Get the list of pools.
    /// @param availability Filter by availability.
    virtual std::vector<Pool> getPools(PoolAvailability availability = kAny)  ;

    /// Get a specific pool.
    virtual Pool getPool(const std::string& poolname)  ;
    
    /// Create a new pool.
    virtual void newPool(const Pool& pool)  ;
    
    /// Update pool metadata.
    virtual void updatePool(const Pool& pool)  ;
    
    /// Remove a pool.
    virtual void deletePool(const Pool& pool)  ;

    /// Get a location for a logical name.
    /// @param path     The path to get.
    virtual Location whereToRead(const std::string& path)  ;
    
    /// Get a location for an inode
    /// @param inode The file inode.
    virtual Location whereToRead(ino_t inode)  ;

    /// Start the PUT of a file.
    /// @param path  The path of the file to create.
    /// @return      The physical location where to write.
    virtual Location whereToWrite(const std::string& path)  ;

    /// chooses a server to perform alternate operations
    /// e.g. tunnelling a gridftp connection
    /// @param path  A path, could be ignored, depending on the implementation
    virtual Location chooseServer(const std::string& path)  ;
    
    /// Cancel a write.
    /// @param path The logical file name.
    /// @param loc  As returned by whereToWrite
    virtual void cancelWrite(const Location& loc)  ;
    
    /// Get the estimation of the free/used space for writing into a directory
    /// @param path               The path of the directory to query
    /// @param totalfree          The total number of free bytes (may not be contiguous)
    /// @param used               The total number of used bytes
    virtual void getDirSpaces(const std::string& path, int64_t &totalfree, int64_t &used)  ;
    
    /// Write a logical file towards a given URL
    /// @param localsrcpath          The path of the file
    /// @param remotedesturl         The URL to write to
    /// @return                      0 on success, error code otherwise. EAGAIN means performance marker
    // Beware, the path to the delegated proxy (if any) is stored in the dmlite context
    virtual DmStatus fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  ;
    
    /// Fetch a file from a given URL
    /// @param localdestpath      The logical name of the file to create
    /// @param remotesrcurl       The URL to read the file from
    /// @return                   0 on success, error code otherwise. EAGAIN means performance marker
    // Beware, the path to the delegated proxy (if any) is stored in the dmlite context
    virtual DmStatus fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  ;
  };

  /// Plug-ins must implement a concrete factory to be instantiated.
  class PoolManagerFactory: public virtual BaseFactory {
   public:
    /// Virtual destructor
    virtual ~PoolManagerFactory();

   protected:
    // Stack instance is allowed to instantiate PoolManager
    friend class StackInstance;  

    /// Children of PoolManagerFactory are allowed to instantiate too (decorator)
    static PoolManager* createPoolManager(PoolManagerFactory* factory,
                                          PluginManager* pm)  ;

    /// Instantiate a implementation of Pool
    virtual PoolManager* createPoolManager(PluginManager* pm)  ;
  };

};

#endif // DMLITE_CPP_POOLMANAGER_H
