/// @file   include/dmlite/cpp/authn.h
/// @brief  Authentication API. Any sort of security check is plugin-specific.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef DMLITE_CPP_AUTHN_H
#define DMLITE_CPP_AUTHN_H

#include "dmlite/common/config.h"
#include "base.h"
#include "exceptions.h"
#include "utils/extensible.h"
#include "utils/logger.h"

#include <string>
#include <vector>

namespace dmlite {
  
  // Forward declarations.
  class PluginManager;
  class StackInstance;
  
  /// Security credentials. To be filled by the front-end.
  class SecurityCredentials: public Extensible {
   public:
    std::string mech;
    std::string clientName;
    std::string remoteAddress;
    std::string sessionId;
    
    // These fields may come from openid-connect
    std::string oidc_audience;
    std::string oidc_issuer;
    std::string oidc_scope;
    
    std::vector<std::string> fqans;
    
    bool operator == (const SecurityCredentials&) const;
    bool operator != (const SecurityCredentials&) const;
    bool operator <  (const SecurityCredentials&) const;
    bool operator >  (const SecurityCredentials&) const;
  };
  
  /// User information.
  /// To be filled by the Authn plugin with whichever data
  /// it is needed. (i.e. uid for LCGDM Adapter)
  /// To be used by other plugins whenever they need it.
  /// IMPORTANT: This means plugins must be compatible with the Authn
  ///            put in charge of security.
  class UserInfo: public Extensible {
   public:
    std::string name;
    
    bool operator == (const UserInfo&) const;
    bool operator != (const UserInfo&) const;
    bool operator <  (const UserInfo&) const;
    bool operator >  (const UserInfo&) const;
  };
  
  /// Group information
  /// See UserInfo
  class GroupInfo: public Extensible {
   public:
    std::string name;
    
    bool operator == (const GroupInfo&) const;
    bool operator != (const GroupInfo&) const;
    bool operator <  (const GroupInfo&) const;
    bool operator >  (const GroupInfo&) const;
  };
  

  /// Security context. To be created by the Authn.
  class SecurityContext {
   public:
    SecurityContext() {}
    
    SecurityContext(const SecurityCredentials& c,
                    const UserInfo& u,
                    std::vector<GroupInfo>& g):
                      credentials(c), user(u), groups(g) {}
    
    SecurityCredentials    credentials;
    
    UserInfo               user;
    std::vector<GroupInfo> groups;
    
    const std::string prettystring() {
      std::string r;
      r += SSTR("user: " << user.name << "(" << user.getLong("uid") << "," << user.getLong("banned", 0) << ") groups: '");
      for (std::vector<GroupInfo>::iterator i = groups.begin(); i != groups.end(); i++) {
        if (r.length()) r.append(",");
        
        r.append( SSTR( i->name << "(" << i->getLong("gid", -1) << "," << i->getLong("banned", 0) << ")") );
        
      }
      
      r += "'";
      return r;
    }
    
    // We store here a sort of little log of the authorization phase
    // This is supposed to describe why a user has been denied access (or granted)
    // And it's supposed to be easy to pass around.
    std::string            AuthNprocessing_msg;
    
    
    void AuthNprocessing_append(const char *str) {
      std::string s(str);
      
      if (AuthNprocessing_msg.length() > 0)
        AuthNprocessing_msg.append(" - ");
      
      AuthNprocessing_msg += s;
    };
    
    
    bool operator == (const SecurityContext&) const;
    bool operator != (const SecurityContext&) const;
    bool operator <  (const SecurityContext&) const;
    bool operator >  (const SecurityContext&) const;
  };
  
  

  /// User and group handling.
  ///@note This is the only interface not inheriting from BaseInterface.
  class Authn {
   public:
    /// Destructor
    virtual ~Authn();

    /// String ID of the user DB implementation.
    virtual std::string getImplId(void) const throw() = 0;

    /// Create a security context from the credentials.
    /// @param cred The security credentials.
    /// @return     A newly created SecurityContext.
    virtual SecurityContext* createSecurityContext(const SecurityCredentials& cred)  ;

    /// Create a default security context.
    /// @return     A newly created SecurityContext.
    virtual SecurityContext* createSecurityContext(void)  ;

    /// Create a new group.
    /// @param groupName The group name.
    /// @return          The new group.
    virtual GroupInfo newGroup(const std::string& groupName)  ;

    /// Get a specific group.
    /// @param groupName The group name.
    /// @return          The group.
    virtual GroupInfo getGroup(const std::string& groupName)  ;
    
    /// Get a specific group using an alternative key.
    /// @param key   The key name.
    /// @param value They value to search for.
    /// @return      The group.
    /// @note        The implementation will throw an exception if the field
    ///              can not be used as key.
    virtual GroupInfo getGroup(const std::string& key,
                               const boost::any& value)  ;
    
    /// Get the group list.
    virtual std::vector<GroupInfo> getGroups(void)  ;
    
    /// Update group info. 'name' identify uniquely the group.
    /// @param group The group metadata to update.
    virtual void updateGroup(const GroupInfo& group)  ;
    
    /// Delete a group.
    virtual void deleteGroup(const std::string& groupName)  ;

    /// Create a new user.
    /// @param userName The user name.
    /// @return         The new user.
    virtual UserInfo newUser(const std::string& userName)  ;

    /// Get a specific user.
    /// @param userName The user name.
    /// @return         The user.
    virtual UserInfo getUser(const std::string& userName)  ;
    
    /// Get a specific user using an alternative key.
    /// @param key   The key name.
    /// @param value They value to search for.
    /// @return      The user.
    /// @note        The implementation will throw an exception if the field
    ///              can not be used as key.
    virtual UserInfo getUser(const std::string& key,
                             const boost::any& value)  ;
    
    /// Get the user list.
    virtual std::vector<UserInfo> getUsers(void)  ;
    
    /// Update user info. 'name' identify uniquely the user.
    /// @param user The user metadata to update.
    virtual void updateUser(const UserInfo& user)  ;
    
    /// Delete a user.
    virtual void deleteUser(const std::string& userName)  ;

    /// Get the mapping of a user/group. Additionaly, new users and groups MAY
    /// be created by the implementation.
    /// @param userName   The user name.
    /// @param groupNames The different groups. Can be empty.
    /// @param user       Pointer to an UserInfo struct where to put the data.
    /// @param groups     Pointer to a vector where the group mapping will be put.
    /// @note If groupNames is empty, grid mapfile will be used to retrieve the default group.
    virtual void getIdMap(const std::string& userName,
                          const std::vector<std::string>& groupNames,
                          UserInfo* user,
                          std::vector<GroupInfo>* groups)  ;
  };


  /// AuthnFactory
  class AuthnFactory: public virtual BaseFactory {
   public:
    /// Destructor
    virtual ~AuthnFactory();

   protected:
    // Stack instance is allowed to instantiate Authn
    friend class StackInstance;

    /// Children of AuthnFactory are allowed to instantiate too (decorator)
    static Authn* createAuthn(AuthnFactory* factory,
                              PluginManager* pm)  ;

    /// Instantiate a implementation of Authn
    virtual Authn* createAuthn(PluginManager* pm)  ;
  };

};

#endif // DMLITE_CPP_AUTH_H
