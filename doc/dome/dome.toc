\contentsline {chapter}{\numberline {1}Dome}{8}{}%
\contentsline {section}{\numberline {1.1}DOME: Main features}{9}{}%
\contentsline {subsection}{\numberline {1.1.1}From spacetokens to quota tokens}{10}{}%
\contentsline {subsection}{\numberline {1.1.2}Pools and filesystems}{11}{}%
\contentsline {subsection}{\numberline {1.1.3}Open checksumming}{12}{}%
\contentsline {section}{\numberline {1.2}Tech}{12}{}%
\contentsline {subsection}{\numberline {1.2.1}Architecture}{12}{}%
\contentsline {subsection}{\numberline {1.2.2}Security}{14}{}%
\contentsline {subsection}{\numberline {1.2.3}Checksum queuer}{14}{}%
\contentsline {subsection}{\numberline {1.2.4}File pulls queuer}{16}{}%
\contentsline {section}{\numberline {1.3}Application programming interface}{16}{}%
\contentsline {subsection}{\numberline {1.3.1}DPM historical primitives}{17}{}%
\contentsline {subsection}{\numberline {1.3.2}RFIO historical primitives}{19}{}%
\contentsline {section}{\numberline {1.4}Command set of DOME}{20}{}%
\contentsline {subsection}{\numberline {1.4.1}Common request header fields}{20}{}%
\contentsline {subsubsection}{\nonumberline remoteclientdn}{20}{}%
\contentsline {subsubsection}{\nonumberline remoteclienthost}{20}{}%
\contentsline {subsubsection}{\nonumberline remoteclientgroups}{20}{}%
\contentsline {subsubsection}{\nonumberline remoteclientoidcaudience}{20}{}%
\contentsline {subsubsection}{\nonumberline remoteclientoidcissuer}{21}{}%
\contentsline {subsubsection}{\nonumberline remoteclientoidcscope}{21}{}%
\contentsline {subsection}{\numberline {1.4.2}Head node operation}{21}{}%
\contentsline {subsubsection}{\nonumberline dome\_access}{21}{}%
\contentsline {subsubsection}{\nonumberline dome\_accessreplica}{21}{}%
\contentsline {subsubsection}{\nonumberline dome\_put}{22}{}%
\contentsline {subsubsection}{\nonumberline dome\_putdone}{22}{}%
\contentsline {subsubsection}{\nonumberline dome\_addreplica}{23}{}%
\contentsline {subsubsection}{\nonumberline dome\_create}{24}{}%
\contentsline {subsubsection}{\nonumberline dome\_makedir}{24}{}%
\contentsline {subsubsection}{\nonumberline dome\_symlink}{25}{}%
\contentsline {subsubsection}{\nonumberline dome\_readlink}{25}{}%
\contentsline {subsubsection}{\nonumberline dome\_delreplica}{25}{}%
\contentsline {subsubsection}{\nonumberline dome\_getspaceinfo}{26}{}%
\contentsline {subsubsection}{\nonumberline dome\_getquotatoken}{28}{}%
\contentsline {subsubsection}{\nonumberline dome\_setquotatoken}{28}{}%
\contentsline {subsubsection}{\nonumberline dome\_modquotatoken}{29}{}%
\contentsline {subsubsection}{\nonumberline dome\_delquotatoken}{30}{}%
\contentsline {subsubsection}{\nonumberline dome\_getdirspaces}{30}{}%
\contentsline {subsubsection}{\nonumberline dome\_chksum}{31}{}%
\contentsline {subsubsection}{\nonumberline dome\_chksumstatus}{32}{}%
\contentsline {subsubsection}{\nonumberline dome\_get}{33}{}%
\contentsline {subsubsection}{\nonumberline dome\_pullstatus}{34}{}%
\contentsline {subsubsection}{\nonumberline dome\_statpool}{35}{}%
\contentsline {subsubsection}{\nonumberline dome\_addpool}{36}{}%
\contentsline {subsubsection}{\nonumberline dome\_rmpool}{36}{}%
\contentsline {subsubsection}{\nonumberline dome\_addfstopool}{37}{}%
\contentsline {subsubsection}{\nonumberline dome\_rmfs}{37}{}%
\contentsline {subsubsection}{\nonumberline dome\_modifyfs}{38}{}%
\contentsline {subsubsection}{\nonumberline dome\_setcomment}{38}{}%
\contentsline {subsubsection}{\nonumberline dome\_getcomment}{39}{}%
\contentsline {subsubsection}{\nonumberline dome\_getstatinfo}{39}{}%
\contentsline {subsubsection}{\nonumberline dome\_updatereplica}{40}{}%
\contentsline {subsubsection}{\nonumberline dome\_getreplicainfo}{41}{}%
\contentsline {subsubsection}{\nonumberline dome\_getreplicavec}{42}{}%
\contentsline {subsubsection}{\nonumberline dome\_addfstopool}{43}{}%
\contentsline {subsubsection}{\nonumberline dome\_getdir}{44}{}%
\contentsline {subsubsection}{\nonumberline dome\_getuser}{45}{}%
\contentsline {subsubsection}{\nonumberline dome\_getusersvec}{46}{}%
\contentsline {subsubsection}{\nonumberline dome\_newuser}{46}{}%
\contentsline {subsubsection}{\nonumberline dome\_deleteuser}{46}{}%
\contentsline {subsubsection}{\nonumberline dome\_updateuser}{47}{}%
\contentsline {subsubsection}{\nonumberline dome\_deletegroup}{47}{}%
\contentsline {subsubsection}{\nonumberline dome\_getgroup}{48}{}%
\contentsline {subsubsection}{\nonumberline dome\_updategroup}{48}{}%
\contentsline {subsubsection}{\nonumberline dome\_getgroupsvec}{49}{}%
\contentsline {subsubsection}{\nonumberline dome\_newgroup}{49}{}%
\contentsline {subsubsection}{\nonumberline dome\_setacl}{49}{}%
\contentsline {subsubsection}{\nonumberline dome\_setmode}{50}{}%
\contentsline {subsubsection}{\nonumberline dome\_setowner}{50}{}%
\contentsline {subsubsection}{\nonumberline dome\_setutime}{51}{}%
\contentsline {subsubsection}{\nonumberline dome\_setsize}{51}{}%
\contentsline {subsubsection}{\nonumberline dome\_setchecksum}{51}{}%
\contentsline {subsubsection}{\nonumberline dome\_updatexattr}{52}{}%
\contentsline {subsubsection}{\nonumberline dome\_getidmap}{52}{}%
\contentsline {subsubsection}{\nonumberline dome\_unlink}{53}{}%
\contentsline {subsubsection}{\nonumberline dome\_removedir}{54}{}%
\contentsline {subsubsection}{\nonumberline dome\_rename}{54}{}%
\contentsline {subsubsection}{\nonumberline dome\_config}{55}{}%
\contentsline {subsubsection}{\nonumberline dome\_config}{56}{}%
\contentsline {subsubsection}{\nonumberline dome\_config}{57}{}%
\contentsline {subsubsection}{\nonumberline dome\_info}{58}{}%
\contentsline {subsection}{\numberline {1.4.3}Disk node operation}{59}{}%
\contentsline {subsubsection}{\nonumberline dome\_makespace}{59}{}%
\contentsline {subsubsection}{\nonumberline dome\_dochksum}{59}{}%
\contentsline {subsubsection}{\nonumberline dome\_pull}{60}{}%
\contentsline {subsubsection}{\nonumberline dome\_pfnrm}{60}{}%
\contentsline {subsubsection}{\nonumberline dome\_statpfn}{61}{}%
\contentsline {chapter}{\numberline {2}Configuration}{62}{}%
\contentsline {section}{\numberline {2.1}Command-line parameters}{62}{}%
\contentsline {section}{\numberline {2.2}Configuration file: Structure and location}{62}{}%
\contentsline {section}{\numberline {2.3}Configuration file: Directives and parameters}{62}{}%
\contentsline {subsection}{\numberline {2.3.1}Configuration file: Common directives for head nodes and disk nodes}{63}{}%
\contentsline {subsubsection}{\nonumberline INCLUDE}{63}{}%
\contentsline {subsubsection}{\nonumberline glb.debug}{63}{}%
\contentsline {subsubsection}{\nonumberline glb.debug.components[]}{64}{}%
\contentsline {subsubsection}{\nonumberline glb.myhostname}{64}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.poolsize}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.conn\_timeout}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.ops\_timeout}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.ssl\_check}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.ca\_path}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.cli\_certificate}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.cli\_private\_key}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.restclient.xrdhttpkey}{65}{}%
\contentsline {subsubsection}{\nonumberline glb.reloadfsquotas}{66}{}%
\contentsline {subsubsection}{\nonumberline glb.reloadusersgroups}{66}{}%
\contentsline {subsubsection}{\nonumberline glb.role}{66}{}%
\contentsline {subsubsection}{\nonumberline glb.auth.dnmatch-cnprefix}{66}{}%
\contentsline {subsubsection}{\nonumberline glb.auth.dnmatch-cnsuffix}{67}{}%
\contentsline {subsubsection}{\nonumberline glb.auth.authorizeDN[]}{67}{}%
\contentsline {subsubsection}{\nonumberline head.put.minfreespace\_mb}{67}{}%
\contentsline {subsubsection}{\nonumberline glb.dmlite.configfile}{67}{}%
\contentsline {subsubsection}{\nonumberline glb.dmlite.poolsize}{67}{}%
\contentsline {subsubsection}{\nonumberline glb.workers}{68}{}%
\contentsline {subsubsection}{\nonumberline glb.printstats.interval}{68}{}%
\contentsline {subsection}{\numberline {2.3.2}Specific to head nodes}{68}{}%
\contentsline {subsubsection}{\nonumberline head.chksumstatus.heartbeattimeout}{68}{}%
\contentsline {subsubsection}{\nonumberline head.checksum.maxtotal}{68}{}%
\contentsline {subsubsection}{\nonumberline head.checksum.maxpernode}{68}{}%
\contentsline {subsubsection}{\nonumberline head.checksum.qtmout}{68}{}%
\contentsline {subsubsection}{\nonumberline head.db.host}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.user}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.password}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.port}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.poolsz}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.cnsdbname}{69}{}%
\contentsline {subsubsection}{\nonumberline head.db.dpmdbname}{69}{}%
\contentsline {subsubsection}{\nonumberline head.filepulls.maxtotal}{70}{}%
\contentsline {subsubsection}{\nonumberline head.filepulls.maxpernode}{70}{}%
\contentsline {subsubsection}{\nonumberline head.filepulls.qtmout}{70}{}%
\contentsline {subsubsection}{\nonumberline head.gridmapfile}{70}{}%
\contentsline {subsubsection}{\nonumberline head.prohibitrandomserver}{70}{}%
\contentsline {subsubsection}{\nonumberline head.filepuller.stathook}{70}{}%
\contentsline {subsubsection}{\nonumberline head.filepuller.stathooktimeout}{71}{}%
\contentsline {subsubsection}{\nonumberline head.mdcache.maxitems}{71}{}%
\contentsline {subsubsection}{\nonumberline head.mdcache.itemttl}{71}{}%
\contentsline {subsubsection}{\nonumberline head.mdcache.itemmaxttl}{71}{}%
\contentsline {subsubsection}{\nonumberline head.mdcache.itemttl\_negative}{71}{}%
\contentsline {subsubsection}{\nonumberline head.unlink.ignorereadonlyfs}{71}{}%
\contentsline {subsubsection}{\nonumberline head.unlink.ignorebrokenfs}{72}{}%
\contentsline {subsubsection}{\nonumberline head.informer.mainurl}{72}{}%
\contentsline {subsubsection}{\nonumberline head.informer.additionalurls[]}{72}{}%
\contentsline {subsubsection}{\nonumberline head.informer.delay}{72}{}%
\contentsline {subsubsection}{\nonumberline head.informer.additionalinfo}{73}{}%
\contentsline {subsubsection}{\nonumberline head.pendingput.timeout}{73}{}%
\contentsline {subsubsection}{\nonumberline head.oidc.allowissuer[]}{73}{}%
\contentsline {subsubsection}{\nonumberline head.oidc.allowaudience[]}{74}{}%
\contentsline {subsection}{\numberline {2.3.3}Specific to disk nodes}{74}{}%
\contentsline {subsubsection}{\nonumberline disk.headnode.domeurl}{74}{}%
\contentsline {subsubsection}{\nonumberline disk.cksummgr.heartbeatperiod}{74}{}%
\contentsline {subsubsection}{\nonumberline disk.filepuller.pullhook}{74}{}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
