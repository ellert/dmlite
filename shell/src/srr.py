# WLCG Storage Resource Reporting implementation for DPM
# * https://docs.google.com/document/d/1yzCvKpxsbcQC5K9MyvXc-vBF1HGPBk4vhjw3MEXoXf8/edit
# * https://twiki.cern.ch/twiki/bin/view/LCG/AccountingTaskForce
# Configuration
# * https://twiki.cern.ch/twiki/bin/view/DPM/DpmSetupManualInstallation#Publishing_space_usage
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import json
import time
import socket
import argparse
import tempfile
import os, sys
import subprocess
import logging, logging.handlers

try: import urllib.parse as urlparse
except ImportError: import urlparse

from dmliteshell.infoutils import SystemInfo

__version__ = '0.1.1'

_log = logging.getLogger('dmlite-shell')


# The top level object
class StorageService(object):

    def __init__(self, host, cert, key):
        self._host = host
        self.info = SystemInfo(host, cert, key)
        self.entry = {}
        self.entry["name"] = host
        self.entry["implementation"] = "DPM"
        self.entry["implementationversion"] = self.info.getsysinfo("dome")
        self.entry["qualitylevel"] = "production"
        self.entry["storageshares"] = []
        self.entry["storageendpoints"] = []
        self.entry["datastores"] = []
        self.entry["latestupdate"] = int(time.time())

    def _service_alive(self, name, legacy_name=None):
        if legacy_name == None:
            legacy_name = name
        try:
            try:
                stat = subprocess.call(["systemctl", "is-active", "--quiet", name])
            #except FileNotFoundError: # since python 3.3
            except OSError:
                # systemd not available, try legacy init scripts
                devnull = os.open(os.devnull, os.O_RDWR) # subprocess.DEVNULL only since python 3.3
                stat = subprocess.call(["service", legacy_name, 'status'], stdout=devnull, stderr=devnull)
            return stat == 0
        except Exception as e:
            _log.info("failed to get service %s (%s) status: %s", name, legacy_name, str(e))
        return False

    def addshares(self):
        jgqt, totalcapacity, totalused, totalgroups = self.info.getspaces()
        for space, qt in jgqt.items():
            quotatktotspace = max(0, int(qt["quotatktotspace"]))
            pathusedspace = max(0, int(qt["pathusedspace"]))
            self.entry["storageshares"].append(storageShare(qt["quotatkname"], quotatktotspace, pathusedspace, qt["path"], qt["groups"]))

    def addendpoints(self):
        if self._service_alive('dpm-gsiftp'):
            self.entry["storageendpoints"].append(storageEndpoint("gsiftp", "gsiftp://" + self._host + "/", "gsiftp"))
        if self._service_alive('httpd'):
            self.entry["storageendpoints"].append(storageEndpoint("https", "https://" + self._host + "/", "https"))
        if self._service_alive('xrootd@dpmredir', 'xrootd'):
            self.entry["storageendpoints"].append(storageEndpoint("xrootd", "root://" + self._host + "/", "xrootd"))
        if self._service_alive('srmv2.2'):
            self.entry["storageendpoints"].append(storageEndpoint("srm", "srm://" + self._host + ":8446/srm/managerv2?SFN=/", "srm"))

    def adddataStores(self):
        self.entry["datastores"].append(dataStore("DPM data store"))

    def printjson(self, out=sys.stdout):
        out.write(json.dumps({"storageservice": self.entry}, indent=4))
 
    def publish(self, url, cert=None, key=None, capath=None):
        if url == None or url == 'stdout://': # stdout
            sys.stdout.write(json.dumps({"storageservice": self.entry}, indent=4))
        elif url == 'stderr://':
            sys.stderr.write(json.dumps({"storageservice": self.entry}, indent=4))
        elif url.startswith('/') or url.startswith('file://'):
            pass
        elif url.startswith('davs://') or url.startswith('https://'):
            if url.startswith('davs://'):
                url = "https://{0}".format(url[len('davs://'):])
            self._publish_https(url, cert, key, capath)
        elif url.startswith('root://') or url.startswith('xroot://'):
            if url.startswith('xroot://'):
                url = "root://{0}".format(url[len('xroot://'):])
            self._publish_xrootd(url, cert, key, capath)
        else:
            raise Exception("Output URL '{0}' not supported".format(url))

    def _publish_https(self, url, cert, key, capath):
        try:
            import pycurl
        except ImportError as e:
            raise Exception("unable to import pycurl module (install python2-pycurl or python3-pycurl package): {0}".format(str(e)))

        urlbase = os.path.dirname(url)
        urlnew = "{0}.{1}".format(url, int(time.time()))

        c = pycurl.Curl()
        c.setopt(c.SSLCERT, cert)
        c.setopt(c.SSLKEY, key)
        if capath:
            c.setopt(c.CAPATH, capath)
        c.setopt(c.SSL_VERIFYPEER, 0)
        c.setopt(c.SSL_VERIFYHOST, 2)
        c.setopt(c.FOLLOWLOCATION, 1)
        if _log.getEffectiveLevel() < logging.DEBUG:
            c.setopt(c.VERBOSE, True)

        try:
            c.setopt(c.URL, urlbase)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "HEAD")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 200:
                raise Exception("base path %s not found" % urlbase)

            # avoid overwriting directory because after failed
            # PUT we would try to DELETE whole directory content
            hdrs = []
            c.setopt(c.URL, url)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "HEAD")
            c.setopt(c.HEADERFUNCTION, hdrs.append)
            c.perform()
            if c.getinfo(c.HTTP_CODE) == 200 and sum([x.startswith(b'Content-Length:') for x in hdrs]) == 0:
                raise Exception("destination %s seems to be directory" % url)

            _log.debug("put the new file to %s", urlnew)
            c.setopt(c.URL, urlnew)
            c.setopt(c.NOBODY, False)
            c.setopt(c.CUSTOMREQUEST, "PUT")
            # suppress the response body
            c.setopt(c.WRITEFUNCTION, lambda x: None)
            c.setopt(c.POSTFIELDS, json.dumps({"storageservice": self.entry}, indent=4))
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 201:
                raise Exception("unable to put new file %s (HTTP code %s)" % (urlnew, c.getinfo(c.HTTP_CODE)))

            _log.debug("delete existing file %s", url)
            c.setopt(c.URL, url)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "DELETE")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 204 and c.getinfo(c.HTTP_CODE) != 404:
                raise Exception("unable to delete file %s (HTTP code %i)" % (url, c.getinfo(c.HTTP_CODE)))

            _log.debug("rename the new file %s to %s", urlnew, url)
            c.setopt(c.URL, urlnew)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "MOVE")
            c.setopt(c.HTTPHEADER, ["Destination:%s" % url])
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 201:
                raise Exception("unable to rename %s to %s (HTTP code %i)" % (urlnew, url, c.getinfo(c.HTTP_CODE)))

            _log.info("%s written successfully" % url)

        except Exception as e:
            _log.error("file writing error: %s", str(e))

            _log.debug("delete temporary file %s", urlnew)
            c.setopt(c.URL, urlnew)
            c.setopt(c.NOBODY, True)
            c.setopt(c.CUSTOMREQUEST, "DELETE")
            c.perform()
            if c.getinfo(c.HTTP_CODE) != 204:
                if c.getinfo(c.HTTP_CODE) != 404:
                    _log.error("unable to cleanup temporary file %s (http code %i)", urlnew, c.getinfo(c.HTTP_CODE))
                else:
                    _log.info("temporary file %s doesn't exist", urlnew)

            raise Exception("failed to upload %s with HTTP protocol" % url)

        finally:
            _log.debug("cleanup")
            c.close()

    def _publish_xrootd(self, url, cert, key, capath):
        # set environment for XRootD transfers
        # XRD_* env variables must be set before importing XRootD module
        if _log.getEffectiveLevel() < logging.DEBUG:
            os.putenv('XRD_LOGLEVEL', 'Dump')
            os.putenv('XRD_LOGMASK', 'All')
            #os.putenv('XRD_LOGFILE', '/tmp/xrootd.debug')
        os.putenv('XRD_CONNECTIONWINDOW', '10') # by default connection timeouts after 300s
        #os.putenv('XRD_REQUESTTIMEOUT', '10') # can be set individually for each operation

        # set X509_* env variable used by XRootD authentication
        if cert: os.putenv('X509_USER_CERT', cert)
        if key: os.putenv('X509_USER_KEY', key)
        if capath: os.putenv('X509_CERT_DIR', capath)

        try:
            import XRootD.client
        except ImportError as e:
            raise Exception("unable to import XRootD module (install python2-xrootd or python3-xrootd package): {0}".format(str(e)))

        u = urlparse.urlparse(url)
        urlhost = "{0}://{1}".format(u.scheme, u.netloc)
        filepath = u.path
        filepathnew = "{0}.{1}".format(filepath, int(time.time()))
        filepathold = "{0}.bak".format(filepathnew)
        urlnew = "{0}{1}".format(urlhost, filepathnew)
        urlold = "{0}{1}".format(urlhost, filepathold)

        cleanup = []
        try:
            _log.debug("create temporary file with storage json")
            fd, tmpfile = tempfile.mkstemp()
            cleanup.append(tmpfile)
            with os.fdopen(fd, 'w') as f:
                f.write(json.dumps({"storageservice": self.entry}, indent=4))

            xrdc = XRootD.client.FileSystem(urlhost)

            _log.debug("upload the temporary file %s to %s", tmpfile, urlnew)

            status, details = xrdc.copy(tmpfile, urlnew, force=True)
            cleanup.append(urlnew)
            if not status.ok:
                raise Exception("unable to copy data to {0}: {1}".format(urlnew, status.message))

            status, detail = xrdc.stat(filepath)
            if status.ok:
                status, details = xrdc.mv(filepath, filepathold)
                if not status.ok:
                    raise Exception("unable to move {0} to {1}: {2}".format(filepath, filepathold, status.message))
                cleanup.append(urlold)
            else:
                _log.info("no previous version of %s: %s", filepath, status.message)

            status, details = xrdc.mv(filepathnew, filepath)
            if not status.ok:
                # move old file back in case of problems with new file
                status1, details1 = xrdc.mv(filepathold, filepath)
                if status1.ok:
                    cleanup.remove(urlold)
                raise Exception("unable to move {0} to {1}: {2}".format(filepathnew, filepath, status.message))
            cleanup.remove(urlnew)

            _log.info("%s written successfully", url)

        except Exception as e:
            _log.error(str(e))
            raise Exception("failed to upload {0} with XRootD protocol".format(filepath))

        finally:
            # cleanup temporary files
            for c in cleanup:
                try:
                    if c.startswith('root://'):
                        xrdc.rm(urlparse.urlparse(c).path)
                    else:
                        os.unlink(c)
                except Exception as e:
                    _log.debug("unable to remove temporary file %s: %s", c, str(e))


# Derive from dict for easy serialisation
class storageEndpoint(dict):

    def __init__(self, name, endpointurl, interfacetype):
        self["name"] = name
        self["endpointurl"] = endpointurl
        self["interfacetype"] = interfacetype
        self["qualitylevel"] = "production"
        self["assignedshares"] = ["all"]


# Derive from dict for easy serialisation
class storageShare(dict):

    def __init__(self, name, totalsize, usedsize, path, groups):
        self["name"] = name
        self["timestamp"] = int(time.time())
        self["totalsize"] = totalsize
        self["usedsize"] = usedsize
        self["path"] = [path]
        self["vos"] = groups
        self["assignedendpoints"] = ["all"]
        self["servingstate"] = "open"


# Derive from dict for easy serialisation
class dataStore(dict):

    def __init__(self, name):
        self["name"] = name
        self["message"] = "Not used"


#=====================================================================
# main
#=====================================================================
def main(argv):
    import inspect

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    fqdn = socket.getfqdn()
    domain = '.'.join(fqdn.split('.')[1:])
    if domain == '': domain = 'cern.ch'

    # parse options from command line
    class VerbosityAction(argparse.Action):

        def __call__(self, parser, namespace, values, option_string=None):
            loglevel = self.default
            if len(values) > 0:
                loglevel = int({
                    'CRITICAL': logging.CRITICAL,
                    'DEBUG': logging.DEBUG,
                    'ERROR': logging.ERROR,
                    'FATAL': logging.FATAL,
                    'INFO': logging.INFO,
                    'NOTSET': logging.NOTSET,
                    'WARN': logging.WARN,
                    'WARNING': logging.WARNING,
                }.get(values[0].upper(), values[0]))
            _log.setLevel(loglevel)
            setattr(namespace, self.dest, loglevel)

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", dest="loglevel", action=VerbosityAction, default=logging.DEBUG, nargs=0, help="set log level to DEBUG")
    parser.add_argument("-q", "--quiet", dest="loglevel", action=VerbosityAction, default=logging.ERROR, nargs=0, help="set log level to ERROR")
    parser.add_argument("--log-level", dest="loglevel", action=VerbosityAction, nargs=1, help="set log level, default: %(default)s")
    parser.add_argument("--log-file", dest="logfile", metavar="FILE", help="set log file (default: %(default)s)")
    parser.add_argument("--log-size", dest="logsize", type=int, default=10 * 1024 * 1024, help="maximum size of log file, default: %(default)s")
    parser.add_argument("--log-backup", dest="logbackup", type=int, default=4, help="number of log backup files, default: %(default)s")
    # storage summary arguments
    parser.add_argument("--path", help="Path to publish the summary file, default: %(default)s", default="/dpm/{0}/home/dteam".format(domain))
    parser.add_argument("--file", help="Name of the summary file, default: %(default)s", default="storagesummary.json")
    parser.add_argument("--cert", help="Path to host certificate, default: %(default)s", default="/etc/grid-security/hostcert.pem")
    parser.add_argument("--key", help="Path to host key, default: %(default)s", default="/etc/grid-security/hostkey.pem")
    parser.add_argument("--capath", help="Path to CAs, default: %(default)s", default="/etc/grid-security/certificates")
    parser.add_argument("--host", help="FQDN, default: %(default)s", default=fqdn)
    parser.add_argument("--print", help="Just print, don't publish", action="store_true", dest="prnt")
    parser.add_argument("--proto", help="Publish protocol, default %(default)s", default="https")
    parser.add_argument("--port", help="Dome's port number, default: %(default)s", default=1094)
    parser.add_argument("--davport", help="Webdav's port, default: %(default)s", default=443)

    options = parser.parse_args(argv[1:])

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    _log.info("command: %s", " ".join(argv))
    _log.info("script: %s", os.path.abspath(inspect.getfile(inspect.currentframe())))
    _log.info("version: %s", __version__)
    _log.info("python: %s", str(sys.version_info))

    try:
        # Create object
        dpm = StorageService(options.host, options.cert, options.key)
        dpm.addshares()

        # Which endpoints are supposed to be there?
        dpm.addendpoints()
        # Don't bother with this for now
        #dpm.adddataStores()

        # Print or publish
        if options.prnt or options.proto == 'stdout':
            dpm.printjson()
        elif options.proto in ['https', 'davs']:
            url = "https://%s:%s%s%s%s" % (options.host, options.davport, options.path, '' if options.path.endswith('/') else '/', options.file)
            dpm.publish(url, options.cert, options.key, options.capath)
        elif options.proto in ['root', 'xroot']:
            url = "root://%s:%s%s%s" % (options.host, options.port, options.path, '' if options.path.endswith('/') else '/', options.file)
            dpm.publish(url, options.cert, options.key, options.capath)
        else:
            _log.error("unknown publish protocol %s", options.proto)

    except Exception as e:
        _log.error(str(e))
        return 1

    return os.EX_OK
