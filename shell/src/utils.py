from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os
import time
import ctypes
import ctypes.util



#########################################################################
######## compatibility - implementation of missing functionality ########
#########################################################################

class timespec(ctypes.Structure):
    _fields_ = [
        ("tv_sec", ctypes.c_long),
        ("tv_nsec", ctypes.c_long),
    ]

    def __str__(self):
        return "%i.%09i" % (self.tv_sec, self.tv_nsec)

    def __int__(self):
        return self.tv_sec

    def __float__(self):
        return self.tv_sec + float(self.tv_nsec) / 1e9


def clock_gettime(clk_id):
    library = ctypes.util.find_library('rt')
    if not library:
        raise Exception("unable to find librt")

    try:
        _clock_gettime = ctypes.CDLL(library, use_errno=True).clock_gettime
        _clock_gettime.argtypes = (ctypes.c_int, ctypes.POINTER(timespec))
        _clock_gettime.restype = ctypes.c_int
    except OSError as e:
        raise Exception("can't load %s: %s" % (library, str(e)))

    ts = timespec()

    rc = _clock_gettime(clk_id, ctypes.pointer(ts))
    if rc != 0:
        errno = ctypes.get_errno()
        raise OSError(errno, os.strerror(errno))

    return float(ts)


if not hasattr(time, 'monotonic'):
    def _monotonic():
        CLOCK_MONOTONIC = 1
        return clock_gettime(CLOCK_MONOTONIC)
    setattr(time, 'monotonic', _monotonic)


if not hasattr(time, 'process_time'):
    def _process_time():
        CLOCK_PROCESS_CPUTIME_ID = 2
        return clock_gettime(CLOCK_PROCESS_CPUTIME_ID)
    setattr(time, 'process_time', _process_time)



#########################################################################
######## other useful functions                                  ########
#########################################################################

def prettySize(size):
    isize = int(size) # argument can be string
    if isize < 1024**1:
        prettySize = "%iB" % isize
    elif isize < 1024**2:
        prettySize = '%.2fkB' % (float(isize) / 1024**1)
    elif isize < 1024**3:
        prettySize = '%.2fMB' % (float(isize) / 1024**2)
    elif isize < 1024**4:
        prettySize = '%.2fGB' % (float(isize) / 1024**3)
    elif isize < 1024**5:
        prettySize = '%.2fTB' % (float(isize) / 1024**4)
    else:
        prettySize = '%.2fPB' % (float(isize) / 1024**5)
    return prettySize


def prettyInputSize(prettysize):
    if 'PB' in prettysize:
        prettysize = prettysize.replace('PB', '')
        size = int(prettysize) * 1024**5
    elif 'TB' in prettysize:
        prettysize = prettysize.replace('TB', '')
        size = int(prettysize) * 1024**4
    elif 'GB' in prettysize:
        prettysize = prettysize.replace('GB', '')
        size = int(prettysize) * 1024**3
    elif 'MB' in prettysize:
        prettysize = prettysize.replace('MB', '')
        size = int(prettysize) * 1024**2
    elif 'KB' in prettysize:
        prettysize = prettysize.replace('kB', '')
        size = int(prettysize) * 1024**1
    else:
        size = int(prettysize)
    return size



if __name__ == '__main__':
    for i in range(5):
        print("monotonic: %.06f" % time.monotonic())
    for i in range(5):
        for i in range(1000000): pass
        print("process_time: %.06f" % time.process_time())
