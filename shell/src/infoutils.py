from __future__ import division
import json
import re
import subprocess
import os
import sys
from datetime import datetime

if sys.version_info[:2] < (2, 7):
    # python-asn1 dependency for python-ldap3 package
    # doesn't work on SLC6 with python 2.6
    import ldif
else:
    import ldap3
import rpm

from .executor import DomeExecutor


class SystemInfo(object):
    """All necessary info on the DPM and the system"""

    def __init__(self, host, cert, key):
        self.host = host
        self.cert = cert
        self.key = key
        self.capath = '/etc/grid-security/certificates/'
        self.ts = rpm.TransactionSet()
        self.ports = set([])
        self.getlisteningports()

    def getlisteningports(self):
        """Check which ports are being listened on"""
        try:
            devnull = os.open(os.devnull, os.O_RDWR) # subprocess.DEVNULL only since python 3.3
            pipe_out, _ = subprocess.Popen("ss -tln", shell=True, stdout=subprocess.PIPE, stderr=devnull).communicate()
            for listen in pipe_out.split('\n'):
                m = re.search(r":([0-9]+)\s", listen)
                if m != None:
                    self.ports.add(int(m.group(1)))
        except:
            pass

    def rpmversion(self, package):
        """Get version number from rpm package"""
        mi = self.ts.dbMatch("name", package)
        if mi.count() != 1:
            return None
        else:
            entry = next(mi)
            return str(entry['version'])

    def nameversion(self, packages):
        for package in packages:
            version = self.rpmversion(package)
            if version != None:
                return [package, version]
        return ["unknown", "unknown"]

    def getprotinfo(self, prot):
        """Return relevant info for each protocol"""
        if prot == "https":
            if 443 in self.ports:
                interfaceversion = "1.1"
                interfacename = "https"
                implname, implversion = self.nameversion(["lcgdm-dav", "dmlite-apache-httpd"])
                return interfacename, interfaceversion, implname, implversion
            else:
                return None
        if prot == "gsiftp":
            if 2811 in self.ports:
                interfacename = "gsiftp"
                interfaceversion = "2.0"
                implname, implversion = self.nameversion(["dpm-dsi", "dmlite-dpm-dsi"])
                return interfacename, interfaceversion, implname, implversion
            else:
                return None
        if prot == "root":
            if 1094 in self.ports:
                interfacename = "xroot"
                interfaceversion = "3.1"
                implname, implversion = self.nameversion(["dpm-xrootd", "dmlite-dpm-xrootd"])
                return interfacename, interfaceversion, implname, implversion
            else:
                return None
        if prot == "srm":
            if 8446 in self.ports:
                interfacename = "ogf.srm"
                interfaceversion = "2.2"
                implname = "dpm-srm-server-mysql"
                implversion = self.rpmversion(implname)
                return interfacename, interfaceversion, implname, implversion
            else:
                return None
        return None

    def getsysinfo(self, component):
        """Get dmlite related info"""
        if component == "dome":
            return self.rpmversion("dmlite-dome")
        if component == "dmlite":
            return self.rpmversion("dmlite-libs")
        return None

    def getspaces(self):
        """Find all the StorageShares on this DPM
        and return an array of them"""
        dome_commnad_base = "https://" + self.host + ":1094/domehead"
        executor = DomeExecutor(dome_commnad_base, self.cert, self.key, self.capath, "", "")
        jgqt, err = executor.getquotatoken("/", 0, 1)
        if err:
            raise Exception("{0}: Error on host {1} ({2})".format(sys.argv[0], self.host, str(err)))
        totalcapacity = 0
        totalused = 0
        totalgroups = set([])
        for space, qt in jgqt.items():
            if isinstance(qt["groups"], dict):
                groups = list(qt["groups"].values())
            if isinstance(qt["groups"], list):
                groups = qt["groups"]
            # don't show "root" qt group unless it is the only group asociated
            # with this space, because ldif code rely on non-empty groups
            if len(groups) > 1: groups.remove("root")
            qt["groups"] = groups
            list(map(totalgroups.add, groups))
            totalcapacity += int(qt["quotatktotspace"])
            totalused += int(qt["pathusedspace"])
        return jgqt, totalcapacity, totalused, list(totalgroups)


class Entry(object):
    """Base class for all GLUE2 entries"""

    def __init__(self):
        # An array of child objects in the ldap hierarchy
        self.Children = []
        # This will be the attribute taken for the DN
        self.name = ""
        self.Attributes = {}
        self.Attributes["GLUE2EntityCreationTime"] = [datetime.utcnow().replace(microsecond=0).isoformat() + 'Z']
        if sys.version_info[:2] < (2, 7):
            self.ldif_writer = ldif.LDIFWriter(sys.stdout, cols=1000000)
        else:
            self.ldif_writer = ldap3.Connection(None, client_strategy='LDIF')

    def add_child(self, entry):
        self.Children.append(entry)

    def convert(self, data):
        ret = {}
        for k, v in data.items():
            ret[k] = list(map(lambda x: x.encode('utf-8'), v))

    def print_out(self, parent=None):
        """ Recursively print out with all children"""
        if parent == None:
            dn = self.name + "=" + self.Attributes[self.name][0]
        else:
            dn = self.name + "=" + self.Attributes[self.name][0] + "," + parent
        bAttributes = {}
        if sys.version_info[:2] < (2, 7):
            # python3 LDIFWriter works only with string dictionary keys
            # and list of bytes as dictionary attribute values
            for k, v in self.Attributes.items():
                bAttributes[k] = list(map(lambda x: x.encode('utf-8'), v))
            self.ldif_writer.unparse(dn, bAttributes)
        else:
            bClasses = None
            for k, v in self.Attributes.items():
                if k.lower() == 'objectclass':
                    bClasses = list(map(lambda x: x.encode('utf-8'), v))
                else:
                    bAttributes[k] = list(map(lambda x: x.encode('utf-8'), v))
            for line in self.ldif_writer.add(dn, bClasses, bAttributes).split('\n'):
                if line.startswith('version: '): continue
                if line.startswith('changetype: add'): continue
                sys.stdout.write("%s\n" % line)
            sys.stdout.write('\n')

        for e in self.Children:
            e.print_out(dn)

    def set_foreign_key(self):
        """To be implemented by subclasses"""
        pass

    def getname(self):
        return self.Attributes[self.name][0]


class Service(Entry):
    """A GLUE2Service"""

    def __init__(self, hostname, sitename):
        Entry.__init__(self)
        self.name = "GLUE2ServiceID"
        self.Attributes["GLUE2ServiceID"] = [hostname + "/Service"]
        self.Attributes["GLUE2ServiceType"] = ["dpm"]
        self.Attributes["GLUE2ServiceQualityLevel"] = ["production"]
        self.Attributes["GLUE2ServiceCapability"] = [
            'data.access.flatfiles',
            'data.transfer',
            'data.management.replica',
            'data.management.storage',
            'data.management.transfer',
            'security.authentication',
            'security.authorization',
        ]
        self.Attributes["GLUE2ServiceAdminDomainForeignKey"] = [sitename]
        self.Attributes["ObjectClass"] = ["GLUE2Service", "GLUE2StorageService"]


class Endpoint(Entry):
    """A GLUE2Endpoint"""

    def __init__(self, epprot, ephost, epport, eppath, epcert, interfacename, interfaceversion, implname, implversion):
        Entry.__init__(self)
        self.name = "GLUE2EndpointID"
        self.Attributes["GLUE2EndpointID"] = [ephost + "/Endpoint/" + epprot]
        self.Attributes["GLUE2EndpointURL"] = [epprot + "://" + ephost + ":" + str(epport) + eppath]
        if epcert != None: self.Attributes["GLUE2EndpointIssuerCA"] = [epcert]
        self.Attributes["GLUE2EndpointInterfaceName"] = [interfacename]
        self.Attributes["GLUE2EndpointInterfaceVersion"] = [interfaceversion]
        self.Attributes["GLUE2EndpointImplementationName"] = [implname]
        self.Attributes["GLUE2EndpointImplementationVersion"] = [implversion]
        self.Attributes["GLUE2EndpointQualityLevel"] = ["production"]
        self.Attributes["GLUE2EndpointHealthState"] = ["ok"]
        self.Attributes["GLUE2EndpointServingState"] = ["production"]
        self.Attributes["GLUE2EndpointCapability"] = [
            'data.access.flatfiles',
            'data.transfer',
            'data.management.replica',
            'data.management.storage',
            'data.management.transfer',
            'security.authentication',
            'security.authorization',
        ]
        self.Attributes["GLUE2EndpointServiceForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2StorageEndpoint", "GLUE2Endpoint"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2EndpointServiceForeignKey"] = [value]


class AccessProtocol(Entry):
    """A Glue2AccessProtocol"""

    def __init__(self, ephost, interfacename, interfaceversion, implname, implversion):
        Entry.__init__(self)
        self.name = "GLUE2StorageAccessProtocolID"
        self.Attributes["GLUE2StorageAccessProtocolID"] = [ephost + "/AccessProtocol/" + interfacename]
        self.Attributes["GLUE2StorageAccessProtocolVersion"] = [interfaceversion]
        self.Attributes["GLUE2StorageAccessProtocolType"] = [interfacename]
        # self.Attributes["GLUE2StorageAccessProtocolEndpoint"]=[endpointurl]
        self.Attributes["GLUE2StorageAccessProtocolStorageServiceForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2StorageAccessProtocol"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2StorageAccessProtocolStorageServiceForeignKey"] = [value]


class AccessPolicy(Entry):
    """A GLUE2AccessPolicy"""

    def __init__(self, groups, hostname):
        Entry.__init__(self)
        self.name = "GLUE2PolicyID"
        self.Attributes["GLUE2PolicyID"] = [hostname + "/AccessPolicy"]
        self.Attributes["GLUE2PolicyRule"] = groups
        self.Attributes["GLUE2PolicyScheme"] = ["org.glite.standard"]
        self.Attributes["GLUE2AccessPolicyEndpointForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2AccessPolicy"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2AccessPolicyEndpointForeignKey"] = [value]


class StorageServiceCapacity(Entry):
    """A GLUE2StorageServiceCapacity"""

    def __init__(self, hostname, tot, used):
        Entry.__init__(self)
        self.name = "GLUE2StorageServiceCapacityID"
        self.Attributes["GLUE2StorageServiceCapacityID"] = [hostname + "/StorageServiceCapacity"]
        self.Attributes["GLUE2StorageServiceCapacityType"] = ["online"]
        self.Attributes["GLUE2StorageServiceCapacityFreeSize"] = [str((tot - used) // 1024**3)]
        self.Attributes["GLUE2StorageServiceCapacityTotalSize"] = [str(tot // 1024**3)]
        self.Attributes["GLUE2StorageServiceCapacityUsedSize"] = [str(used // 1024**3)]
        self.Attributes["GLUE2StorageServiceCapacityStorageServiceForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2StorageServiceCapacity"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2StorageServiceCapacityStorageServiceForeignKey"] = [value]


class DataStore(Entry):
    """A GLUE2DataStore"""

    def __init__(self, hostname, tot, used):
        Entry.__init__(self)
        self.name = "GLUE2ResourceID"
        self.Attributes["GLUE2ResourceID"] = [hostname + "/DataStore"]
        self.Attributes["GLUE2DataStoreTotalSize"] = [str(tot // 1024**3)]
        self.Attributes["GLUE2DataStoreUsedSize"] = [str(used // 1024**3)]
        self.Attributes["GLUE2DataStoreFreeSize"] = [str((tot - used) // 1024**3)]
        self.Attributes["GLUE2DataStoreType"] = ["disk"]
        self.Attributes["GLUE2DataStoreLatency"] = ["online"]
        self.Attributes["GLUE2DataStoreStorageManagerForeignKey"] = ["Undefined"]
        self.Attributes["GLUE2ResourceManagerForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2DataStore"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2DataStoreStorageManagerForeignKey"] = [value]
        self.Attributes["GLUE2ResourceManagerForeignKey"] = [value]


class MappingPolicy(Entry):
    """A GLUE2MappingPolicy"""

    def __init__(self, qtname, groups, hostname):
        Entry.__init__(self)
        self.name = "GLUE2PolicyID"
        self.Attributes["GLUE2PolicyID"] = [hostname + "/MappingPolicy/" + qtname]
        self.Attributes["GLUE2PolicyRule"] = groups
        self.Attributes["GLUE2PolicyScheme"] = ["org.glite.standard"]
        self.Attributes["GLUE2MappingPolicyShareForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2MappingPolicy"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2MappingPolicyShareForeignKey"] = [value]


class Manager(Entry):
    """A GLUE2Policy"""

    def __init__(self, domeversion, dmliteversion, hostname):
        Entry.__init__(self)
        self.name = "GLUE2ManagerID"
        self.Attributes["GLUE2ManagerID"] = [hostname + "/Manager"]
        self.Attributes["GLUE2ManagerProductName"] = ["DPM"]
        self.Attributes["GLUE2ManagerProductVersion"] = [domeversion]
        self.Attributes["GLUE2EntityOtherInfo"] = ["dmliteversion=" + dmliteversion]
        self.Attributes["GLUE2StorageManagerStorageServiceForeignKey"] = ["Undefined"]
        self.Attributes["GLUE2ManagerServiceForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2StorageManager", "GLUE2Manager"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2StorageManagerStorageServiceForeignKey"] = [value]
        self.Attributes["GLUE2ManagerServiceForeignKey"] = [value]


class Share(Entry):
    """A GLUE2StorageShare"""

    def __init__(self, path, pool, groups, qtname, hostname):
        Entry.__init__(self)
        self.name = "GLUE2ShareID"
        self.Attributes["GLUE2ShareID"] = [hostname + "/Share/" + qtname]
        self.Attributes["GLUE2ShareDescription"] = ['Reserved space for VOs {0} (quotatoken {1})'.format(','.join(sorted(groups)), qtname)]
        self.Attributes["GLUE2StorageShareServingState"] = ["production"]
        self.Attributes["GLUE2StorageShareSharingID"] = [pool]
        self.Attributes["GLUE2StorageShareAccessLatency"] = ["online"]
        self.Attributes["GLUE2StorageShareExpirationMode"] = ["releasewhenexpired"]
        self.Attributes["GLUE2StorageShareRetentionPolicy"] = ["output"]
        self.Attributes["GLUE2StorageSharePath"] = [path]
        self.Attributes["GLUE2StorageShareStorageServiceForeignKey"] = ["Undefined"]
        self.Attributes["GLUE2ShareServiceForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2Share", "GLUE2StorageShare"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2StorageShareStorageServiceForeignKey"] = [value]
        self.Attributes["GLUE2ShareServiceForeignKey"] = [value]


class ShareCapacity(Entry):
    """A GLUE2StorageShareCapacity"""

    def __init__(self, qtname, qtspace, pathused, hostname):
        Entry.__init__(self)
        self.name = "GLUE2StorageShareCapacityID"
        self.Attributes["GLUE2StorageShareCapacityID"] = [hostname + "/Capacity/" + qtname]
        self.Attributes["GLUE2StorageShareCapacityTotalSize"] = [str(qtspace // 1024**3)]
        self.Attributes["GLUE2StorageShareCapacityUsedSize"] = [str(pathused // 1024**3)]
        self.Attributes["GLUE2StorageShareCapacityFreeSize"] = [str((qtspace - pathused) // 1024**3)]
        self.Attributes["GLUE2StorageShareCapacityType"] = ["online"]
        self.Attributes["GLUE2StorageShareCapacityStorageShareForeignKey"] = ["Undefined"]
        self.Attributes["ObjectClass"] = ["GLUE2StorageShareCapacity"]

    def set_foreign_key(self, value):
        self.Attributes["GLUE2StorageShareCapacityStorageShareForeignKey"] = [value]
