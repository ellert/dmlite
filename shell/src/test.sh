#!/bin/sh
# Test dmlite-shell functionality
PYTHON=python
DSHELL=/usr/bin/dmlite-shell

TEST_DISKSERVER='dpmdisk1.farm.particle.cz'
TEST_BASE='/dpm/farm.particle.cz/home/test_dmlite_shell'
TEST_USER='test_dmlite_shell'
TEST_GROUP='test_dmlite_shell'
TEST_POOL='test_ds_pool' # max 15 characters

function testCmd() {
  RUN="${PYTHON} ${DSHELL}"
  for ARG in "$@"; do
    RUN="${RUN} -e '${ARG}'"
  done
  echo "########################################################################"
  echo "Running: ${RUN}"
  eval "${RUN}"
  if [ $? -ne 0 ]; then
    echo "FAILED"
    exit 1
  fi
}

function cleanupCmd() {
  RUN="${PYTHON} ${DSHELL} -e '$1' > /dev/null"
  echo "Cleanup (these commands can fail): ${RUN}"
  eval "${RUN}"
}



if [ $# -lt 2 ]; then
  echo "usage: $0 /dpm/fqdn/home/testdir diskserver.fqdn [ /usr/bin/python [ /usr/bin/dmlite-shell ]]"
  echo "example: $0 /dpm/farm.particle.cz/home/test_dmlite_shell dpmdisk1.farm.particle.cz /usr/bin/python3"
  echo "!!! WARNING - use non-existing base path for tests not to overwrite/destroy existing data !!!"
  exit 1
fi

if [ $# -ge 1 ]; then
  TEST_BASE=$1
fi

if [ $# -ge 2 ]; then
  TEST_DISKSERVER=$2
fi

if [ $# -ge 3 ]; then
  PYTHON=$3
fi

if [ $# -ge 4 ]; then
  DSHELL=$4
fi



echo "### dmlite-shell test ###"
echo "python interpreter: ${PYTHON}"
echo "dmlite-shell script: ${DSHELL}"
echo "test base path: ${TEST_BASE}"
echo "test disknode: ${TEST_DISKSERVER}"
echo "test user: ${TEST_USER}"
echo "test group: ${TEST_GROUP}"
echo "test pool: ${TEST_POOL}"

echo "########################################################################"
cleanupCmd "unlink ${TEST_BASE}/file1"
cleanupCmd "unlink ${TEST_BASE}/file1-moved"
cleanupCmd "unlink ${TEST_BASE}/file2"
cleanupCmd "unlink ${TEST_BASE}/file2-moved"
cleanupCmd "unlink ${TEST_BASE}/file3"
cleanupCmd "unlink ${TEST_BASE}/file3-moved"
cleanupCmd "rmdir ${TEST_BASE} -r"
cleanupCmd "userdel ${TEST_USER}"
cleanupCmd "groupdel ${TEST_GROUP}"
cleanupCmd "quotatokendel ${TEST_BASE} ${TEST_POOL}"
cleanupCmd "fsdel /tmp ${TEST_DISKSERVER}"
cleanupCmd "pooldel ${TEST_POOL}"
echo "########################################################################"
echo "########################################################################"



testCmd 'help'
testCmd 'version'
testCmd 'getimplid'
testCmd 'qryconf'
testCmd 'exit'
#testCmd 'pwd'
testCmd 'cd /' 'pwd' 'cd dpm' 'pwd' 'ls' 'cd ..' 'ls'
#testCmd "ls ${TEST_BASE}"

# pool, filesystems and quotatoken
testCmd "pooladd ${TEST_POOL} filesystem"
testCmd "poolmodify ${TEST_POOL} defsize 12345678"
testCmd "fsadd /tmp ${TEST_POOL} ${TEST_DISKSERVER}"
testCmd "fsmodify /tmp ${TEST_DISKSERVER} ${TEST_POOL} DISABLED"
testCmd "poolinfo"
testCmd "mkdir ${TEST_BASE} -p"
testCmd "quotatokenset ${TEST_BASE} pool ${TEST_POOL} size 1073741824 desc TEST_DS_POOL groups root"
#testCmd "quotatokenmod <token_id> path ${TEST_BASE}"
#testCmd "quotatokenmod <token_id> pool ${TEST_POOL}"
#testCmd "quotatokenmod <token_id> size 10737418240"
#testCmd "quotatokenmod <token_id> desc TEST_DS_POOL"
#testCmd "quotatokenmod <token_id> groups root"
testCmd "quotatokenget / -s"
testCmd "quotatokendel ${TEST_BASE} ${TEST_POOL}"
testCmd "rmdir ${TEST_BASE} -r"
testCmd "fsdel /tmp ${TEST_DISKSERVER}"
testCmd "pooldel ${TEST_POOL}"

# create/remove directories and files
#testCmd "getlfn 5"
testCmd "mkdir ${TEST_BASE} -p"
testCmd "info ${TEST_BASE}"
testCmd "mkdir ${TEST_BASE}/dir1"
testCmd "ln ${TEST_BASE}/dir1 ${TEST_BASE}/dir1-link"
testCmd "readlink ${TEST_BASE}/dir1-link"
testCmd "rmdir ${TEST_BASE}/dir1"
testCmd "mkdir ${TEST_BASE}/dir1/1/2/3/4 -p"
testCmd "rmdir ${TEST_BASE}/dir1 -r"
testCmd "cd ${TEST_BASE}" "mkdir dir2"
testCmd "cd ${TEST_BASE}" "ln dir2 dir2-link"
testCmd "cd ${TEST_BASE}" "rmdir dir2"
testCmd "create ${TEST_BASE}/file1"
testCmd "ln ${TEST_BASE}/file1 ${TEST_BASE}/file1-link"
testCmd "readlink ${TEST_BASE}/file1-link"
testCmd "info ${TEST_BASE}/file1"
testCmd "mv ${TEST_BASE}/file1 ${TEST_BASE}/file1-moved"
testCmd "unlink ${TEST_BASE}/file1-moved"
testCmd "create ${TEST_BASE}/file2 775"
testCmd "info ${TEST_BASE}/file2"
testCmd "ln ${TEST_BASE}/file2 ${TEST_BASE}/file2-link"
testCmd "readlink ${TEST_BASE}/file2-link"
testCmd "mv ${TEST_BASE}/file2 ${TEST_BASE}/file2-moved"
testCmd "unlink ${TEST_BASE}/file2-moved"
testCmd "cd ${TEST_BASE}" "create file3"
testCmd "cd ${TEST_BASE}" "ln file3 file3-link"
testCmd "cd ${TEST_BASE}" "readlink file3-link"
testCmd "cd ${TEST_BASE}" "info file3"
testCmd "cd ${TEST_BASE}" "mv file3 file3-moved"
testCmd "cd ${TEST_BASE}" "unlink file3-moved"
testCmd "ls ${TEST_BASE}"
testCmd "du ${TEST_BASE}"
testCmd "rmdir ${TEST_BASE} -r"

# manage users
testCmd "userinfo"
testCmd "useradd ${TEST_USER}"
testCmd "userinfo ${TEST_USER}"
testCmd "userban ${TEST_USER} LOCAL_BAN"
testCmd "userinfo ${TEST_USER}"
testCmd "userban ${TEST_USER} ARGUS_BAN"
testCmd "userinfo ${TEST_USER}"
testCmd "userban ${TEST_USER} NO_BAN"
testCmd "userinfo ${TEST_USER}"
testCmd "userdel ${TEST_USER}"

# manage groups
testCmd "groupinfo"
testCmd "groupadd ${TEST_GROUP}"
testCmd "groupinfo ${TEST_GROUP}"
testCmd "groupban ${TEST_GROUP} LOCAL_BAN"
testCmd "groupinfo ${TEST_GROUP}"
testCmd "groupban ${TEST_GROUP} ARGUS_BAN"
testCmd "groupinfo ${TEST_GROUP}"
testCmd "groupban ${TEST_GROUP} NO_BAN"
testCmd "groupinfo ${TEST_GROUP}"
testCmd "groupdel ${TEST_GROUP}"

# change owner/group/mode
testCmd "useradd ${TEST_USER}"
testCmd "groupadd ${TEST_GROUP}"
testCmd "mkdir ${TEST_BASE}/dir1 -p"
testCmd "chmod ${TEST_BASE}/dir1 755"
testCmd "chgrp ${TEST_BASE}/dir1 ${TEST_GROUP}"
testCmd "chown ${TEST_BASE}/dir1 ${TEST_USER}"
#testCmd "utime ${TEST_BASE}/dir1 2020-02-02 2030-03-03"
testCmd "comment ${TEST_BASE}/dir1 \"this is a test\""
testCmd "comment ${TEST_BASE}/dir1"
testCmd "info ${TEST_BASE}/dir1"
testCmd "cd ${TEST_BASE}" "mkdir dir2"
testCmd "cd ${TEST_BASE}" "chmod dir2 755"
testCmd "cd ${TEST_BASE}" "chgrp dir2 ${TEST_GROUP}"
testCmd "cd ${TEST_BASE}" "chown dir2 ${TEST_USER}"
#testCmd "cd ${TEST_BASE}" "utime dir2 2020-02-02 2030-03-03"
testCmd "cd ${TEST_BASE}" "comment dir2 \"this is a test\""
testCmd "cd ${TEST_BASE}" "comment dir2"
testCmd "cd ${TEST_BASE}" "info dir2"
testCmd "rmdir ${TEST_BASE} -r"
testCmd "groupdel ${TEST_GROUP}"
testCmd "userdel ${TEST_USER}"

# change ACL
testCmd "useradd ${TEST_USER}"
testCmd "groupadd ${TEST_GROUP}"
testCmd "mkdir ${TEST_BASE} -p"
testCmd "create ${TEST_BASE}/file1 644"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 user::rw-,group::rw-,other::r-- set"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 group::--- modify"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 user:${TEST_USER}:r--,group:${TEST_GROUP}:r--,mask::rw- modify"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 user:${TEST_USER}:rw-,group:${TEST_GROUP}:rw-,mask::r-- modify"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 user:${TEST_USER}:rw-,group:${TEST_GROUP}:rw-,mask::rw- modify"
testCmd "acl ${TEST_BASE}/file1"
testCmd "acl ${TEST_BASE}/file1 user:${TEST_USER}:rw-,group:${TEST_GROUP}:rw-,mask::rw- delete"
testCmd "acl ${TEST_BASE}/file1"
testCmd "unlink ${TEST_BASE}/file1"
testCmd "mkdir ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 user::rwx,group::rwx,other::r-x set"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 group::--- modify"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 user:${TEST_USER}:r-x,group:${TEST_GROUP}:r-x,mask::rwx modify"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 user:${TEST_USER}:rwx,group:${TEST_GROUP}:rwx,mask::r-x modify"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 user:${TEST_USER}:rwx,group:${TEST_GROUP}:rwx,mask::rwx modify"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "acl ${TEST_BASE}/dir1 user:${TEST_USER}:rwx,group:${TEST_GROUP}:rwx,mask::rwx delete"
testCmd "acl ${TEST_BASE}/dir1"
testCmd "rmdir ${TEST_BASE}/dir1"
testCmd "rmdir ${TEST_BASE} -r"
testCmd "groupdel ${TEST_GROUP}"
testCmd "userdel ${TEST_USER}"
